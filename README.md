
Streaming CRM
=============

Perform streaming variational inference for Bayesian nonparametric latent
variable models based on completely random measures.


Contents
--------

 * `feature_models/`: Implementations of IBP feature models.
 * `mixture_models/`: Implementations of NRM mixture models.


Installation
------------

- The `util` directory in `pybasicbayes` contains cython code that must be
  built.  Run:
    1. `cd pybasicbayes/util`
    2. `python setup.py build_ext --inplace`
    3. `cd ../..`
