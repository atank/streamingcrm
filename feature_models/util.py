from __future__ import division


import numpy as np
import numpy.linalg as npl
import scipy.linalg as spl

def mvn_logpdf(x, mu, Sig):

    D = mu.shape[0]
    #C = npl.cholesky(Sig)


    # check signs and whatnot
    #lp = -D/2.*np.log(2.*np.pi) - np.sum(np.log(np.diag(C)))

    lp = -D/2.*np.log(2.*np.pi)-D*np.log(Sig[0,0])/2

    x_mc = x - mu
    #tmp = spl.solve_triangular(C, x_mc, lower=True)

    lp -= 0.5*np.dot(x_mc, x_mc)*(1/Sig[0,0])

    return lp


def make_Ztilde(z, D):

    ztilde = np.zeros(D*z.shape[0])
    for i in xrange(z.shape[0]):
        ztilde[i*D] = z[i]
    Ztilde = spl.circulant(ztilde).T[:D,:]

    return Ztilde

def make_8x8_bars(N, sigma,size, alpha,non):
    nbars = 8
    prob = alpha*np.ones(nbars)
    bars = np.zeros([size, size, size])
    bars[0,0,] = 1
    bars[1,2,] = 1  
    bars[2,4,] = 1
    bars[3,6,] = 1
    bars[4,:,0] = 1
    bars[5,:,2] = 1
    bars[6,:,4] = 1
    bars[7,:,6] = 1
    barson = np.zeros([N, nbars])
    whichoff = range(non,size)

    dat = np.zeros([N, size, size])
    dat_v = np.zeros([N,size*size])
    for i in xrange(0,N):
        on = np.random.uniform(0,1,nbars) < prob
        on[whichoff] = False
        print(on)
        barson[i,] = on
        dummy = np.where(on == True)[0]
        print(dummy)
        for j in dummy:
            dat[i,:,:] += bars[j,:,:]
        dat[i,:,:] += np.sqrt(sigma)*np.random.randn(size,size)
        dat_v[i,:] = dat[i,:,:].flatten()

    return(dat_v)

def trans(x,length):
    bit = np.zeros(length)
    if x == 0: return(bit)
    for i in xrange(length):
        bit[i] = x % 2
        x >>= 1
    return(bit[::-1])
