from __future__ import division

import numpy as np

def gen_seeds(ntest):
    return 8675309 + np.arange(ntest, dtype=np.int_)


# def bars_topics_random(alpha, ntopics, prob_on, V, nexamples,wordspertopic):
# 	x = np.zeros(nexamples,V)
# 	topics = np.zeros(ntopics, V)
# 	alpha_ones = np.ones(V)*alpha
# 	prob_on = prob
# 	#draw topic proportions
# 	for i in xrange(ntopics):
# 		topics[i,:] = np.random.dirichlet(alpha_ones)

# 	topics_per_doc = np.zeros(nexamples, ntopics)
# 	for i in xrange(nexamples):
# 		featon =  np.random.randn < prob_on
# 		topics_per_doc[i,:] = featon
# 		featon = np.where(featon)[0]
# 		for j in xrange(featon):
# 			add = np.multinomial(wordspertopic, topics[j,:] )
# 			x[i,:] += add

# 	return(topics_per_doc, topics, x)


def make_8x8_bars_disc(N,non, wordspertopic, prob_topon, baseline_prob):
	size = 8
	nbars = 8
	prob = prob_topon*np.ones(nbars)
	bars = np.zeros([size, size, size]) + baseline_prob
	bars_flat = np.zeros([size, size*size])
	bars[0,0,] = .1
	bars[1,2,] = .1
	bars[2,4,] = .1
	bars[3,6,] = .1
	bars[4,:,0] = .1
	bars[5,:,2] = .1
	bars[6,:,4] = .1
	bars[7,:,6] = .1

	for i in xrange(size):
		#print(bars[i,:,:])
		bars_flat[i,:] = bars[i,:,:].flatten()
		bars_flat[i,:] /= np.sum(bars_flat[i,:])
		#print(bars_flat[i,:])

	barson = np.zeros([N, nbars])
	whichoff = range(non,size)
	dat_v = np.zeros([N,size*size])
	for i in xrange(0,N):
		numon = 0
		while numon == 0:
			on = np.random.uniform(0,1,nbars) < prob
			on[whichoff] = False
			dummy = np.where(on == True)[0]
			numon = dummy.shape[0]
			#print(on)
		barson[i,] = on
		dummy = np.where(on == True)[0]

		#print(dummy)
		for j in dummy:
			dat_v[i,:] += np.random.multinomial(wordspertopic,bars_flat[j,:])

	return(dat_v, barson, bars_flat)


def top_topic_words(topics, vocab, M):
    """ Return list of lists containing the top M words from each topic.
        
        topics : V x K ndarray, topic distributions.
        vocab : list of length V.
        M : int > 0, number of words for each topic.
    """

    top = list()

    for k in xrange(topics.shape[1]):
        p = topics[k,:]
        p /= np.sum(p)

        # Sort and then reverse to get descending
        inds = np.argsort(p)[::-1]

        tmp = [vocab[i] for i in inds[:M]]

        top.append(tmp)

    return top

