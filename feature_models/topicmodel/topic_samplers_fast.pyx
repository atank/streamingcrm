from __future__ import division

import numpy as np
cimport numpy as np

import cython
cimport cython

import scipy as sp

@cython.boundscheck(True)
@cython.wraparound(False)
@cython.cdivision(True)
def loglik_maskeddocs(np.ndarray[np.double_t, ndim=2] Xtete not None,
                      np.ndarray[np.double_t, ndim=2] Xtetr not None,
                      double U,
                      np.ndarray[np.double_t, ndim=1] pi not None,
                      double shape, double rate, int Nlocalgibbs,
                      int Nlocalburnin, int Nlocalthin, int ntest, int K, int nV):

    cdef double llik_total = 0.
    cdef int i

    for i in xrange(ntest):
        Ztotal,Vsamp,samp_count = uncollapsed_local_FTM_sampler(Xtetr[i,:],U,pi,Nlocalgibbs,Nlocalburnin,
                                                                Nlocalthin,K,shape,rate,nV)
        # Compute expected value of local variables
        Ez = Ztotal / samp_count
        Ev = np.sum(Vsamp, axis=0) / <double>(samp_count)
        Ez = np.squeeze(Ez)
        llik_total += likelihoodcompute(Ez,Ev,U,Xtete[i,:],nV)
        #print(llik_total)

    return(llik_total)


def uncollapsed_local_FTM_sampler(x,U,pi,Nlocalgibbs,Nlocalburnin,Nlocalthin,K,shape,rate,nV):
    scale = 1/rate
    tsamp = int(Nlocalgibbs/Nlocalthin) - 1
    Vsamp = np.empty((tsamp,K))
    #initialize the Vs
    V = np.ones((1,K))*shape*scale
    #initialize the Zs
    Z = np.random.rand(1,K) < pi
    Ztotal = np.zeros((1,K))
    #initialize S
    S = np.zeros((1,nV, K))
    samp_count = 0
    #####begin local sampler#####

    for i in xrange(Nlocalgibbs + Nlocalburnin):
        S = sampleS_unc(U,Z,V,S,x[np.newaxis,:],1,K,nV)
        topiccounts = np.sum(S,1)
        Z = sampleZ_unc(Z,V,pi,topiccounts,1)
        #print topiccounts.shape
        V = sampleV_unc(topiccounts,1,V,K,shape,rate)
        if i > Nlocalburnin and (i - Nlocalburnin) % Nlocalthin == 0:
            Ztotal += Z
            Vsamp[samp_count,:] = V
            samp_count += 1
    return(Ztotal,Vsamp,samp_count)


def sampleZ_unc(Z,V,pi,topiccounts,n):
    for i in xrange(n):
        #which zs are active
        nowords = np.where(topiccounts[i,:] == 0)[0]
        for k in nowords:
            p0 = 1 - pi[k]
            p1 = np.exp(-V[i,k])*pi[k]
            #maxl = max(logp1,logp0)
            #logp1 -= maxl
            #logp0 -= maxl
            #p1 = np.exp(logp1)/(np.exp(logp0) + np.exp(logp1))
            p1 = p1/(p1 + p0)
            Z[i,k] = np.random.rand() < p1
    return Z


def sampleV_unc(topiccounts,n,V,K,shape,rate):
    for i in xrange(n):
        for k in xrange(K):
            shapenew = shape + topiccounts[i,k]
            ratenew = rate + 1
            scalenew = 1./ratenew
            V[i,k] = np.random.gamma(shapenew,scalenew)
    return V


def likelihoodcompute(Epi,Ev,Eu,test_doc,nV):
    rate = np.dot(Eu, Ev*Epi)
    if np.max(rate) == 0:
        print "fart"
        rate = np.ones(nV)*(1e-100)
    return poisson_loglikelihood(test_doc, rate)


def sampleS_unc(U,Z,V, S,X,n,K,nV):
    for i in xrange(n):
        for j in xrange(nV):
            p = U[j,:]*Z[i,:]*V[i,:]
            if np.max(p) == 0:
                p == np.ones(K)*1./K
            else:
                p /= np.sum(p)
            S[i,j,:] = np.random.multinomial(X[i,j],p)
    return S


def poisson_loglikelihood(test_doc, rate):
    loglik = np.sum(-rate + test_doc*np.log(rate) - np.log(sp.misc.factorial(test_doc)))
    return(loglik)
