from __future__ import division

import types

import numpy as np
import scipy as sp
import scipy.stats as ss

import scipy.optimize as spopt

from util import make_8x8_bars_disc, test_data_separate

# Switch these commented lines to use the fast code
from topic_samplers import loglik_maskeddocs
#from topic_samplers_fast import loglik_maskeddocs

import topicstream_fast


import matplotlib.pyplot as plt

eps = np.finfo('float64').eps

class StreamingFocusedTopic(object):

    def __init__(self, alpha_topic=None, epsilon_topic=None, shape=None,
                 scale=None, alpha_IBP=None, V=None, Ngibbs=None, gibbs_add_topics=None, 
                 pred_new=None,prune_thresh=None,burnin=None, prune_on=None, 
                 visualize_trial=None, test_frac=None, N_lik_samp=None,
                 word_test_frac=0.2):
        """
        alpha_topic - dirichlet prior on topic proportions
        epsilon_topic - threshold for throwing out new topics
        shape - shape prior on gamma rvs for per document topic weights
        rate -  rate prior on gamma rvs for per document topic weights
        alpha_IBP - IBP parameter
        V - vocabulary size
        Ngibbs - number of gibbs iterations
        gibbs_add_topics - number of topics to instantiate for each update inside sampler (before thresholding)(IBP hack)
        pred_new - the probability of a "new" topic being active in the gibbs sampler (IBP hack)
        prune_thresh - threshold for pruning existing topics.
        burnin - burnin for the gibbs sampler
        prune_on - toggles pruneing
        visualize_trial, if we want a visualization after every update step
        test_frac - for prediction, fraction of words to use for estimating doc param vs for prediction
        N_lik_samp - number times to sample features to compute the predictive logliklihoof
        word_test_frac : fraction of words in test doc to hold out
        """

        self.alpha_topic = alpha_topic
        self.epsilon_topic = epsilon_topic
        self.shape = shape
        self.scale = scale
        self.alpha_IBP = alpha_IBP
        self.n = 0
        self.V = V
        self.alpha_prior = np.ones(V)*self.alpha_topic
        self.nu = self.alpha_prior
        self.Ngibbs = Ngibbs
        self.K = 0
        self.gibbs_add_topics = gibbs_add_topics
        self.pred_z_u = []
        self.pred_z = []
        self.pred_new = pred_new
        self.prune_thresh = prune_thresh
        self.burnin = burnin
        self.prune_on = prune_on
        self.visualize_trial = visualize_trial
        self.when_created = []
        self.test_frac = test_frac
        self.loglik = []
        self.loglik_n = []
        self.N_lik_samp = N_lik_samp
        self.word_test_frac = word_test_frac
        self.word_train_frac = 1 - word_test_frac

        self.s_expectations_avg = []

    def update(self, x):
        #compute required expectations
        x = x.astype('int')
        self.rho, self.s_expectations, self.topic_count_avg = \
            self.gibbs_collapsed_fast(x)
            #self.gibbs_collapsed(x)

        #visualize the data from trial, if bars experiment
        if self.visualize_trial:
            plt.close('all')
            self.visualize(x)
            self.visualize_topics()
            plt.show()
            #raw_input()

        #add the current soft assignments to the unormalized version
        self.pred_z_u += self.rho[:self.K]

        #add new features that are above the threshold
        self.add_new_features()

        #update the mean field parameters
        self.mean_field_params(self.s_expectations, self.newfeat)

        #update the number of observations seen so far
        self.n = self.n + 1

        # Record statistics
        Es = np.sum(self.s_expectations, axis=1) / np.sum(x)
        # if self.K == 0:
        #     self.s_expectations_avg = Es[self.newfeat]
        # elif self.K > 0:
        #     self.s_expectations_avg[:self.K] += Es[:self.K]
        #     self.s_expectations_avg[self.K:] += Es[self.newfeat]

        #update number of features
        self.K = self.K + self.newfeat.shape[0]
        #print(self.pred_z_u)

        #feature predictions for the IBP to be used at the next time step
        self.pred_z = self.pred_z_u/(self.n + 1)

        #prune features. 
        if self.prune_on:
            self.prune_feat()
    def test_likelihood2(self,Xtest,Ntest):
        Eu = self.nu / np.sum(self.nu, axis=1)[:,np.newaxis]
        print np.sum(Eu,axis=1)
        Ev = self.shape*self.scale
        Ez = self.pred_z
        loglik = 0
        for i in xrange(Ntest):
            loglik += self.likelihoodcompute2(Ez, Ev, Eu, Xtest[i,:])
        return loglik

    def likelihoodcompute2(self,Ez,Ev,Eu,test_doc):
        rate = np.dot(Eu.T, Ev*Ez[:self.K])
        if rate[0] == 0:
            rate = np.ones(self.V)*(1e-100)
        return self.poisson_loglikelihood(test_doc, rate)

    def test_likelihood(self, Xtest_tr, Xtest_te):
        Ntest = Xtest_tr.shape[0]
        loglik = 0.
        Eu = self.nu / np.sum(self.nu, axis=1)[:,np.newaxis]

        s = 1./(1. + 1./self.scale)

        for i in xrange(Ntest):
            rho, s_expectations, topic_count_avg = \
                    self.gibbs_collapsed_fast(Xtest_tr[i,:].astype(np.int_))
                    #self.gibbs_collapsed(Xtest_tr[i,:])

            #posterior expectation of v
            Ev = (topic_count_avg + self.shape) * s
            loglik +=  self.likelihood_compute(rho, Ev, Eu, Xtest_te[i,:])

        #self.loglik.append(loglik)
        #self.loglik_n.append(self.n)

        return loglik

    def plot_test_lik(self):
        plt.figure()
        plt.plot(self.loglik_n,self.loglik)
        plt.show()

    def likelihood_compute(self, rho, Ev, Eu, test_doc):
        #to compute the test likelihood, we perform a monte carlo estimate. 
        #we randomly sample the features from the posterior feature assignments for the
        #the document, then compute the likelihood of the test document

        #scaling factor for the prediction
        factor = self.word_train_frac/self.word_test_frac
        Ev = Ev/factor
        loglik = 0

        #sample features from posterior over feature assignments, compute predictive
        #likelihood, then average
        for i in xrange(self.N_lik_samp):
            which_feat = np.random.rand(self.K) < rho[:self.K]
            dum = which_feat*Ev[:self.K]
            rate = np.dot(Eu.T, dum[:self.K]) 
            #check if the rate is 0, if it is set the rate to some arbitrarily small number.
            if rate[0] == 0:
                    rate = np.ones(self.V)*(1e-100)
            loglik += self.poisson_loglikelihood(test_doc, rate)

        return loglik / self.N_lik_samp

    def poisson_loglikelihood(self,test_doc, rate):
        loglik = np.sum(-rate + test_doc*np.log(rate) - np.log(sp.misc.factorial(test_doc)))
        return(loglik)

    def prune_feat(self):
        #Pruning here works by normalizing each unnormalized sum of soft assignments
        #by the number of iterations since that topic was created
        #if this number falls below some threshold, the topic is pruned
        dum = self.pred_z_u/(self.n - self.when_created)
        prune_these = dum < self.prune_thresh
        prune_these = np.where(prune_these)[0]
        num_prune = prune_these.shape[0]
        if num_prune > 0:
            #print("pruned")
            self.nu = np.delete(self.nu, prune_these, 0)
            self.pred_z_u = np.delete(self.pred_z_u, prune_these)
            self.when_created = np.delete(self.when_created, prune_these)
            self.pred_z = self.pred_z_u/(self.n + 1)
            self.K -= num_prune
            self.s_expectations_avg = np.delete(self.s_expectations_avg,
                                                prune_these)

    def add_new_features(self):
        #update the unnormalized predictive vector
        newfeat_ind = self.rho > self.epsilon_topic
        newfeat_ind[:self.K] = 0 
        newfeat = np.where(newfeat_ind)[0] #number of new features
        numnewfeat = newfeat.shape[0]

        if newfeat.shape[0] > 0:
            #add the new feature
            self.pred_z_u = np.hstack((self.pred_z_u, self.rho[newfeat]))
            self.s_expectations_avg = np.hstack((self.s_expectations_avg,
                                                 np.zeros(numnewfeat)))

            #update the variational parameters matrix
            new = np.ones([newfeat.shape[0], self.V])*self.alpha_topic
            self.when_created = np.hstack((self.when_created, self.n*np.ones(newfeat.shape[0])))
            #if statement for when these are the first topics vs not first topics
            if self.K > 0:
                self.nu = np.vstack((self.nu, new))
            else:
                self.nu = new

        self.newfeat = newfeat

    def mean_field_params(self, S, newfeat):
        #update parameters for past topics
        for i in xrange(self.K):
            self.nu[i,:] += S[i,:]

        #update parameters for new topics
        for i in xrange(newfeat.shape[0]):
            self.nu[self.K + i,:] += S[newfeat[i],:]

    # def test_data_separate(self, Xtest, seed=None):
    #     #function used to split the data set into two
    #     #groups to use to compute held out log likelihood. Basically
    #     #I just convert the word counts into a vector where each in a document
    #     #is stored in an entry in a vector, then randomly sample the words in the document 
    #     #by randomly sampling the indices of the vector
    #     #I then convert back to word counts.

    #     if seed is not None:
    #         np.random.seed(seed)

    #     V = self.V
    #     Ntest = Xtest.shape[0]

    #     Xtest_te = np.zeros((Ntest, V))
    #     Xtest_tr = np.zeros((Ntest, V))

    #     for i in xrange(Ntest):
    #         num_words = int(np.sum(Xtest[i,:]))
    #         #convert into a vector of words
    #         doc = np.zeros(num_words)
    #         count = 0
    #         for j in xrange(V):
    #             nw = Xtest[i,j]
    #             doc[count:count+nw] = j
    #             count += nw

    #         num_train_words = np.round(self.word_train_frac*num_words)
    #         num_test_words = int(num_words - num_train_words)
    #         which_test = np.random.choice(num_words,
    #                                       size=num_test_words, replace=False)
    #         test_doc = doc[which_test]
    #         train_doc = np.delete(doc, which_test)

    #         #convert back into word counts
    #         for j in xrange(self.V):
    #             num_test = np.where(test_doc == j)[0].shape[0]
    #             num_train = np.where(train_doc == j)[0].shape[0]
    #             Xtest_te[i,j] = num_test
    #             Xtest_tr[i,j] = num_train

    #     return Xtest_te, Xtest_tr

    def gibbs_collapsed(self, x):
        #collapsed gibbs sampler 

        #total number of features to consider on this round (arbitrary truncation)
        totalK = self.K + self.gibbs_add_topics

        #create document for sampler
        num_words = int(np.sum(x))
        doc = np.zeros(num_words) #document representation

        #add words to document
        count = 0 
        for i in xrange(V):
            nw = x[i]
            doc[count:count+nw] = i
            count += nw

        aug = self.pred_new*np.ones(self.gibbs_add_topics)
        pred_z = np.hstack((self.pred_z, self.pred_new*np.ones(self.gibbs_add_topics)))
        ztotal = np.zeros(totalK)
        zcurr = np.zeros(totalK)
        S = np.zeros([totalK, self.V]) #collected counts of word to topic assignments
        topic_count_avg = np.zeros(totalK)
        #sample instantiated zs
        zcurr[:self.K] = np.random.rand(self.K) < self.pred_z
        zcurr[0] = 1
        zcurr[1] = 1

        #randomly initialize word assignments
        whichon = np.where(zcurr)[0]
        bcurr = np.random.choice(whichon, num_words)
        
        word_counts = np.zeros([totalK, self.V])
        if self.K > 0:
            word_counts[:self.K,:] = self.nu

        word_counts[self.K:totalK, :] = self.alpha_topic
        topic_counts = np.ones(totalK)*self.shape 

        for i in xrange(num_words):
            topic = bcurr[i]
            word = doc[i]
            word_counts[topic, word] += 1
            topic_counts[topic] += 1

        #begin sampler
        for i in xrange(self.Ngibbs):
            #print(zcurr)
            #sample topic assignments for each word
            active_z = np.where(zcurr)[0]
            for j in xrange(num_words):
                topic = bcurr[j]
                word = doc[j]

                #forget current assignment
                word_counts[topic,word] -= 1
                topic_counts[topic] -= 1 

                #compute probabilities 
                pword = np.zeros(active_z.shape[0])
                ptopic = topic_counts[active_z]/np.sum(topic_counts[active_z])
                for l in xrange(active_z.shape[0]):
                    topic_ind = active_z[l]
                    pword[l] = word_counts[topic_ind,word]/np.sum(word_counts[topic_ind,:])

                #compute using bayes rule
                lik_prior = pword*ptopic
                #normalize
                cond_topic = lik_prior/np.sum(lik_prior)

                #sample topic
                new_topic = np.random.multinomial(1,cond_topic)
                new_topic = np.where(new_topic)[0][0]
                new_topic = active_z[new_topic]
                bcurr[j] = new_topic

                #update counts//
                topic_counts[new_topic] += 1
                word_counts[new_topic, word] += 1

                #add counts to list
                if i > self.burnin:
                    S[new_topic,word] += 1
                    topic_count_avg[new_topic] += 1

            #sample vs
            vcurr = np.random.gamma(topic_counts,1./(1. + 1./self.scale))

            #sample zs
            ##determine which zs currently have no data assigned to them
            samplez = topic_counts == self.shape
            samplez = np.where(samplez)[0]

            #sample these zs from their conditional distirbution
            for j in samplez:
                p1 = np.exp(-vcurr[j])*pred_z[j]
                p0 = 1 - pred_z[j]
                p = p1/(p1 + p0)
                zcurr[j] = np.random.rand(1) < p
            if i > self.burnin:
                ztotal += zcurr

        #number of samples
        den = self.Ngibbs - self.burnin
        return ztotal/den, S/den, topic_count_avg/den

    def visualize_topics(self):
        #fig, ax = plt.subplots(1,self.K)
        print(self.K)
        print(self.nu.shape)
        if self.K > 0:
            fig, ax = plt.subplots(2,self.K + self.gibbs_add_topics)
            for k in xrange(self.K):
                f = self.nu[k,:].reshape((8,8))
                ax[0,k].imshow(f, cmap='Greys')
                ax[0,k].set_title(self.rho[k])
            for k in xrange(self.K + self.gibbs_add_topics):
                f = self.s_expectations[k,:].reshape((8,8))
                ax[1,k].imshow(f, cmap='Greys')
                ax[1,k].set_title(self.topic_count_avg[k])
        #else:
            #f = self.nu[0,:].reshape((8,8))
            #plt.imshow(f,cmap='Greys')

    def visualize_topics_end(self):
        plt.figure
        print(self.K)
        print(self.nu.shape)
        nrow = int(np.ceil(np.sqrt(self.K)))
        if self.K > 0:
            for k in xrange(self.K):
                f = self.nu[k,:].reshape((8,8))
                plt.subplot(nrow, nrow, k+1)
                plt.pcolor(np.flipud(f))
                plt.gray()
                plt.axis('off')
                plt.axis('equal')
                plt.title("%d: %.2f" % (k, self.pred_z[k]))
        plt.show()

    def visualize(self, x):
        plt.figure()
        f = x.reshape((8,8))
        #plt.imshow(f, cmap='Greys')
        plt.pcolor(np.flipud(f))
        plt.gray()
        plt.axis('off')
        plt.axis('equal')
        plt.show()

# Add cython optimized collapsed gibbs sampling method.
StreamingFocusedTopic.gibbs_collapsed_fast = \
    types.MethodType(topicstream_fast.gibbs_collapsed, None,
                     StreamingFocusedTopic)




#code to test streaming interface
if __name__ == "__main__":

    np.seterr(invalid='raise')

    test_frac = .25
    alpha_topic = .25
    epsilon_topic = .95
    shape = 5
    scale = 1
    alpha_IBP = 1.
    V = 8*8
    Ngibbs = 100
    gibbs_add_topics = 2
    mult_factor = 1000
    prob_topon = .25
    pred_new = .1
    baseline_prob = .000003
    prune_thresh = .1
    burnin = 50
    prune_on = True
    visualize_trial = False
    ntrain = 400
    ntest = 10
    lik_interval = 40
    N_lik_samp = 10

    x, topics_on, topics = make_8x8_bars_disc(N=ntrain + ntest,non=8,
                                              wordspertopic=100,
                                              prob_topon=prob_topon,
                                              baseline_prob=baseline_prob)

    xtrain = x[:ntrain,:]
    xtest = x[ntrain:,:]

    stream = StreamingFocusedTopic(alpha_topic=alpha_topic, epsilon_topic=epsilon_topic, shape=shape,
            scale=scale, alpha_IBP=alpha_IBP, V=V, Ngibbs=Ngibbs, gibbs_add_topics=gibbs_add_topics,
            pred_new=pred_new, prune_thresh=prune_thresh, burnin=burnin, prune_on=prune_on,
            visualize_trial=visualize_trial, test_frac=test_frac, N_lik_samp=N_lik_samp)

    #split test data into words for inference and words for prediction.
    Nlocalthin = 10
    Nlocalgibbs = 50
    Nlocalburnin = 50
    word_train_frac = .25
    Xtete, Xtetr = test_data_separate(Xtest=xtest,word_train_frac=word_train_frac)





    #print(xtest_tr[1,:])
    #print(xtest_te[1,:])

    #stream.K = 0
    #stream.nu = topics
    #stream.

    #uncomment/modulate to initialize the algorithm with some correct features
    # stream.K = 8
    # stream.nu = np.random.rand(stream.K,stream.V)
    # #stream.nu = topics[:stream.K,:]*mult_factor
    # stream.pred_z_u = np.ones(stream.K)
    # stream.when_created = np.ones(stream.K)
    # stream.pred_z = stream.pred_z_u/2
    # stream.n = 1
    loglik = []
    for i in xrange(ntrain):
        print(i)
        stream.update(x[i,:])
        #print(stream.pred_z)
        #print(stream.rho)

        if i > 0 and i % lik_interval == 0:
            #full document prediction
            #lik = stream.test_likelihood2(xtest, ntest)

            #partial document prediction
            #lik = stream.test_likelihood(Xtetr,Xtete)

            U = stream.nu / np.sum(stream.nu, axis=1)[:,np.newaxis]
            U = U.T

            lik = loglik_maskeddocs(Xtete=Xtete,Xtetr=Xtetr,U=U,pi=stream.pred_z,shape=stream.shape,rate=1./stream.scale,
                Nlocalgibbs=Nlocalgibbs,Nlocalburnin=Nlocalburnin,Nlocalthin=Nlocalthin,ntest=ntest,K=stream.K,nV = stream.V)

            loglik.append(lik)
        #uncomment to see results from each update step.
        #stream.visualize(x[i,:])
        #plt.show()

        #print(stream.rho)
        #print(stream.K)
            

    stream.visualize_topics_end()

    #stream.plot_test_lik()
    plt.figure()
    plt.plot(loglik)
    plt.show()
    print(loglik)


# #code to test streaming interface(OLD LIKELIHOOD METHOD)
# if __name__ == "__main__":

#     np.seterr(invalid='raise')

#     test_frac = .2
#     alpha_topic = .25
#     epsilon_topic = .95
#     shape = 5
#     scale = 1
#     alpha_IBP = 1.
#     V = 8*8
#     Ngibbs = 100
#     gibbs_add_topics = 2
#     mult_factor = 1000
#     prob_topon = .25
#     pred_new = .1
#     baseline_prob = .000003
#     prune_thresh = .05
#     burnin = 50
#     prune_on = True
#     visualize_trial = False
#     ntrain = 4000
#     ntest = 100
#     lik_interval = 200
#     N_lik_samp = 10

#     x, topics_on, topics = make_8x8_bars_disc(N=ntrain + ntest,non=8,
#                                               wordspertopic=100,
#                                               prob_topon=prob_topon,
#                                               baseline_prob=baseline_prob)

#     xtrain = x[:ntrain,:]
#     xtest = x[ntrain:,:]

#     stream = StreamingFocusedTopic(alpha_topic=alpha_topic, epsilon_topic=epsilon_topic, shape=shape,
#             scale=scale, alpha_IBP=alpha_IBP, V=V, Ngibbs=Ngibbs, gibbs_add_topics=gibbs_add_topics,
#             pred_new=pred_new, prune_thresh=prune_thresh, burnin=burnin, prune_on=prune_on,
#             visualize_trial=visualize_trial, test_frac=test_frac, N_lik_samp=N_lik_samp)

#     #split test data into words for inference and words for prediction.
#     xtest_te, xtest_tr = stream.test_data_separate(xtest)

#     print(xtest_tr[1,:])
#     print(xtest_te[1,:])

#     #stream.K = 0
#     #stream.nu = topics
#     #stream.

#     #uncomment/modulate to initialize the algorithm with some correct features
#     #stream.K = 2
#     #stream.nu = topics[:stream.K,:]*mult_factor
#     #stream.pred_z_u = np.ones(stream.K)
#     #stream.when_created = np.ones(stream.K)
#     #stream.pred_z = stream.pred_z_u/2
#     #stream.n = 1
#     loglik = []
#     for i in xrange(ntrain):
#         print(i)
#         stream.update(x[i,:])
#         #print(stream.pred_z)
#         #print(stream.rho)

#         if i > 0 and i % lik_interval == 0:
#             lik = stream.test_likelihood(xtest_tr, xtest_te)
#             loglik.append(lik)
#         #uncomment to see results from each update step.
#         #stream.visualize(x[i,:])
#         #plt.show()

#         #print(stream.rho)
#         #print(stream.K)
            

#     stream.visualize_topics_end()

#     #stream.plot_test_lik()
#     plt.figure()
#     plt.plot(loglik)
#     plt.show()
#     print(stream.loglik)


#code to test the collapsed gibbs sampler
#if __name__ == "__main__":    
#  alpha_topic = .2
#  epsilon_topic = .08
#  shape = 5
#  scale = 1
#  alpha_IBP = 1.
#  V = 8*8
#  Ngibbs = 100
#  gibbs_add_topics = 5
#  mult_factor = 100000000
#  prob_topon = 1
#  pred_new = .1
#  baseline_prob = .000003
#
#  ###
#  x,topics_on, topics = make_8x8_bars_disc(10,3,100,prob_topon,baseline_prob)
#
#  stream = StreamingFocusedTopic(alpha_topic=alpha_topic, epsilon_topic=epsilon_topic, shape=shape,
#      scale=scale, alpha_IBP=alpha_IBP, V=V, Ngibbs=Ngibbs, gibbs_add_topics=gibbs_add_topics,
#      pred_new=pred_new)
#
#  stream.K = 2
#  stream.nu = topics[:2,:]*mult_factor
#  stream.pred_z = .5*np.ones(stream.K)
#
#  print(x[0,:])
#  print(topics[0,:])
#  print(stream.nu[0,:])
#
#
#  Ez, Es =stream.gibbs_collapsed(x[1,:])




    # def gibbs_uncollapsed(self, x):
    #       totalK = self.K + self.gibbs_add_topics

    #       #####gibbs sampler for the focused topic model
    #       ####first sample each parameter from the its prior
    #       zcurr = np.zeros(totalK)
    #       #sample instantiated zs
    #       zcurr[:self.K] = np.random.randn(self.K) < self.pred_z

    #       #sample Us
    #       Ucurr = np.zeros(totalK, self.V)
    #       for i in xrange(self.K):
    #               Ucurr[i,:] = np.random.dirichlet(self.nu[i,:])
    #       for i in xrange(self.K, totalK):
    #               Ucurr[i,:] = np.random.dirichlet(self.alpha_prior)

    #       #sample vs
    #       vcurr = np.random.gamma(self.shape, self.scale, totalK)


    #       Scurr = np.zeros(totalK, V)

    #       ####begin gibbs sampler
    #       for i in xrange(Ngibbs):

    #               ###first sample the vector of assignmetns of words to topics
    #               for j in xrange(V):
    #                       Scurr[:,j] = np.random.multinomial(x[j], Ucurr[:,j]*vcurr*zcurr)

    #               ###sample Us
    #               for j in xrange(self.K):
    #                       Ucurr[j,:] = np.dirichlet(Scurr[j,:] + self.nu[j,:])
    #               for j in xrange(self.K, totalK):
    #                       Ucurr[j,:] = np.dirichlet(Scurr[j,:] + self.nu[j,:])

    #               ###sample vs
    #               topic_sums = np.sum(Scurr, 1)
    #               vcurr = np.random.gamma(self.shape + np.sum(Scurr, 1),1./(1. + 1./self.scale))

    #               ##sample old z
    #               samplez = topic_sums == 0
    #               samplez[self.K, totalK] = False
    #               samplez = np.where(samplez)[0]

    #               for j in samplez:
    #                       p1 = np.exp(vcurr[j])*np.pred[j]
    #                       p0 = 1 - np.pred[j]
    #                       p = p1/(p1 + p0)
    #                       zcurr[j] = np.random.randn < p

