from __future__ import division
import types
import numpy as np
import scipy as sp
import scipy.stats as ss
import scipy.optimize as spopt
from util import make_8x8_bars_disc, test_data_separate
from topic_samplers import loglik_maskeddocs, sampleS_unc, sampleZ_unc, sampleV_unc



import matplotlib.pyplot as plt
eps = np.finfo('float64').eps

class GibbsFocusedTopic(object):
    
    def __init__(self, Xtrain=None, Xtest=None, alpha_topic=None, shape=None, scale=None, alpha_IBP=None, nV=None, Ngibbs=None,
        burnin=None, thin=None, K=None,likevery=None,Nliksamp=None,liktest=None,localthin=None,
        Nlocalburnin=None,Nlocalgibbs=None,proposal_var=None,imp_samples=None,localvtomean=None,use_local_means=None,
        Xtete=None,Xtetr=None):
        """
        alpha_topic - dirichlet prior on topic proportions
        shape - shape parameter for gamma
        scale - scale parameter for gamma
        alpha_IBP - ibp parameter
        V - size of vocabulary
        Ngibbs - number of gibbs iterations (after burnin)
        burnin - number of burnin iterations """


        self.Xtete=Xtete
        self.Xtetr=Xtetr
        self.use_local_means = use_local_means
        self.imp_samples=imp_samples
        self.proposal_var=proposal_var
        self.localthin = localthin
        self.Nlocalgibbs = Nlocalgibbs
        self.Nlocalburnin = Nlocalburnin
        self.liktest=liktest
        self.Nliksamp=Nliksamp
        self.ntrain = Xtrain.shape[0]
        self.ntest = Xtest.shape[0]
        self.alpha_topic = alpha_topic
        self.shape = shape
        self.scale = scale
        self.alpha_IBP = alpha_IBP
        self.nV = nV
        self.Ngibbs = Ngibbs
        self.burnin = burnin
        self.thin = thin
        self.Xtrain = Xtrain
        self.Xtest = Xtest
        self.K = K
        self.rate = 1./self.scale
        self.test_loglik = []
        self.train_loglik = []
        self.pi = np.empty(K)
        self.U = np.empty((nV,K))
        self.V = np.empty((self.ntrain,K))
        self.S = np.empty((self.ntrain,nV,K))
        self.pi_sum = np.zeros(K)
        self.U_sum = np.zeros((nV,K))
        self.counts = 0
        self.when_lik_compute = []
        self.likevery = likevery
        self.localvtomean = localvtomean

        #randomly initialize some of the parameters
        #initialize pi
        for k in xrange(K):
            self.pi[k] = np.random.beta(1, 1)

        #intialize Z
        self.Z = np.random.rand(self.ntrain,self.K) < self.pi[:,np.newaxis].T

        #initialize V
        for i in xrange(self.ntrain):
            for k in xrange(self.K):
                self.V[i,k] = np.random.gamma(self.shape,self.scale)

        #initialize U
        dparam = self.alpha_topic * np.ones(self.nV)
        for k in xrange(self.K):
            self.U[:,k] = np.random.dirichlet(dparam)

        #itialize S
        self.S = self.sampleS(self.U, self.Z, self.V, self.S,self.Xtrain, self.ntrain)
        self.topiccounts = np.sum(self.S,1)
        self.wordcounts = np.sum(self.S,0).T
        self.topictotals = np.sum(self.Z,0)



    def run_sampler(self, burnin,Ngibbs):
        #initialize sampler
        #Z is an n x K binary matrix
        #U is a V x K matrix where the columns sum to one
        #V is a n x K matrix of positive entries
        #S is n x V x K matrix of counts
        #X is n x V matrix of counts
        #topiccounts is a n x K matrix of counts
        #wordcounts is a K x V matrix of counts
        #topictotals

        pi = self.pi
        U = self.U
        V = self.V
        S = self.S
        Z = self.Z
        topiccounts = self.topiccounts
        wordcounts = self.wordcounts
        topictotals = self.topictotals

        for i in xrange(burnin + Ngibbs):
            pi,Z, U, V, S, topiccounts, wordcounts, topictotals = self.FTM_sample(pi,Z,U,V,S,topiccounts, wordcounts,topictotals)

            if i > burnin and (i - burnin) % self.thin == 0:
                self.counts += 1
                self.pi_sum += pi
                self.U_sum += U

            if i > burnin and (i - burnin) % self.likevery == 1:
                print("iteration: %d" % i)
                #likte = self.loglik_meanpar(self.Xtest,pi,U,self.ntest)
                if self.liktest:
                    #likte = self.loglik_exact(self.Xtest,pi,U,self.ntest)
                    likte = loglik_maskeddocs(self.Xtete,self.Xtetr,U,pi,self.shape,self.rate,self.Nlocalgibbs
                        ,self.Nlocalburnin,self.localthin,self.ntest,self.K,self.nV)
                    #likte = self.loglik_stochastic(self.Xtest,pi,U,self.ntest)
                    #likte = self.loglik_importance(self.Xtest,U,pi,Z,V)
                    #likte = self.loglik_meanpar(self.Xtest,pi,U,self.ntest)

                    print("test lik: %d" % likte)
                    self.test_loglik.append(likte)

                #lik = self.loglik_currpar(self.Xtest,Z,U,V,self.ntest)
                liktr = self.loglik_training(self.Xtrain,Z,U,V,self.ntrain)
                print("train lik: %d" % liktr)
                self.train_loglik.append(liktr)
                self.when_lik_compute.append(i)


        self.pi = pi
        self.Z = Z
        self.U = U
        self.V = V
        self.S = S
        self.topiccounts = topiccounts
        self.wordcounts = wordcounts
        self.topictotals = topictotals

    def loglik_stochastic(self,Xtest,pi,U,Ntest):
        Ev = self.scale*self.shape
        loglik_total = 0
        for i in xrange(Ntest):
            loglik_local = np.zeros(self.Nliksamp)
            for j in xrange(self.Nliksamp):
                z = np.zeros(self.K)
                while np.sum(z) == 0:
                    z = np.random.rand(self.K) < pi
                loglik_local[j] = self.likelihoodcompute(z,Ev,U,Xtest[i,:]) + np.log(1./self.Nliksamp)
            loglik_total += sp.misc.logsumexp(loglik_local)
        return(loglik_total)

    def loglik_exact(self,Xtest,pi,U,Ntest):
        Ev = self.scale*self.shape
        loglik_total = 0
        totcomb = 2**self.K
        for i in xrange(Ntest):
            loglik_local = np.zeros(totcomb - 1)
            for j in xrange(1,totcomb):
                z = self.trans(j,self.K)
                on = np.where(z == 1)
                off = np.where(z == 0)
                lpon = np.sum(np.log(pi[on]))
                lpoff = np.sum(np.log(1 - pi[off]))
                loglik_local[j - 1] = self.likelihoodcompute(z,Ev,U,Xtest[i,:]) + lpon + lpoff
            loglik_total += sp.misc.logsumexp(loglik_local)
        return(loglik_total)

    def trans(self,x,length):
        bit = np.zeros(length)
        if x == 0: return(bit)
        for i in xrange(length):
            bit[i] = x % 2
            x >>= 1
        return(bit[::-1])

    def plot_test_loglik(self):
        if self.liktest:
            plt.figure()
            plt.plot(self.when_lik_compute,self.test_loglik)

        plt.figure()
        plt.plot(self.when_lik_compute,self.train_loglik)

        plt.show()

    def loglik_training(self,X,Z,U,V,Ntrain):
        Ev = self.scale*self.shape;
        loglik = 0
        for i in xrange(self.ntrain):
            #print(i)
            loglik += self.likelihoodcompute(Z[i,:],V[i,:],U,X[i,:])
            #loglik += self.likelihoodcompute(Z[i,:],Ev,U,X[i,:])
        return loglik

    def loglik_meanpar(self,Xtest,pi_sum,U_sum,Ntest):
        Ev = self.scale*self.shape
        #Eu = self.U_sum/self.counts
        #Epi = self.pi_sum/self.counts
        Eu = U_sum
        Epi = pi_sum

        loglik = 0
        for i in xrange(Ntest):
            loglik += self.likelihoodcompute(Epi, Ev, Eu, Xtest[i,:])
        return loglik

    def loglik_currpar(self,Xtest,Z,U,V,Ntest):
        loglik = 0
        for i in xrange(Ntest):
            print(i)
            loglik += self.likelihoodcompute(Z[i,:],V[i,:],U,Xtest[i,:])
        return loglik

    def visualize_top_topics_end(self):
        #top 16 topics
        ntop = 16
        plt.figure
        nrow = int(np.ceil(np.sqrt(ntop)))
        top = np.argsort(self.pi)[:ntop]
        counter = 1
        for k in top:
            f = self.U[:,k].reshape((8,8))
            plt.subplot(nrow, nrow, counter)
            plt.pcolor(np.flipud(f))
            plt.gray()
            plt.axis('off')
            plt.axis('equal')
            plt.title("%d: %.2f" % (k, self.pi[k]))
            #plt.show()
            counter += 1
        plt.show()


    def likelihoodcompute(self,Epi,Ev,Eu,test_doc):
        #print Epi.shape
        #print Ev.shape
        #print Eu.shape
        rate = np.dot(Eu, Ev*Epi)
        #print(rate)
        #print(rate)
        if np.max(rate) == 0:
            print "fart"
            rate = np.ones(self.nV)*(1e-100)
        return self.poisson_loglikelihood(test_doc, rate)

    def poisson_loglikelihood(self,test_doc, rate):
        #print rate
        loglik = np.sum(-rate + test_doc*np.log(rate) - np.log(sp.misc.factorial(test_doc)))
        return(loglik)


    def FTM_sample(self,pi,Z,U,V,S,topiccounts,wordcounts,topictotals):
        #first sample the V entries

        V = self.sampleV(topiccounts,self.ntrain,V)

        #sample pi
        for k in xrange(self.K):
            a = 1./self.K + topictotals[k]
            b = 1 - 1./self.K + self.ntrain - topictotals[k]
            pi[k] = np.random.beta(a,b)

        #sample S
        S = self.sampleS(U,Z,V,S,self.Xtrain,self.ntrain)
                #compute topiccounts, wordcounts, and topictotals
        topiccounts = np.sum(S,1)
        wordcounts = np.sum(S,0).T
        topictotals = np.sum(Z,0)

        #sample Z
        #Z = self.sampleZ(Z,U,V,pi,self.Xtrain,self.ntrain)
        Z = self.sampleZ2(Z,V,pi,topiccounts,self.ntrain)

        #compute topiccounts, wordcounts, and topictotals
        topiccounts = np.sum(S,1)
        wordcounts = np.sum(S,0).T
        topictotals = np.sum(Z,0)

        #sample the U  entries
        #print(wordcounts.shape)
        for k in xrange(self.K):
            alphanew = self.alpha_topic + wordcounts[k,:]
            U[:,k] = np.random.dirichlet(alphanew)

        return(pi,Z, U, V, S, topiccounts, wordcounts, topictotals)

    def sampleV(self,topiccounts,n,V):
        for i in xrange(n):
            for k in xrange(self.K):
                #parameters
                #print(i,k)
                shapenew = self.shape + topiccounts[i,k]
                ratenew = self.rate + 1
                scalenew = 1./ratenew
                V[i,k] = np.random.gamma(shapenew,scalenew)
        return V

    def sampleS(self,U,Z,V, S,X,n):
        for i in xrange(n):
            for j in xrange(self.nV):
                p = U[j,:]*Z[i,:]*V[i,:]
                #print(p)
                if np.max(p) == 0:
                    p == np.ones(self.K)*1./self.K
                else:
                    p /= np.sum(p)
                S[i,j,:] = np.random.multinomial(X[i,j],p)
        return S

    def sample_mult(n,p):
        z = np.zeros(self.K)
        for i in xrange(n):
            q = np.random.rand(self.K) < p
            z += q
        return(z)

    def sampleZ(self,Z,U,V,pi,X,n):
        #print 'fart'
        #print Z.shape
        #print V.shape
        #print X.shape
        for i in xrange(n):
            for k in xrange(self.K):
                zhere1 = Z[i,:].copy()
                zhere1[k] = True
                zhere0 = Z[i,:].copy()
                zhere0[k] = False
                #print zhere0*V[i,:]
                rate1 = np.dot(U,zhere1*V[i,:])


                if np.sum(zhere0) == 0:
                    rate0 = np.ones(self.nV)*eps
                else:
                    rate0 = np.dot(U,zhere0*V[i,:])



                #print(zhere1)
                #print rate1

                #print(rate1.size)
                logp1 = np.log(pi[k])
                logp0 = np.log(1 - pi[k])
                #print(i)
                #print X[i,:]
                logp1 += self.poisson_loglikelihood(X[i,:], rate1)
                logp0 += self.poisson_loglikelihood(X[i,:], rate0)
                #print logp1
                #print logp0
                maxl = max(logp1, logp0)
                logp1 -= maxl
                logp0 -= maxl
                p1 = np.exp(logp1)/(np.exp(logp0) + np.exp(logp1))
                Z[i,k] = np.random.rand() < p1
        return Z

    def sampleZ2(self,Z,V,pi,topiccounts,n):
        for i in xrange(n):
            #which zs are active
            nowords = np.where(topiccounts[i,:] == 0)[0]
            for k in nowords:
                p0 = 1 - pi[k]
                p1 = np.exp(-V[i,k])*pi[k]
                #maxl = max(logp1,logp0)
                #logp1 -= maxl
                #logp0 -= maxl
                #p1 = np.exp(logp1)/(np.exp(logp0) + np.exp(logp1))
                p1 = p1/(p1 + p0)
                Z[i,k] = np.random.rand() < p1
        return Z


    def local_sampler(self,x,U,pi):
        tsamp = int(self.Nlocalgibbs/self.localthin) - 1
        Vsamp = np.empty((tsamp,self.K))
        #initialize the Vs
        V = np.ones((1,self.K))*self.shape*self.scale
        #initialize the Zs
        Z = np.random.rand(1,self.K) < pi
        Ztotal = np.zeros((1,self.K))
        #initialize S
        S = np.zeros((1,self.nV, self.K))
        samp_count = 0
        #####begin local sampler#####

        for i in xrange(self.Nlocalgibbs + self.Nlocalburnin):
            #Z = self.sampleZ(Z,U,V,pi,x[np.newaxis,:],1)
            S = self.sampleS(U,Z,V,S,x[np.newaxis,:],1)
            topiccounts = np.sum(S,1)
            Z = self.sampleZ2(Z,V,pi,topiccounts,1)
            #print topiccounts.shape
            if self.localvtomean:
                V = self.shape*self.scale*np.ones((1,self.K))
            else:
                V = self.sampleV(topiccounts,1,V)
            if i > self.Nlocalburnin and (i - self.Nlocalburnin) % self.localthin == 0:
                Ztotal += Z
                Vsamp[samp_count,:] = V
                samp_count += 1
        return(Ztotal,Vsamp,samp_count)




    def loglik_importance(self,Xtest,U,pi,Z,V):
        Ev = self.shape*self.scale
        sdV = np.sqrt(self.shape*self.scale*self.scale)
        llik_total = 0
        print pi
        #sample local variables
        for i in xrange(self.ntest):
            #print(i)
            Ztotal,Vsamp,samp_count = self.local_sampler(Xtest[i,:],U,pi)
            q_z = Ztotal/samp_count
            qzs = np.squeeze(q_z)
            q_v_mu = np.sum(Vsamp,0)/samp_count
            #Epi,Ev,Eu,test_doc
            qzs = np.squeeze(q_z.T)



            #likthis = self.likelihoodcompute(qzs,q_v_mu,U,Xtest[i,:])
            #likthat = self.likelihoodcompute(Z[i,:],V[i,:],U,Xtest[i,:])

            #llik_total += likthis


            
            q_scale = self.proposal_var/q_v_mu
            q_shape = q_v_mu/q_scale

            if self.use_local_means:
                z = q_v_mu > 10
                q_v_mu = z*q_v_mu
                #print q_v_mu 
                llik_total += self.likelihoodcompute(q_z.T,q_v_mu[np.newaxis,:].T,U,Xtest[i,:])
            else:
                #randomly sample the vs and the zs.
                llik = np.empty(self.imp_samples)



                for j in xrange(self.imp_samples):
                    z = np.random.rand(self.K) < qzs

                    if self.localvtomean:
                        v = Ev*np.ones(self.K)
                    else:
                        v = np.random.gamma(q_shape, q_scale)
                    #likelihood of test data under sample
                    #print z.shape
                    #print v.shape
                    llik[j] = self.likelihoodcompute(z,v,U,Xtest[i,:]) + np.log(1./self.imp_samples)
                    #add the importance sampling factors, first for v
                    #if not self.localvtomean:

                    llik[j] += np.sum(ss.gamma.logpdf(v,self.shape,scale = self.scale) - ss.gamma.logpdf(v,q_shape,scale = q_scale))
                    #then for z
                    which0 = np.squeeze(z) == 0
                    which1 = np.squeeze(z) == 1
                    llik[j] += np.sum(np.log(pi[which1])) + np.sum(np.log(1 - pi[which0]))
                    llik[j] -= np.sum(np.log(qzs[which1]))
                    llik[j] -= np.sum(np.log(1 - qzs[which0]))
                llik_total += sp.misc.logsumexp(llik)
        return llik_total

#code to test streaming interface
if __name__ == "__main__":
    np.seterr(invalid='raise')
    alpha_topic = .25
    shape = 5
    scale = 1
    alpha_IBP = 1.
    nV = 8*8
    Ngibbs = 100
    baseline_prob = .0005
    burnin = 0
    likevery = 10
    thin = 5
    visualize_trial = False
    ntrain = 100
    ntest = 10
    K = 8
    prob_topon = .25
    Nliksamp = 1000
    liktest = True
    localthin = 10
    Nlocalburnin = 50
    Nlocalgibbs = 50
    proposal_var = 10
    imp_samples = 200
    localvtomean = False
    use_local_means = False
    word_train_frac = .25


    x, topics_on, topics = make_8x8_bars_disc(N=ntrain + ntest,non=8,
                                              wordspertopic=50,
                                              prob_topon=prob_topon,
                                              baseline_prob=baseline_prob)

    xtrain = x[:ntrain,:]
    xtest = x[ntrain:,:]

    Xtete, Xtetr = test_data_separate(xtest, word_train_frac=word_train_frac)

    gibbs = GibbsFocusedTopic(Xtrain=xtrain,Xtest=xtest,alpha_topic=alpha_topic,shape=shape,
        scale=scale,alpha_IBP=alpha_IBP,nV=nV,thin=thin,K=K,likevery=likevery,Nliksamp=Nliksamp,liktest=liktest,
        localthin=localthin,Nlocalburnin=Nlocalburnin,Nlocalgibbs=Nlocalgibbs,proposal_var=proposal_var,
        imp_samples=imp_samples,localvtomean=localvtomean,use_local_means=use_local_means,Xtete=Xtete, Xtetr=Xtetr)




    #gibbs.U[:,:8] = topics[:8,:].T
    #gibbs.pi[:] = .25
    #gibbs.U[:,:8]= .125
    gibbs.testtopicson = topics_on[ntrain:,:]
    print(topics_on[ntrain:,:])
    gibbs.run_sampler(burnin=burnin,Ngibbs=Ngibbs)
    gibbs.visualize_top_topics_end()
    gibbs.plot_test_loglik()






