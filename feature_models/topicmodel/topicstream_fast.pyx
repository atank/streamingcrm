
# Optimized functions for topicstream module

import numpy as np
cimport numpy as np

import cython
cimport cython

from libc.stdlib cimport rand, RAND_MAX

cdef extern from "math.h":
    double exp(double x)
    double pow(double x, double a)


@cython.boundscheck(True)
@cython.wraparound(False)
@cython.cdivision(True)
cdef int rand_discrete(int K, double* p):
    cdef double r = rand() / <double>RAND_MAX
    cdef double rsum = 0.
    cdef int i
    for i in xrange(K):
        rsum += p[i]
        if r <= rsum:
            return i


#@cython.profile(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def gibbs_collapsed(self, np.ndarray[np.int_t, ndim=1] x):
    """ Collapsed Gibbs sampler for gamma-Poisson topic model.
        
        x : D-vector of word counts
    """

    # Loop variables
    cdef int i, j, k, l
    cdef int V = self.V
    cdef int K = self.K
    cdef int gibbs_add_topics = self.gibbs_add_topics
    cdef int Ngibbs = self.Ngibbs
    cdef int burnin = self.burnin
    cdef double den
    cdef double shape = self.shape  # gamma shape param
    cdef double scale = self.scale
    cdef double rate = 1. / scale
    cdef double alpha_topic = self.alpha_topic
 
    #total number of features to consider on this round (arbitrary truncation)
    cdef int totalK = K + gibbs_add_topics
    
    # create document for sampler
    cdef int num_words = np.sum(x)
    cdef np.ndarray[np.int_t, ndim=1] doc = np.empty(num_words, dtype=np.int_)
    
    # Expand document counts to token representation
    cdef int idx = 0 
    cdef int nw
    for i in xrange(V):
        nw = x[i]
        for j in xrange(nw):
            doc[idx] = i
            idx += 1
    
    cdef np.ndarray[np.double_t, ndim=1] aug
    cdef np.ndarray[np.double_t, ndim=1] pred_z
    # Can two below be ints?
    cdef np.ndarray[np.double_t, ndim=1] ztotal
    cdef np.ndarray[np.double_t, ndim=1] zcurr

    cdef np.ndarray[np.int_t, ndim=2] S
    cdef np.ndarray[np.double_t, ndim=1] topic_count_avg

    aug = self.pred_new*np.ones(self.gibbs_add_topics)
    pred_z = np.hstack((self.pred_z, self.pred_new*np.ones(gibbs_add_topics)))
    ztotal = np.zeros(totalK)
    zcurr = np.zeros(totalK)
    S = np.zeros((totalK, self.V), dtype=np.int_) #collected counts of word to topic assignments
    topic_count_avg = np.zeros(totalK)

    # Sample instantiated zs.  Make sure there's some active topics.
    #zcurr[:K] = np.random.rand(K) < self.pred_z
    zcurr[:K] = np.random.rand(K) < pred_z[:K]

    # Make sure at least two topics active
    if np.sum(zcurr) < 2:
        zcurr[0] = 1
        zcurr[1] = 1
    #zcurr[1] = 1
    #else:
    #    zcurr[:K] = 1
    #    zcurr[np.random.choice(K,size=2,replace=False)] = 1


    
    # Randomly initialize word assignments
    cdef np.ndarray[np.int_t, ndim=1] whichon
    cdef np.ndarray[np.int_t, ndim=1] bcurr
    whichon = np.where(zcurr)[0]
    bcurr = np.random.choice(whichon, num_words)
    
    cdef np.ndarray[np.double_t, ndim=2] word_counts
    cdef np.ndarray[np.double_t, ndim=1] word_counts_marg
    word_counts = np.zeros((totalK, V))
    word_counts_marg = np.zeros(totalK)

    if K > 0:
        word_counts[:K,:] = self.nu

    word_counts[K:totalK, :] = alpha_topic
    word_counts_marg = np.sum(word_counts, axis=1)

    cdef np.ndarray[np.double_t, ndim=1] topic_counts
    topic_counts = np.ones(totalK) * shape 
    
    cdef int topic
    cdef int word
    for i in xrange(num_words):
        topic = bcurr[i]
        word = doc[i]
        word_counts[topic, word] += 1
        topic_counts[topic] += 1
    
    # Meat of sampler
    #cdef np.ndarray[np.int_t, ndim=1] active_z
    #cdef np.ndarray[np.int_t, ndim=1] samplez
    cdef np.ndarray[np.double_t, ndim=1] pword
    cdef np.ndarray[np.double_t, ndim=1] ptopic
    cdef np.ndarray[np.double_t, ndim=1] lik_prior
    cdef np.ndarray[np.double_t, ndim=1] cond_topic
    #cdef np.ndarray[np.double_t, ndim=1] vcurr
    cdef int nactive_z, zidx, psum
    cdef int new_topic, topic_ind
    cdef double pri_sum, p1, p0, p

    pword = np.empty(totalK)
    lik_prior = np.empty(totalK)
    cond_topic = np.empty(totalK)
    ptopic = np.empty(totalK)

    for i in xrange(Ngibbs):
        #print(zcurr)
        #sample topic assignments for each word
        #active_z = np.where(zcurr)[0]
        #nactive_z = active_z.shape[0]

        for j in xrange(num_words):
            topic = bcurr[j]
            word = doc[j]
    
            #forget current assignment
            word_counts[topic,word] -= 1
            word_counts_marg[topic] -= 1
            topic_counts[topic] -= 1 
    
            #compute probabilities 
            for l in xrange(totalK):
                if zcurr[l] > 0:
                    ptopic[l] = topic_counts[l]
                else:
                    ptopic[l] = 0.
            #ptopic = topic_counts[active_z]/np.sum(topic_counts[active_z])

            for l in xrange(totalK):
                if zcurr[l] > 0:
                    pword[l] = word_counts[l,word] / <double>(word_counts_marg[l])
                else:
                    pword[l] = 0.

            #compute using bayes rule
            #pword*ptopic
            #cond_topic = lik_prior/np.sum(lik_prior)
            # Devectorized probably faster b/c avoid going through python
            pri_sum = 0.
            for l in xrange(totalK):
                lik_prior[l] = pword[l] * ptopic[l]
                pri_sum += lik_prior[l]
            #normalize
            for l in xrange(totalK):
                cond_topic[l] = lik_prior[l] / <double>pri_sum

            #sample topic
            #new_topic = np.random.multinomial(1, cond_topic)
            #new_topic = np.where(new_topic)[0][0]
            #new_topic = np.random.choice(totalK, p=cond_topic)
            new_topic = rand_discrete(totalK, <double*>cond_topic.data)
            bcurr[j] = new_topic
    
            #update counts//
            topic_counts[new_topic] += 1
            word_counts[new_topic, word] += 1
            word_counts_marg[new_topic] += 1
    
            #add counts to list
            if i > burnin:
                S[new_topic,word] += 1
                topic_count_avg[new_topic] += 1
    
        #sample vs
        vcurr = np.random.gamma(topic_counts, 1./(1. + 1./scale))
    
        #sample these zs from their conditional distribution
        for j in xrange(totalK):
            if topic_counts[j] == shape:
                p1 = exp(-vcurr[j])*pred_z[j]
                #p1 = pow(rate / (rate+1.), shape)*pred_z[j]
                p0 = 1. - pred_z[j]
                p = p1/(p1 + p0)
                zcurr[j] = np.random.rand() < p
                #zcurr[j] = (rand() / <double>RAND_MAX) < p

        if i > burnin:
            for l in xrange(totalK):
                ztotal[l] += zcurr[l]
    
    #number of samples
    den = <double>(Ngibbs - burnin)

    return ztotal/den, S/den, topic_count_avg/den
