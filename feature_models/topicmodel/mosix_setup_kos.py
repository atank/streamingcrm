from __future__ import division

import os
import numpy as np

from itertools import product

from topicstream import StreamingFocusedTopic
from util import gen_seeds

# Parameters

Kthresh = 300

ntests = 5
test_size = 500  # Number of documents, will convert to fraction
test_interval = 200  # How often to evaluate test criteria

# Starting seed to make test splits.  Each restart uses a differerent seed
# based on this one (which results in a different permuation of the data).
#seed = 8675309 
#test_seeds = seed + np.arange(ntests, dtype=np.int_)
test_seeds = gen_seeds(ntests)

# Full grid
alpha_topic_grid = [.1, .5]
Ngibbs_grid = [100, 500]
burnin_grid = [ng // 2 for ng in Ngibbs_grid]
prune_thresh_grid = [0.001, 0.01, 0.05]
epsilon_topic_grid = [.25, .5, .95]
N_lik_samp_grid = [50]
gibbs_add_topics_grid = [2, 4]
pred_new_grid = [0.05, 0.1, 0.25]
shape_grid = [1., 5.]
scale_grid = [1., 5.]

# Small grid for debugging
#alpha_topic_grid = [.1]
#Ngibbs_grid = [100]
#burnin_grid = [ng // 2 for ng in Ngibbs_grid]
#prune_thresh_grid = [0.01]
#epsilon_topic_grid = [.25]
#N_lik_samp_grid = [50]
#gibbs_add_topics_grid = [2]
#pred_new_grid = [0.05]
#shape_grid = [2.]
#scale_grid = [2.]

# Fixed parameters
alpha_IBP = 1.
prune_on = True
pred_interval = 200


def make_job_file(jfile, vfile, dfile, resdir, logdir):
    """ Write the actual job file.

        Just uses global variables for the possible parameter values so that
        they can be changed easily.
    """

    # Expand paths, check that they exist, and make them if not
    jfile = os.path.expanduser(os.path.expandvars(jfile))
    vfile = os.path.expanduser(os.path.expandvars(vfile))
    dfile = os.path.expanduser(os.path.expandvars(dfile))
    resdir = os.path.expanduser(os.path.expandvars(resdir))
    logdir = os.path.expanduser(os.path.expandvars(logdir))

    if not (os.path.exists(vfile) and os.path.isfile(vfile)):
        raise RuntimeError("Could not find vocab file: %s" % vfile)

    if not (os.path.exists(dfile) and os.path.isfile(dfile)):
        raise RuntimeError("Could not find doc-word counts file: %s" % dfile)

    if os.path.exists(resdir):
        if not os.path.isdir(resdir):
            raise RuntimeError("Results dir is not a directory: %d" % resdir)
    else:
        try:
            os.makedirs(resdir)
        except IOError:
            raise RuntimeError("Could not create directory: %s" % resdir)

    if os.path.exists(logdir):
        if not os.path.isdir(logdir):
            raise RuntimeError("Logging dir is not a directory: %d" % logdir)
    else:
        try:
            os.makedirs(logdir)
        except IOError:
            raise RuntimeError("Could not create directory: %s" % logdir)

    base_cmd = "python run_ftm.py %s %s %s " % (vfile, dfile, resdir)

    # Create jobs for each parameter combination and write to job file
    with open(jfile, 'w') as f:

        par_prod = product(alpha_topic_grid, Ngibbs_grid, 
                           prune_thresh_grid,
                           epsilon_topic_grid, N_lik_samp_grid,
                           gibbs_add_topics_grid, pred_new_grid,
                           shape_grid,
                           scale_grid,
                           test_seeds)

        for par in par_prod:
            
            alpha_topic, Ngibbs, prune_thresh, epsilon_topic, \
            N_lik_samp, gibbs_add_topics, pred_new, shape, scale, seed_t = par

            burnin = Ngibbs // 2

            # Update name with params we care about
            eid = hash(par)
            ename = "ftm_kos_%d" % (eid,)
            ename = ename.replace(".", "_")

            # Update with params we care about
            cmd = base_cmd + \
                  "%f %f %d " % (alpha_topic, alpha_IBP, Ngibbs)
            cmd += "%d %f %f " % (gibbs_add_topics, pred_new, prune_thresh)
            cmd += "%d %d %d " % (burnin, prune_on, N_lik_samp)
            cmd += "%d %d %d " % (pred_interval, test_size, test_interval)
            cmd += "%f %f %f %d " % (epsilon_topic, shape, scale, seed_t)
            cmd += "%d " % Kthresh
            cmd += "%s " % ename

            # Redirect stdout and stderr to logfile so that print logs.
            logfile = os.path.join(logdir, "%s.log" % ename)
            cmd += ">%s 2>&1" % logfile 

            f.write(cmd + "\n")
            
    os.chmod(jfile, 0755)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Set up files for streaming FTM.")
    parser.add_argument('jfile', type=str,
                        help="path to write jobfile to")
    parser.add_argument('vfile', type=str,
                        help="path to vocab file")
    parser.add_argument('dfile', type=str,
                        help="path to binary pickle file storing sparse matrix of counts")
    parser.add_argument('resdir', type=str, help="path to results directory")
    parser.add_argument('logdir', type=str, help="path to write logs to")

    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)

    make_job_file(**args)
