from __future__ import division

import os

import numpy as np
import pandas as pd
import cPickle as pkl

from scipy.sparse import csr_matrix
from sklearn.cross_validation import ShuffleSplit

from util import gen_seeds


ntest = 5
test_seeds = gen_seeds(ntest)


def make_test_splits(outdir, X, test_size):
    """ Split corpus into training and testing documents.
        
        outdir : path to dir to store results
        X : scipy.sparse array in csc format with docs in columns
        test_size : number of documents in test set
    """

    # Transpose b/c the code expects docs in rows
    X = csr_matrix(X.T)

    D, V = X.shape
    test_frac = test_size / D
    for i in xrange(ntest):
        ss = ShuffleSplit(D, n_iter=1, test_size=test_frac,
                          random_state=test_seeds[i])
        # Extract training and test set (there's only one here)
        for tr_inds, te_inds in ss:
            pass
        
        Xtrain = X[tr_inds,:]
        Xtest = X[te_inds,:]

        # Write training and testing sets to files
        Xtr_idx = Xtrain.nonzero()
        Xtr_dat = np.squeeze(np.asarray(Xtrain[Xtr_idx]))
        Xte_idx = Xtest.nonzero()
        Xte_dat = np.squeeze(np.asarray(Xtest[Xte_idx]))
        
        df_tr = pd.DataFrame({'did': Xtr_idx[0], 'wid': Xtr_idx[1],
                              'count': Xtr_dat})
        of = os.path.join(outdir, "kos_train_%d.csv" % i)
        df_tr.to_csv(of, index=False, columns=['did', 'wid', 'count'])
        del df_tr

        df_te = pd.DataFrame({'did': Xte_idx[0], 'wid': Xte_idx[1],
                              'count': Xte_dat})
        of = os.path.join(outdir, "kos_test_%d.csv" % i)
        df_te.to_csv(of, index=False, cols=['did', 'wid', 'count'])


if __name__ == "__main__":

    import sys

    argv = sys.argv[1:]
    
    outdir = argv[0]
    docfile = argv[1]

    with open(docfile, 'rb') as f:
        X = pkl.load(f)

    print "Make sure test_size matches that in run_ftm."
    test_size = 500
    make_test_splits(outdir, X, test_size)
