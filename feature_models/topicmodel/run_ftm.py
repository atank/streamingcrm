from __future__ import division

import os
import sys
import cPickle as pkl

import numpy as np

from scipy.sparse import csc_matrix, csr_matrix
from scipy.io import mmread

from sklearn.cross_validation import ShuffleSplit

from topicstream import StreamingFocusedTopic


def run_exper(vfile=None, dfile=None, respath=None,
              alpha_topic=None,
              alpha_IBP=None,
              Ngibbs=100,
              gibbs_add_topics=5,
              pred_new=None,
              prune_thresh=None,
              burnin=50,
              prune_on=True,
              N_lik_samp=50,
              pred_interval=1000,
              test_size=None, test_interval=None,
              epsilon_topic=None,
              shape=None,
              scale=None,
              seed=None, Kthresh=-1, name=None,
              visualize_trial=False,
              word_test_frac=0.2):
    """ Cluster documents and save resulting object.
        
        vfile : path to vocab file
        dfile : path to pkl file containing doc word counts (pkl file
                     should be binary)
        respath : path to directory to store results

        Model parameters, see topicsteram.py for documentation
        alpha_topic
        alpha_IBP
        Ngibbs
        gibbs_add_topics
        pred_new
        prune_thresh
        burnin
        prune_on
        N_lik_samp
        epsilon_topic
        shape
        scale

        test_size : fraction of documents in test set
        test_interval : how often to evaluate test criteria
        seed : random seed to create test split and permutation of documents
        Kthresh : maximum number of clusters that can be used, by default it is
                  -1 which will cause no threshold to be used.
        name : experiment name (to make file names)
        visualize_trial : don't use use
        word_test_frac : fraction of words in test docs to hold out
    """
    
    if Kthresh < 0:
        Kthresh = np.inf

    # Load data
    print "Loading data..."
    cpath = os.path.expanduser(os.path.expandvars(dfile))
    with open(cpath, 'rb') as f:
        X = pkl.load(f)

    # Transpose b/c the code expects docs in rows
    X = csr_matrix(X.T)

    D, V = X.shape

    # Train/test split, returned indices are randomly permuted
    print "Creating training/test sets..."
    test_frac = test_size / D
    ss = ShuffleSplit(D, n_iter=1, test_size=test_frac, random_state=seed)
    for tr_inds, te_inds in ss:
        # Extract training and test set (there's only one here)
        pass

    Xtest = X[te_inds,:]

    # Convert to dense array b/c splitting code only words for that
    try:
        Xtest = Xtest.toarray()
    except MemoryError:
        raise RuntimeError("Could not create dense array for Xtest")

    Xtest = Xtest.astype('int')

    test_llik = list()
    num_topics = list()

    # Run streaming algorithm
    ftm_stream = StreamingFocusedTopic(alpha_topic=alpha_topic,
                                       epsilon_topic=epsilon_topic,
                                       shape=shape, scale=scale,
                                       alpha_IBP=alpha_IBP,
                                       V=V, Ngibbs=Ngibbs,
                                       gibbs_add_topics=gibbs_add_topics,
                                       pred_new=pred_new,
                                       prune_thresh=prune_thresh,
                                       burnin=burnin, prune_on=prune_on,
                                       visualize_trial=visualize_trial,
                                       test_frac=test_size,
                                       N_lik_samp=N_lik_samp,
                                       word_test_frac=word_test_frac)

    print "Running..."

    #print "SMALL DATA SETS FOR DEBUGGING!  REMOVE BEFORE RUNNING"
    #tr_inds = tr_inds[:10]

    failed = False
    for i, n in enumerate(tr_inds):

        if i % 10 == 0:
            print "doc: %d, K: %d" % (i, ftm_stream.K)
        
        x = np.squeeze(X[n,:].toarray()).astype(np.int_)
        ftm_stream.update(x)

        # Kill experiment if used too many clusters b/c it probably won't
        # finish.
        K = ftm_stream.K 
        if K > Kthresh:
            print "Error: # topics (%d) exceeds Kthresh (%d)" % (K, Kthresh),
            failed = True
            break

        # Evaluate
        if i >= test_interval and i % test_interval == 0:
            print "  evaluating test ll ", 
            tmp = ftm_stream.test_likelihood(Xtest)
            print "%.2f" % tmp
            test_llik.append(tmp)
            num_topics.append(ftm_stream.K)

        # So we can see progress in the logs
        sys.stdout.flush()
        sys.stderr.flush()

    ftm_stream.s_expectations_avg /= len(tr_inds)

    # Write results
    respath = os.path.expanduser(os.path.expandvars(respath))

    if not os.path.exists(respath):
        os.makedirs(respath)

    if not failed:
        res_dict = {'model': ftm_stream, 'test_ll': test_llik,
                    'num_topics': num_topics}
    else:
        res_dict = {'model': None, 'test_ll': None,
                    'num_topics': None}

    res_name = "res-%s" % (name,)

    outpath = os.path.join(respath, res_name + ".pkl")
    with open(outpath, 'w') as f:
        pkl.dump(res_dict, f)

    return


if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description="Run FTM topic model.")

    parser.add_argument('vfile', type=str,
                        help="path to vocab file")
    parser.add_argument('dfile', type=str,
                        help="path to binary pickle file storing sparse matrix of counts")
    parser.add_argument('respath', type=str, help="path to results directory")

    parser.add_argument('alpha_topic', type=float, help="")
    parser.add_argument('alpha_IBP', type=float, help="")
    parser.add_argument('Ngibbs', type=int, help="")
    parser.add_argument('gibbs_add_topics', type=int, help="")
    parser.add_argument('pred_new', type=float, help="")
    parser.add_argument('prune_thresh', type=float, help="")
    parser.add_argument('burnin', type=int, help="")
    parser.add_argument('prune_on', type=int, help="")
    parser.add_argument('N_lik_samp', type=int, help="")
    parser.add_argument('pred_interval', type=int, help="")
    parser.add_argument('test_size', type=float, help="")
    parser.add_argument('test_interval', type=int, help="")
    parser.add_argument('epsilon_topic', type=float, help="")
    parser.add_argument('shape', type=float)
    parser.add_argument('scale', type=float)
    parser.add_argument('seed', type=int, help="")
    parser.add_argument('Kthresh', type=float, default=np.inf,
                        help="Maximum number of clusters allowd")
    parser.add_argument('name', type=str, default=None,
                        help="Name of experiment for saving resultns")

    # Example of optional argument
    #parser.add_argument('--seed', type=int,
    #                    help="Seed for random number generator", default=0)
    
    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)

    np.seterr(invalid='raise')

    # Run code
    run_exper(**args)
