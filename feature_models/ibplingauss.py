from __future__ import division

import numpy as np
import scipy as sp
import scipy.linalg as spl

import copy as copy

from scipy.stats import poisson
from numpy import linalg as LA
from util import mvn_logpdf, make_Ztilde, make_8x8_bars, trans
import matplotlib.pyplot as plt

eps = np.finfo('float64').eps


class StreamIBPLinGaussian(object):

    def __init__(self, K=None, rho=None, nu=None, Nsamp_Gibbs=None,
                 epsilon_feat=None, sigma_feat=None, predictive=None, unnorm_predictive=None
                 , mu_feature=None, sigma_y=None, nu_mu=None, nu_sigma=None, D=None, 
                 add_zeros=None, n=None, gamma=None, alpha=None, thresh_diff=None,
                 merge_interval=None, merge_f=None, pred_samples=None, bd_move=None):
        """
            K : Number of features to start with.
            rho : Initial variational approximation of soft-assignments of
                  features.
            nu : Initial variational approximation of features
    
            nu_mu : D x K ndarray with feature means in columns
        """
        
        # Just set for now
        self.pred_samples = pred_samples
        self.merge_interval = merge_interval
        self.merge_f = merge_f
        self.gamma = gamma
        self.K = K
        self.rho = rho
        #self.nu = nu
        self.nu_mu = nu_mu
        self.nu_sigma = nu_sigma
        self.Nsamp_Gibbs = Nsamp_Gibbs
        self.epsilon_feat = epsilon_feat
        self.sigma_feature = sigma_feat  # feature_sigma
        self.sigma_y = sigma_y
        self.mu_feature = mu_feature
        self.D = D
        self.predictive = predictive
        self.unnorm_predictive = unnorm_predictive
        self.n = n
        self.add_zeros = add_zeros
        self.alpha = alpha
        self.thresh_diff = thresh_diff
        self.bd_move = bd_move



    def update(self, x):
        """ Update variational approximations for new observation x.
            
            This function will probably require some thresholds as well.

            x : D dimensional observation.
        """
        self.n += 1
        #compute soft assignments for this data point using past variational distirbutions
        self._update_rho(x)

        #given these soft assignments, update the variational parameters
        if (self.K != 0):
            self._update_nu(x)

        if (self.merge_f == True):
            if (self.n % self.merge_interval == 1):
                self.merge()

        #print(self.nu_mu)
        print(self.rho)
        print(self.unnorm_predictive)


    def _update_rho(self, x):
        """ Helper function to perform variational update for rho.
        """

        # Compute marginals
        self.rho = self.gibbs_update(x)
        
        # Remove features with small probabilities
        #print(self.rho)


        Kold = self.K
        tmp = self.rho > self.epsilon_feat
        #print(tmp)
        tmp.astype(int)
        tmp[:self.K] = 1
        self.rho = self.rho[tmp]
        self.K = self.rho.shape[0]
        self.num_new_feat = self.K - Kold
        
        nnew = self.num_new_feat
        mu_new = np.tile(self.mu_feature[:,np.newaxis], (1, nnew))
        sig_new = self.sigma_feature * np.ones(nnew)

        # Create new parameters for introduced features
        if (Kold == 0):
            self.nu_mu = mu_new
            self.nu_sigma = sig_new
            self.unnorm_predictive = np.zeros(nnew)
        else:
            self.nu_mu = np.hstack((self.nu_mu, mu_new))
            self.nu_sigma = np.hstack((self.nu_sigma, sig_new))
            self.unnorm_predictive = np.hstack((self.unnorm_predictive, np.zeros(nnew)))

        self.unnorm_predictive += self.rho
        self.predictive = self.unnorm_predictive/self.n


    def merge(self):
        normed_mu = self.nu_mu/LA.norm(self.nu_mu,axis=0)
        sim = np.dot(normed_mu.T,normed_mu)
        for i in xrange(0,self.K):
            for j in xrange(i + 1, self.K):
                if (sim[i,j] > thresh_merge):
                    merge_features(i,j)

    def merge_features(self,i,j):
        weighti = self.preditive[i]
        weightj = self.predictive[j]
        self.unnorm_predictive[i] += self.unnorm_predictive[j]

        self.nu_sigma[i] = 1/(weighti/self.nu_sigma[i] + weightj/self.nu_sigma[j])

        if self.weighted_comb == True:
            self.nu_mu[i,:] = weighti*self.nu_mu[i,:] + weightj*self.nu_mu[j,:]
        else:
            if self.add_combine == True:
                self.nu_mu[i,:] = self.nu_mu[i,:] + self.nu_mu[j,:]

        self.K = self.K - 1
        self.nu_mu = np.delete(self.nu_mu,j,1)
        self.nu_sigma = np.delete(self.nu_sigma,j)




    # def importance_update(self,x):
    #     z = np.random.rand(self.samp_import,self.K) < self.predictive
    #     znew =  


    def gibbs_update(self, x):
        #randomly initialize old feature assignments
        print('start gibbs')
        K = self.K
        sigma_y = self.sigma_y
        D = x.shape[0]
        predictive = self.predictive

        #initialize tildemu and tildesigma matrices
        h = np.tile(self.mu_feature[:,np.newaxis], (1, self.add_zeros))
        if (K == 0):
            tilde_mu = h
            zcurr = np.zeros(self.add_zeros)
            sig_feat = self.sigma_feature*np.ones(self.add_zeros)
        else:
            zcurr = np.random.rand(self.K) < self.predictive
            #zcurr = np.zeros(K)
            zcurr = np.hstack((zcurr, np.zeros(self.add_zeros)))
            tilde_mu = np.hstack((self.nu_mu, h))
            sig_feat = np.hstack((self.nu_sigma,
                             self.sigma_feature*np.ones(self.add_zeros)))

        Kmax = zcurr.shape[0]
        Kadd = self.add_zeros 
        zcounts = np.zeros(zcurr.shape)
        Kold = 0
        #print(Kmax)


        #print(tilde_sig)
        #print(tilde_sig.shape)

        # Componentwise Gibbs updates
        for j in xrange(self.Nsamp_Gibbs):
            #print(Kold)

            # Update each component
            for i in xrange(self.K):
                #print(i)

                ## Update Ztilde to set this entry to one

                z = zcurr.copy()
                z[i] = 1

                tmp = mvn_logpdf(x, np.dot(tilde_mu, z), 
                                 (sigma_y + np.dot(z,sig_feat))*np.eye(D))

                lpr = np.log(predictive[i] + eps)
                lpz1 = tmp + lpr

                #print(lpz1)

                ## Update Ztilde to set this entry to zero
                #for d in xrange(D):
                #    Ztilde[d, i*D+d] = 0
                z[i] = 0
                Ztilde = make_Ztilde(z, D)

                #print(np.dot(Ztilde, tilde_mu))


                tmp = mvn_logpdf(x, np.dot(tilde_mu, z), 
                                 (sigma_y + np.dot(z,sig_feat))*np.eye(D))

                lpr = np.log(1-predictive[i] + eps)
                lpz0 = tmp + lpr
                #print(lpz0)

                # Fix this to be stable
                pi0 = np.exp(lpz0) / (np.exp(lpz0) + np.exp(lpz1))
                #print(i)
                #print(pi0)

                zcurr[i] = np.random.rand() > pi0
                #print(zcurr[i])

            # Sample new components via MH


            
            #print(x)
            #print(np.dot(Ztilde, tilde_mu))


            ll0 = mvn_logpdf(x, np.dot(tilde_mu, zcurr),(sigma_y + np.dot(zcurr,sig_feat))*np.eye(D))

            if self.bd_move == False:
                Knew = np.minimum(np.random.poisson(self.gamma), Kadd)
                #print(Knew)
                lgKnew = np.log(poisson.pmf(Knew, self.gamma))
                lgK0 = np.log(poisson.pmf(Kold, self.gamma))
            else:
                if Kold == 0:
                    Knew = 1
                    lgKnew = np.log(1)
                    lgK0 = np.log(.5)
                else:
                    Knew = np.random.sample([Kold - 1, Kold + 1])
                    lgKnew = np.log(.5)
                    lgK0 = np.log(.5)
            #print('h')
            #print(Knew)
            #print(Kold)
            #Knew = 1
            #print(Knew)
            zcurr[K:K+Knew] = 1
            zcurr[K+Knew:] = 0

            ll1 = mvn_logpdf(x, np.dot(tilde_mu, zcurr),(sigma_y + np.dot(zcurr,sig_feat))*np.eye(D))
            
            lpKnew = np.log(poisson.pmf(Knew, self.alpha/self.n))
            lpK0 = np.log(poisson.pmf(Kold, self.alpha/self.n))
            lgKnew = np.log(poisson.pmf(Knew, self.gamma))
            lgK0 = np.log(poisson.pmf(Kold, self.gamma))

            lr = ll1 + lpKnew + lgK0 - ll0 - lpK0 - lgKnew
            #print(lr)

            # Check this
            if np.max((0, lr)) > np.random.exponential(1):
                # Leave zcurr alone because we already changed it
                Kold = Knew    
            else:
                zcurr[K:K+Kold] = 1
                zcurr[K+Kold:] = 0
                Kold = Kold

            # Incorporate into zcounts
            zcounts += zcurr

        # Update self.rho with zcounts / nsamp
        self.rho = zcounts / self.Nsamp_Gibbs
        return(self.rho)

    def sample_z_old(self, x):
        pass

    def _update_nu(self, x):
            

        nu_sigma = copy.deepcopy(self.nu_sigma)
        nu_mu = copy.deepcopy(self.nu_mu)
        #nu_mu = np.random.randn(self.D, self.K)


        # Update nu_sigma here
        self.nu_sigma = 1./(1./nu_sigma + self.rho/self.sigma_y + eps)

        diff = np.inf
        while diff > self.thresh_diff:
            sigma_old = copy.deepcopy(self.nu_sigma)
            mu_old = copy.deepcopy(self.nu_mu)

            for k in xrange(0, self.K):
                xbar = (x - np.dot(self.nu_mu, self.rho)
                        + self.nu_mu[:,k]*self.rho[k])
                dummy = nu_mu[:,k]/nu_sigma[k] + xbar*self.rho[k]/self.sigma_y
                self.nu_mu[:,k] = dummy*self.nu_sigma[k]

            diff = np.max(np.abs(mu_old - self.nu_mu))

    def pred_ll(self, Xnew):
        """ Predictive log-likelihood for new observation(s) Xnew.
            
            Xnew : ndarray of new observation(s).
        """
        exact = True
        llik = 0
        if exact == True:
            llik = 0
            totcomb = 2**self.K
            for i in xrange(0,Xnew.shape[0]):
                llik_this = np.zeros(totcomb)
                for j in xrange(0,totcomb):
                    z = trans(j,self.K)
                    on = np.where(z == 1)
                    off = np.where(z == 0)
                    lpon = np.sum(np.log(self.predictive[on]))
                    lpoff = np.sum(np.log(1 - self.predictive[off]))

                    llik_this[j] = mvn_logpdf(Xnew[i,:].T,np.dot(self.nu_mu,z), (sigma_y + np.dot(z,self.nu_sigma))*np.eye(D))
                    llik_this[j] += lpon + lpoff
                llik += sp.misc.logsumexp(llik_this)
        else:
            ndat = Xnew.shape[0]
            for i in xrange(0,ndat):
                llik_this = 0
                for j in xrange(0,self.pred_samples):
                    z = np.random.binomial(1,self.predictive) 
                    #print(Xnew[i,:])
                    q = np.exp(mvn_logpdf(Xnew[i,:].T, np.dot(self.nu_mu, z),(sigma_y + np.dot(z,self.nu_sigma))*np.eye(D)))
                    llik_this = llik_this + q
                llik_this = llik_this/self.pred_samples
                llik += np.log(llik_this)
        print(llik)
        return(llik)

    def visualize_features(self):
        fig, ax = plt.subplots(1,self.K)
        for k in xrange(self.K):
            #print('dff')
            f = self.nu_mu[:,k].reshape((8,8))
            ax[k].imshow(f, cmap='Greys')
            ax[k].set_title(self.unnorm_predictive[k])
        #plt.show()


#joint tester code
if __name__ == "__main__":
    sigma_y = .25*.25
    size = 8
    nbars = 8
    sigma_feat = .3
    Nsamp_Gibbs = 10
    K = 0
    add_zeros = 5
    gamma = 1
    alpha = 1
    thresh_diff = .1
    n = 0
    epsilon_feat = .3
    Ntrain = 100
    Ntest = 100
    prob_feat = .2
    D = size*size
    mu_feature = np.zeros(D)
    merge_interval = 50
    merge_f = False
    pred_samples = 30
    bd_move = False

    x = make_8x8_bars(Ntrain+Ntest, sigma_y, size, prob_feat, nbars)
    xtrain = x[:Ntrain]
    xtest = x[Ntrain:]

    stream = StreamIBPLinGaussian(K=K, add_zeros=add_zeros, Nsamp_Gibbs=Nsamp_Gibbs, sigma_y=sigma_y, 
     D=D, sigma_feat = sigma_feat, mu_feature=mu_feature, gamma=gamma,
     alpha=alpha, thresh_diff=thresh_diff, n=0, epsilon_feat=epsilon_feat,
      merge_interval=merge_interval, merge_f=merge_f, pred_samples=pred_samples,
      bd_move=bd_move)

    lik_int = 10

    lik = np.zeros(Ntrain/lik_int + 1)
    count = 1
    for i in xrange(0,Ntrain):
        stream.update(xtrain[i,:])
        if i % lik_int == 6:
            stream.visualize_features()
            lik[count] = stream.pred_ll(xtest)
            count += 1

    stream.visualize_features()
    plt.show()

    plt.plot(lik[1:])
    plt.show()
    
    print(lik)




# ##tester code for variational inference####
# if __name__ == "__main__":
#     sigma_y = .01
#     size = 8
#     #Create bars
#     bars = np.zeros([size, size, size])
#     bars[0,0,] = 1
#     bars[1,2,] = 1  
#     bars[2,4,] = 1
#     bars[3,6,] = 1
#     bars[4,:,0] = 1
#     bars[5,:,2] = 1
#     bars[6,:,4] = 1
#     bars[7,:,6] = 1
#     x1t = np.zeros([size,size])
#     #xt += bars[1,:,:]
#     x1t += bars[2,:,:]
#     #xt += bars[3,:,:]
#     x1 = x1t.flatten()
#     x1 += np.sqrt(sigma_y)*np.random.randn(size*size)
#     feat1 = bars[1,:,:].flatten()
#     feat7 = bars[2,:,:].flatten()
#     feat2 = bars[3,:,:].flatten()

#     x2t = np.zeros([size,size])
#     x2t += bars[1,:,:]
#     x2 = x2t.flatten()
#     x2 += np.sqrt(sigma_y)*np.random.randn(size*size)
    
#     x3t = np.zeros([size,size])
#     x3t += bars[1,:,:]
#     x3t += bars[2,:,:]
#     x3 = x3t.flatten()
#     x3 += np.sqrt(sigma_y)*np.random.randn(size*size)

#     D = size*size

#     mu_feature = np.zeros(D)
#     sigma_feat = .2
    
#     Nsamp_Gibbs = 10
#     K = 0
#     #nu_mu = np.concatenate((feat1[:,np.newaxis], feat7[:,np.newaxis]), axis=1)
#     #nu_mu = np.concatenate((feat1[:,np.newaxis], mu_feature[:,np.newaxis]), axis=1)

#     #nu_sigma = sigma_feat*np.ones(K)
#     #nu_sigma = np.array([.005, .3])
#     #nu_sigma = .0096*np.ones(K)
#     #predictive = .5*np.ones(K)
#     #n = 2
#     add_zeros = 5
#     gamma = 1
#     alpha = 1
#     thresh_diff = .1
#     n = 0
#     epsilon_feat = .1

#     stream = StreamIBPLinGaussian(K=K, add_zeros=add_zeros, Nsamp_Gibbs=Nsamp_Gibbs, sigma_y=sigma_y, 
#      D=D, sigma_feat = sigma_feat, mu_feature=mu_feature, gamma=gamma,
#      alpha=alpha, thresh_diff=thresh_diff, n=0, epsilon_feat=epsilon_feat)

#     stream.update(x3)
#     stream.update(x2)
#     stream.update(x1)
#     stream.update(x3)
#     stream.update(x2)
#     stream.update(x1)
#     stream.update(x3)
#     stream.update(x2)
#     stream.update(x1)
#     stream.update(x3)
#     stream.update(x2)
#     stream.update(x1)
#     stream.visualize_features()

#     #print(stream.nu_mu)

#     # stream.update(x1)
#     # print(stream.unnorm_predictive)
#     # print(stream.nu_mu)
#     # print(stream.nu_sigma)
#     # print(stream.K)
#     # stream.update(x1)
#     # stream.update(x1)
#     # stream.update(x1)
#     # stream.update(x1)
#     # stream.update(x1)
#     # stream.update(x1)

#     # print(stream.nu_mu)







#gibbs sampler tester code####
# if __name__ == "__main__":
#     sigma_y = .01
#     size = 8
#     # Create bars
#     bars = np.zeros([size, size, size])
#     bars[0,0,] = 1
#     bars[1,2,] = 1  
#     bars[2,4,] = 1
#     bars[3,6,] = 1
#     bars[4,:,0] = 1
#     bars[5,:,2] = 1
#     bars[6,:,4] = 1
#     bars[7,:,6] = 1
#     xt = np.zeros([size,size])
#     xt += bars[1,:,:]
#     #xt += bars[7,:,:]
#     #xt += bars[2,:,:]
#     x = xt.flatten()
#     x += np.sqrt(sigma_y)*np.random.randn(size*size)
#     feat1 = bars[1,:,:].flatten()
#     feat7 = bars[7,:,:].flatten()
#     feat2 = bars[2,:,:].flatten()
#     D = size*size

#     mu_feature = np.zeros(D)
#     sigma_feat = .3
    
#     Nsamp_Gibbs = 100
#     K = 2
#     nu_mu = np.concatenate((feat1[:,np.newaxis], feat7[:,np.newaxis]), axis=1)
#     #nu_mu = np.concatenate((feat1[:,np.newaxis], feat7[:,np.newaxis], feat2[:,np.newaxis]), axis=1)
#     nu_sigma = .0096*np.ones(K)
#     predictive = .5*np.ones(K)
#     n = 2
#     add_zeros = 5
#     gamma = 1
#     alpha = 1

#     stream = StreamIBPLinGaussian(K=K, nu_mu=nu_mu, nu_sigma=nu_sigma, predictive=predictive,
#      n=n, add_zeros=add_zeros, Nsamp_Gibbs=Nsamp_Gibbs, sigma_y=sigma_y, 
#      D=D, sigma_feat = sigma_feat, mu_feature=mu_feature, gamma=gamma,
#      alpha=alpha)
#     stream.gibbs_update(x) 
#     print(stream.rho[0:10])



    # Test members