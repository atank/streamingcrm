from __future__ import division
from scipy.io import mminfo,mmread

import numpy as np
import scipy.optimize as spopt

from scipy.sparse import csc_matrix

from util import logmpoyla, make_discrete_bars
import matplotlib.pyplot as plt
from nrm_mixture_lag import GGPMixtureMultinomialStream_lag

filename = 'docword_kos2.mtx'
X = csc_matrix(mmread(filename).todense().T)
N = X.shape[1]
V = X.shape[0]

Ntest = 400
Xtrain = X[:,Ntest:]
Ntrain = Xtrain.shape[1]
Xtest = X[:,:Ntest]

g = np.squeeze(X[:,0].toarray())
print g.shape
stream_nlag = GGPMixtureMultinomialStream_lag(x=g, a=1, tau=1, sig=0, alpha=0.2,
                          newclust_eps=0.95 ,topic_eps=.1,fixed_lag=0,n_smooth_it=1,m_lag=499)
stream_lag = GGPMixtureMultinomialStream_lag(x=g, a=1, tau=1, sig=0, alpha=0.2,
                          newclust_eps=0.95 ,topic_eps=.1,fixed_lag=1,n_smooth_it=1,m_lag=Ntrain-1,
                          lag_prune=.1)

test_interval = 1000
lik_test_lag = []
lik_test_nlag = []
for i in xrange(1,Ntrain):
    print i
    if (i % test_interval == 2):
        lik_test_lag.append(stream_lag.pred_ll(Xtest))
        lik_test_nlag.append(stream_nlag.pred_ll(Xtest))

    g = np.squeeze(Xtrain[:,i].toarray())
    stream_lag.update(g)
    stream_nlag.update(g)

lik_test_lag.append(stream_lag.pred_ll(Xtest))
lik_test_nlag.append(stream_nlag.pred_ll(Xtest))
        
lik_lag = np.array(lik_test_lag)
lik_nlag = np.array(lik_test_nlag)
plt.plot(lik_lag,'r',lik_nlag,'b')
plt.show()