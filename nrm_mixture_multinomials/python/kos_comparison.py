from __future__ import division
from scipy.io import mminfo,mmread

import numpy as np
import scipy.optimize as spopt

from scipy.sparse import csc_matrix

from util import logmpoyla, make_discrete_bars
import matplotlib.pyplot as plt
from nrmmixture import GGPMixtureMultinomialStream


####parameters####
a=1;tau=1;sig=1;alpha=.15;newclust_eps=.5;prune_eps=.001;merge_eps=0;merge=False;merge_interval=300

filename = 'docword_kos2.mtx'
X = csc_matrix(mmread(filename).todense().T)
N = X.shape[1]
V = X.shape[0]


Ntest = 400
Xtrain = X[:,Ntest:]
Ntrain = Xtrain.shape[1]
Xtest = X[:,:Ntest]

g = np.squeeze(X[:,0].toarray())

streaminv = GGPMixtureMultinomialStream(x=g,a=10,tau=.1,sig=.5,alpha=.1,newclust_eps=.75,
        prune_eps=prune_eps,merge_eps=merge_eps,merge=merge,merge_interval=merge_interval)
streamDP = GGPMixtureMultinomialStream(x=g,a=a,tau=1,sig=0,alpha=alpha,newclust_eps=newclust_eps,
        prune_eps=prune_eps,merge_eps=merge_eps,merge=merge,merge_interval=merge_interval)

test_interval = 200
lik_test_inv= []
lik_test_DP = []
nclust_inv = []
nclust_DP = []

for i in xrange(1,Ntrain):
    print i  
    #print streamDP.nclust
    #print streaminv.nclust

    if (i % test_interval == 2):
        lik_test_inv.append(streaminv.pred_ll_fast(Xtest))
        lik_test_DP.append(streamDP.pred_ll_fast(Xtest))
        #lik_test_inv.append(streaminv.pred_ll_nonew(Xtest))
        #lik_test_DP.append(streamDP.pred_ll_nonew(Xtest))
        nclust_inv.append(streaminv.nclust)
        nclust_DP.append(streamDP.nclust)

    g = np.squeeze(Xtrain[:,i].toarray())
    streaminv.update(g)
    streamDP.update(g)
plt.subplot(2,1,1)
plt.plot(lik_test_DP[3:],color='red')
plt.plot(lik_test_inv[3:],color='blue')
plt.subplot(2,1,2)
plt.plot(nclust_DP,color='red')
plt.plot(nclust_inv,color='blue')

plt.show()



