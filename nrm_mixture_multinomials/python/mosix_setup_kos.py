from __future__ import division

import os
import numpy as np

from itertools import product

from nrmmixture import GGPMixtureMultinomialStream

# neweps must be >= sig values
a_l = [ 1.,5., 10.,50.,100.,500., 1000.]
#a_l = [1.]
tau_l = [1.,5.,10.,50.,100.,500.,1000.]
#tau_l = [1.]
sig_l = [0., .25, .5, .75]
neweps_l = [.5, .5, .5, .75]
alpha_l = [0.1]
merge_eps = 0
Kthresh = 3000


ntests = 20
#ntests = 1
test_size = .2
test_interval = 20  # How often to evaluate test criteria
merge_interval = 300000


# Starting seed to make test splits.  Each restart uses a differerent seed
# based on this one (which results in a different permuation of the data).
split_seed = 86975309 
shuffle_seed = 845573

def make_job_file(jfile, vfile, dfile, resdir, logdir):
    """ Write the actual job file.

        Just uses global variables for the possible parameter values so that
        they can be changed easily.
    """

    # Expand paths, check that they exist, and make them if not
    jfile = os.path.expanduser(os.path.expandvars(jfile))
    vfile = os.path.expanduser(os.path.expandvars(vfile))
    dfile = os.path.expanduser(os.path.expandvars(dfile))
    resdir = os.path.expanduser(os.path.expandvars(resdir))
    logdir = os.path.expanduser(os.path.expandvars(logdir))

    if not (os.path.exists(vfile) and os.path.isfile(vfile)):
        raise RuntimeError("Could not find vocab file: %s" % vfile)

    if not (os.path.exists(dfile) and os.path.isfile(dfile)):
        raise runtimeError("Could not find doc-word counts file: %s" % dfile)

    if os.path.exists(resdir):
        if not os.path.isdir(resdir):
            raise RuntimeError("Results dir is not a directory: %d" % resdir)
    else:
        try:
            os.makedirs(resdir)
        except IOError:
            raise RuntimeError("Could not create directory: %s" % resdir)

    if os.path.exists(logdir):
        if not os.path.isdir(logdir):
            raise RuntimeError("Logging dir is not a directory: %d" % logdir)
    else:
        try:
            os.makedirs(logdir)
        except IOError:
            raise RuntimeError("Could not create directory: %s" % logdir)

    base_cmd = "python run_nrmmix.py %s %s %s " % (vfile,
                                                        dfile, resdir)

    # Create jobs for each parameter combination and write to job file
    with open(jfile, 'w') as f:
        par_prod = product(a_l, tau_l, sig_l, alpha_l, range(ntests))
        idx = 0
        for par in par_prod:
            
            a, tau, sig, alpha, testid = par
            # This will have to change if we want to grid over neps
            neps = neweps_l[sig_l.index(sig)]

            ename = "exp-a-%.2f-tau-%.2f-sig-%.2f-alpha-%.2f-neps-%.2f-testid-%d" % (a, tau, sig, alpha, neps, testid)
            ename = ename.replace(".", "_")

            cmd = base_cmd + "%f %f %f %f %f %f %d %f %d %d %d " % (a, tau, sig, alpha,
															neps, merge_eps, merge_interval,
															test_size,test_interval,
															shuffle_seed + testid,split_seed)
            cmd += "%d " % Kthresh

            # Redirect stdout and stderr to logfile so that print logs.
            logfile = os.path.join(logdir, "%s.log" % ename)
            cmd += ">%s 2>&1" % logfile 

            f.write(cmd + "\n")
            
            # Make new seed for next parameter setting
            idx += 1

    os.chmod(jfile, 0755)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Set up files for NRM document clustering.")
    parser.add_argument('jfile', type=str,
                        help="path to write jobfile to")
    parser.add_argument('vfile', type=str,
                        help="path to vocab file")
    parser.add_argument('dfile', type=str,
                        help="path to binary pickle file storing sparse matrix of counts")
    parser.add_argument('resdir', type=str, help="path to results directory")
    parser.add_argument('logdir', type=str, help="path to write logs to")

    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)

    make_job_file(**args)
