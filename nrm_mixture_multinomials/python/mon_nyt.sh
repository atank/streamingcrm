#!/bin/bash

watch --no-title '
echo "nrm stream monitor (Ctrl-c to exit)"
echo "Last updated: `date +"%T"`\n"

echo -n "# jobs in queue: "
mosps -e -J2 | tail -n +2 | wc -l

echo -n "# jobs running: "
mosps -e -J2 | tail -n +2 | grep -v "queue" | wc -l

echo -n "# jobs finished: "
ls -l results_kos | grep "res.*pkl" | wc -l
'
