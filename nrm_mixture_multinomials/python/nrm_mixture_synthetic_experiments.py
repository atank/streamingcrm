from __future__ import division

import types
import matplotlib.cm as cm

import numpy as np
import scipy.optimize as spopt

from scipy.sparse import csc_matrix

from util import make_discrete_bars
from util import make_8x8_bars_mixture
#from util import logmpoyla
import matplotlib.pyplot as plt
from nrmmixture import GGPMixtureMultinomialStream as GGPMS
from nrm_fast import pred_ll as pred_ll_fast


def plot_8x8_bars_mixture_components(stream,path,title):
    nrows = 4
    ncol = 4
    Ncomp = stream.nclust
    #nrows = int(np.ceil(Ncomp/8))
    print(stream.nu.shape)
    components = np.zeros((Ncomp,8,8))
    for i in xrange(Ncomp):
        #print(np.reshape(stream.nu[:,i],(8,8)).shape)
        components[i,:,:] = np.squeeze(np.reshape(stream.nu[:,i],(8,8)))

    for i in range(Ncomp):
        print(i)
        plt.subplot(ncol,nrows,i + 1)
        plt.imshow(components[i,:,:],cmap=cm.Greys_r,interpolation='none')
        plt.axis('off')
 
    #plt.title(title)
    plt.savefig(path)
    plt.show()

def mixture_bars_experiment():
    savepath = '/Users/alextank/StreamingBNP/doc/papers/nips_var_workshop_2014/figures/'


    a = 10
    tau = 10
    sig = .1
    alpha = .1
    newclust_eps = .1
    prune_eps = 0 
    merge_eps = .07
    Ntrain = 200
    Ntest = 5000
    obsperdat = 50
    baselineprob = .15
    test_interval = np.round(Ntrain/20)
    prune_eps = max(prune_eps,sig)
    newclust_eps = max(newclust_eps,sig)
    merge_interval = 20
    merge_metric = "l1_assignments"
    #merge_metric = "cosine"


    Xtrain,Xtest = make_8x8_bars_mixture(Ntrain,Ntest,obsperdat,baselineprob)
    Xtrain = csc_matrix(Xtrain)
    Xtest = csc_matrix(Xtest)

    print(Xtrain.shape)
    print(Xtest.shape)


    g = np.squeeze(Xtrain[:,0].toarray())
    GGPstream = GGPMS(x=g,a=a,tau=tau,sig=sig,alpha=alpha,newclust_eps=newclust_eps,
        prune_eps=prune_eps,merge_eps=merge_eps,merge=True,merge_interval=20,merge_metric =merge_metric)

    g = np.squeeze(Xtrain[:,0].toarray())
    GGPstream_nm = GGPMS(x=g,a=a,tau=tau,sig=sig,alpha=alpha,newclust_eps=newclust_eps,
        prune_eps=prune_eps,merge_eps=0,merge=False,merge_interval=2000000,merge_metric =merge_metric)


    lik_test = []
    lik_test_nm = []
    when_lik = []
    for i in xrange(1,Ntrain):
        print(i)
        g = np.squeeze(Xtrain[:,i].toarray())
        #print(g)
        GGPstream.update(g)
        GGPstream_nm.update(g)
        if (i % test_interval == 2):
            lik_test_nm.append(GGPstream_nm.pred_ll_fast(Xtest,nonew=False))
            lik_test.append(GGPstream.pred_ll_fast(Xtest,nonew=False))
            when_lik.append(i)
    plot_8x8_bars_mixture_components(GGPstream,savepath + 'bars_image.pdf','with merge')
    plot_8x8_bars_mixture_components(GGPstream_nm,savepath + 'bars_image_nm.pdf','no merge')
    print(lik_test)
    plt.plot(when_lik,lik_test,'r',when_lik,lik_test_nm,'b')
    plt.ylabel('log-likelihood')
    plt.xlabel('n')
    plt.savefig(savepath + 'bars_lik.pdf')
    plt.show()

if __name__ == "__main__":
    #okseed = 8990
    np.random.seed(892690)
    mixture_bars_experiment()











