from __future__ import division

import types

import numpy as np
import scipy.optimize as spopt
import math
import time


from scipy.sparse import csc_matrix

from util import make_discrete_bars
from nrm_fast import logmpoyla
#from util import logmpoyla
from nrm_fast import pred_ll as pred_ll_fast
import matplotlib.pyplot as plt
from scipy.special import gammaln
from scipy.misc import logsumexp
from util import make_8x8_bars_mixture
from nrm_gibbs_runner import GGPMixtureMultinomialGibbs
import pickle as pkl

lik_interval = 1
a = 1
tau = 1
sig = .5
alpha=.1
split_seed = 231
shuffle_seed = 232
infile = "/homes/alextank/streamingcrm/data/uci_bag_of_words/kos/docword.kos.pkl"
test_size = .2
f = open(infile)
X = pkl.load(f)
W, D = X.shape
np.random.seed(seed=split_seed)
te_inds= np.random.choice(D,size=np.round(test_size*D),replace=False)
#print(te_inds)
tr_inds = np.setdiff1d(np.array(range(D)),te_inds)
np.random.seed(seed=shuffle_seed)
np.random.shuffle(tr_inds)

Xtest = X[:,te_inds]
Xtrain = X[:,tr_inds]

Xtrain = Xtrain[:,:500]


#Xtrain = Xtrain[:,:60]

n = Xtrain.shape[1]
p = Xtrain.shape[0]
n_samp = 10
print(n)
print(p)
ggp_mix = GGPMixtureMultinomialGibbs(X=Xtrain, a=a, tau=tau, sig=sig,alpha=alpha,init=1,lik_interval=lik_interval)




#ggp_mix.assignments=np.array([0,0,0,0,0,1,1,1,1,1])
#ggp_mix.clust_counts = np.array([5,5])
#ggp_mix.n_clust = 2
loglik = []
for i in xrange(n_samp):
    print([i,ggp_mix.n_clust])
    ggp_mix.GGP_sample()
    loglik.append(ggp_mix.pred_ll_fast(Xtest,nonew=True))


U = np.arange(0,100,1)
pU = np.zeros(U.shape)
j = 0;
for i in U:
    pU[j] = ggp_mix.u_post(i)
    #print(pU[j])
    j += 1


V = np.arange(-2,10,.1)
pV = np.zeros(V.shape)
j = 0
for i in V:
    pV[j] = np.exp(ggp_mix.V_log_density(i))
    j += 1


plt.plot(loglik)
plt.show()

# plt.subplot(4,1,1)
# plt.plot(U,pU)
# plt.subplot(4,1,2)
# plt.hist(ggp_mix.U_samp)
# plt.subplot(4,1,3)
# plt.plot(V,pV)
# plt.subplot(4,1,4)
# plt.hist(ggp_mix.V_samp)





# #ggp_mix.clust_counts = np.array([5,5])
# #ggp_mix.assignments=np.array([0,0,0,0,0,1,1,1,1,1])
# #ggp_mix.n_clust = 2
# print(ggp_mix.logliklihood())

# plt.plot(ggp_mix.log_lik)
# plt.show()
