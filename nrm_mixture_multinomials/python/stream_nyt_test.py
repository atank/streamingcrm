from __future__ import division

import os
import time
import numpy as np 
import scipy as sp
import sys, os
import pickle as pkl
from scipy.sparse import csc_matrix
from scipy.misc import factorial, comb
from nrm_fast import pred_ll as pred_ll_fast
from nrmmixture import GGPMixtureMultinomialStream
import matplotlib
import matplotlib.pyplot as plt

start_t = time.clock()

seed = 2312
np.random.seed(seed)
test_interval = 5000


outfile = os.path.expanduser("~/streamingcrm/data/uci_bag_of_words/nytimes/docword.nytimessmall.7800.20word.pkl")
filen = open(outfile)
X = pkl.load(filen)
n,p=X.shape
print "X is %d x %d" % (n, p)



ntrain = 100 # int(n*.9)  
print(ntrain)

Xtrain = X[:,:ntrain]
Xtest = X[:,ntrain:]



g = np.squeeze(Xtrain[:,0].toarray())
streamdp = GGPMixtureMultinomialStream(x=g, a=1, tau=1, sig=0, alpha=0.5,
                          newclust_eps=0.5 ,prune_eps=0,merge_eps=.001,merge_interval=500,merge=True)
liketestdp = []
num_clusts_dp = []

for i in xrange(1,ntrain):
    #print i 
    #print streamdp.num_clusters()
    if (i % 5 == 0):
        print([i, streamdp.num_clusters()])

    g = np.squeeze(Xtrain[:,i].toarray())
    g = g.astype(float)
    streamdp.update(g)
    #num_clusts_dp.append(streamdp.num_clusters())

    #if i >= test_interval and i % test_interval == 0:
    #    liketestdp.append(streamdp.pred_ll_fast(Xtest,nonew=False))
    #    #num_clusts_dp.append(streamdp.num_clusters())

    #    #num_clusts_inv.append(streaminv.num_clusters())

print "Script took: %.4f" % (time.clock() - start_t,)


#plt.figure()
#plt.subplot(2,1,1)
#plt.plot(liketestdp, 'b')
#plt.subplot(2,1,2)
#plt.plot(num_clusts_dp, 'b')
#plt.show()
