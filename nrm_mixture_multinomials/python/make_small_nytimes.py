import os
import numpy as np
import scipy as sp
import pickle as pkl
from scipy.sparse import csc_matrix


smallsize = .05
nyt_path = os.path.expanduser("~/streamingcrm/data/uci_bag_of_words/nytimes/")
#nyt_file = "ldac.nytimes.7800.K100.pseudocounts.pkl"
nyt_file = "docword.nytimes.7800.20word.pkl"
filen = open(nyt_path + nyt_file)
X = pkl.load(filen)
W,D = X.shape
seed = 10000
print(int(D*smallsize))
include = np.random.choice(range(D),size = int(D*smallsize),replace=False)

Xsmall = X[:,include]
Xsmall = csc_matrix(Xsmall)

#filename = nyt_path + "ldac.nytimessmall.7800.K100.pseudocounts.pkl"
filename = nyt_path + "docword.nytimessmall.7800.20word.pkl"
pkl.dump(Xsmall,open(filename,'wb'))
