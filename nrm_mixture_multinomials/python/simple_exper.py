from __future__ import division

import numpy as np

from scipy.sparse import csc_matrix
from scipy.io import mmread

from nrmmixture import GGPMixtureMultinomialStream as GGPMixStream


# Hack because this isn't a real package
import sys
sys.path.append("../../util")
import textutil as tutil


def run_exper(vfile, dfile, vminfreq=0, vmaxfreq=np.inf):
    """ Run a simple streaming "topic modeling" experiment
        
        vfile : string, path to file containing vocabulary
        dfile : string, path to file containing word counts in format
                D
                W
                nnz
                wordid docid count
                ...
                wordid docid count

        vminfreq : Minimum number of documents word appears in to be in vocab.
        vmaxfreq : Minimum number of documents word appears in to be in vocab.
    """

    print "Loading vocab"
    vocab_full = tutil.load_vocab(vfile)

    print "Loading word counts"
    X_full = tutil.load_word_counts(dfile)

    #print "Filter vocabulary to get rid of junk"
    #X, vocab = tutil.filter_vocab_tfidf(X_full, vocab_full, vminfreq, vmaxfreq)
    X, vocab = X_full, vocab_full

    # Grab some entries for test set

    print "Running streaming algorithm"
    # Run streaming algorithm
    x = X[:,0].toarray()
    ggp_stream = GGPMixStream(x, a=1., tau=1., sig=1e-4, alpha=0.1,
                              newclust_eps=0.1)

    for n in xrange(1, D):
        if n % 10 == 0:
            print "Processing doc %d of %d (%d topics)" % (n, D,
                                                ggp_stream.rho.shape[0])
        x = X[:,n].toarray()
        
        #import ipdb; ipdb.set_trace()
        ggp_stream.update(x)

    return X, vocab, ggp_stream


if __name__ == "__main__":

    import os
    import sys
    argv = sys.argv[1:]
    argc = len(argv)

    vfile = os.path.expanduser(argv[0])
    dfile = os.path.expanduser(argv[1])

    X, vocab, ggp_stream = run_exper(vfile, dfile)
