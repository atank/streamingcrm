from __future__ import division

import types
import pandas as pd
import numpy as np
import scipy.optimize as spopt
import math
import time
import os

from scipy.sparse import csc_matrix
import sys
from util import make_discrete_bars
from nrm_fast import logmpoyla
#from util import logmpoyla
from nrm_fast import pred_ll as pred_ll_fast
import matplotlib.pyplot as plt
from scipy.special import gammaln
from scipy.misc import logsumexp
from util import make_8x8_bars_mixture
from nrm_gibbs_runnerBACKUP import GGPMixtureMultinomialGibbs
import pickle as pkl

def run_exper(a=None,tau=None,sig=None,shuffle_seed=None,split_seed=None):
    save_int = 5
    alpha = .1
    lik_interval = 1
    n_samp = 300
    respath = "/homes/alextank/streamingcrm/nrm_mixture_multinomials/python/gibbs_res/"
    infile = "/homes/alextank/streamingcrm/data/uci_bag_of_words/kos/docword.kos.pkl"
    test_size = .2
    f = open(infile)
    X = pkl.load(f)
    W, D = X.shape
    np.random.seed(seed=split_seed)
    print(D)
    print(test_size)
    print(np.round(test_size*D))
    te_inds= np.random.choice(D,size=np.round(test_size*D),replace=False)
    #print(te_inds)
    tr_inds = np.setdiff1d(np.array(range(D)),te_inds)
    #resetseed to resample the ordering of the training data
    np.random.seed(seed=shuffle_seed)
    np.random.shuffle(tr_inds)

    Xtest = X[:,te_inds]
    Xtrain = X[:,tr_inds]
    #Xtrain = Xtrain[:,:50]

    ggp_mix = GGPMixtureMultinomialGibbs(X=Xtrain, a=a, tau=tau, sig=sig,alpha=alpha,init=1,lik_interval=lik_interval)
    res_name = "nrmgibbs-shuffle_seed-%d-split_seed-%d-a-%d-tau-%d-sig-%d.pdf" % (shuffle_seed,split_seed,a,tau,sig)


    loglik = []
    nclusts = []
    sys.stdout.flush()
    sys.stderr.flush()


    for i in xrange(n_samp):
        print([i,ggp_mix.n_clust])
        ggp_mix.GGP_sample()
        loglik.append(ggp_mix.pred_ll_fast(Xtest,nonew=True))
        nclusts.append(ggp_mix.n_clust)
        sys.stdout.flush()
        sys.stderr.flush()

        if (i >= save_int and i % save_int == 0):
            full_model_loglik = ggp_mix.log_lik
            df = pd.DataFrame({'nclusts':nclusts,'loglik':loglik,'probmodel':full_model_loglik})
            print("saving")
            outpath = os.path.join(respath,res_name + "D.csv")
            with open(outpath, 'w') as f:
                df.to_csv(f)
            #full_model_loglik = ggp_mix.log_lik
            #res_dict = {'gibsnrm': ggp_mix, 'a': a, 'tau':tau, 'sig':sig,
            #    'alpha':alpha, 'shuffle_seed':shuffle_seed, 
            #    'split_seed':split_seed,'num_clusters':nclusts,
            #    'loglik':loglik,'model_log_lik':full_model_loglik,
            #    'nsamp':n_samp}

            #res_name = "nrmgibbs-shuffle_seed-%d-split_seed-%d-a-%d-tau-%d-sig-%d.pdf" % (shuffle_seed,split_seed,a,tau,sig)

            #outpath = os.path.join(respath, res_name + "D.pkl")
            #with open(outpath, 'w') as f:
            #    pkl.dump(res_dict, f)
            #    f.close()
            #return

       
    full_model_loglik = ggp_mix.log_lik

    #respath = os.path.expanduser(os.path.expandvars(respath))

    res_dict = {'gibsnrm': ggp_mix, 'a': a, 'tau':tau, 'sig':sig,
    'alpha':alpha, 'shuffle_seed':shuffle_seed, 'split_seed':split_seed,
    'num_clusters':nclusts,
    'loglik':loglik,'model_log_lik':full_model_loglik,'nsamp':n_samp}

    res_name = "nrmgibbs-shuffle_seed-%d-split_seed-%d-a-%d-tau-%d-sig-%d.pdf" % (shuffle_seed,split_seed,a,tau,sig)

    outpath = os.path.join(respath, res_name + "D.pkl")
    with open(outpath, 'w') as f:
        pkl.dump(res_dict, f)
        f.close()
    return



if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Run NRM document clustering experiment.")
    parser.add_argument('a', type=float, help="")
    parser.add_argument('tau', type=float, help="")
    parser.add_argument('sig', type=float, help="")
    parser.add_argument('shuffle_seed', type=int, help="")
    parser.add_argument('split_seed',type=int,help="")

    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)
    run_exper(**args)

