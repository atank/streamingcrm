from __future__ import division

import os
import cPickle as pkl
import numpy as np
import matplotlib.pyplot as plt


def get_top_words(nu_in, vocab, rho=None, L=10):
    """ Determine L most probable words in each topic.

        K is the number of topics.
        
        nu_in : V X K ndarray of topics (not necessarily normalized)
        vocab : list of words making up the vocabulary
        rho : [optional] K-ndarray of topic proportions in corpus to sort
              results.  NOT USED RIGHT NOW.
        L : int, number of words to return for each topic

        Returns:
        list of lists of top words, one element for each topic.  Each element
        is a list of tuples of the form (word, probability).
    """
    nu = nu_in / np.sum(nu_in, axis=0)[np.newaxis,:]
    top_words = list()

    V, K = nu.shape

    for k in xrange(K):
        if k % 10 == 0:
            print "Topic: %d" % k
        nu_k = np.squeeze(nu[:,k])
        inds = np.argsort(nu_k)[::-1]

        these_words = [(vocab[w], nu_k[w]) for w in inds[:L]]
        top_words.append(these_words)

    return top_words


if __name__ == "__main__":

    vocab_file = os.path.expanduser("~/work/uw/projects/streaming_bnp/streamingcrm/data/uci_bag_of_words/nytimes/vocab.nytimes.7800.txt")
    dp_resfile = "results_nyt/res-a-1000_00-tau-1_00-sig-0_00-alpha-0_50-neps-0_50-meps-0_00-minterval-500_00-shuffle_seed-7386-split_seed-86975309D.pkl"
    ggp_resfile = "results_nyt/res-a-100_00-tau-100_00-sig-0_50-alpha-0_50-neps-0_50-meps-0_00-minterval-500_00-shuffle_seed-7386-split_seed-86975309D.pkl"

    print "Loading vocab..."
    with open(vocab_file, 'r') as f:
        vocab = [v.strip() for v in f.readlines()]

    print "Loading nrm data..."
    with open(ggp_resfile, 'r') as f:
        d = pkl.load(f)
    nrm = d['nrm_mm']

    print "Loading dp data..."
    with open(dp_resfile, 'r') as f:
        d = pkl.load(f)
    dp = d['nrm_mm']

    # V X K matrix of topics
    nu = nrm.nu
    nrm_weights = nrm.rho_sum / np.sum(nrm.rho_sum)
    dp_weights = dp.rho_sum / np.sum(dp.rho_sum)

    nrm_wsrt = np.sort(nrm_weights)[::-1]
    dp_wsrt = np.sort(dp_weights)[::-1]

    nrm_tailfrac = 1. - np.cumsum(nrm_wsrt)
    dp_tailfrac = 1. - np.cumsum(dp_wsrt)

    print "Processing top words..."
    nwords = 5
    top_words = get_top_words(nu, vocab, L=nwords)
    # Write some top words to latex table format
    ntopics = 4
    tab_str =  "\\begin{tabular}{|"
    tab_str += "c|" * ntopics
    tab_str += "}\n\\hline\n"
    for k in xrange(ntopics-1):
        tab_str += "\\textbf{Topic %d} & " % (k+1,)
    tab_str += "\\textbf{Topic %d} " % ntopics
    tab_str += "\\\\ \\hline\n"
    # This is inefficient, but I used a dumb data structure
    for w in xrange(nwords):
        for k in xrange(ntopics-1):
            words, probs = zip(*top_words[k])
            tab_str += "%s (%.4f) & " % (words[w], probs[w])
        words, probs = zip(*top_words[ntopics-1])
        tab_str += "%s (%.4f) " % (words[w], probs[w])
        tab_str += "\\\\ \\hline\n"
    tab_str += "\\end{tabular}"
    print "Latex string for table of topic words."
    print tab_str + "\n"

    print "Comparing weights..."
    nweights = 25
    #plt.plot(np.log(nrm_tailfrac), '.')
    plt.scatter(np.log(np.arange(nrm_wsrt.shape[0])+1), np.log(nrm_wsrt),
                marker='x', c='#f8766d', alpha=0.75)
    plt.hold(True)
    #plt.plot(np.log(dp_tailfrac), '*')
    plt.scatter(np.log(np.arange(dp_wsrt.shape[0])+1), np.log(dp_wsrt),
                marker='o', c="#00bfc4", edgecolor='none', alpha=0.75)
    plt.legend(['IG', 'DP'], scatterpoints=1)
    plt.xlabel('Rank (log scale)', fontsize=14)
    plt.ylabel('Weight (log scale)', fontsize=14)
    plt.show()
    plt.savefig("./nyt_weights.pdf", pad_inches=0.0)
