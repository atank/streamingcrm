from __future__ import division

import numpy as np
from scipy.special import gammaln

eps = np.finfo('float64').eps


def logmpoyla(counts, alpha):
    #print(counts)
    #print(alpha)
    asum = np.sum(alpha, axis=0)
    #print(alpha)
    #print(asum)
    M = np.sum(counts, axis=0)

    lprob = gammaln(asum + eps) + gammaln(M + 1.) - gammaln(M + asum + eps)
    lprob += np.sum(gammaln(counts + alpha + eps) \
                    - gammaln(alpha + eps) \
                    - gammaln(counts + 1.), axis=0)
    return lprob



def make_8x8_bars_mixture(Ntrain,Ntest,obsperdat,baselineprob):
    nbars = 8
    size = 8
    N = Ntrain + Ntest
    bars = np.zeros([size, size, size]) + baselineprob
    bars[0,0,] = 1
    bars[1,2,] = 1  
    bars[2,4,] = 1
    bars[3,6,] = 1
    bars[4,:,0] = 1
    bars[5,:,2] = 1
    bars[6,:,4] = 1
    bars[7,:,6] = 1
    dat_v = np.zeros((size*size,N))
    for i in xrange(0,Ntrain+Ntest):
        on = np.random.randint(8,size=1)
        dat_v[:,i] = np.random.multinomial(obsperdat,bars[on,:,:].flatten()/np.sum(bars[on,:,:].flatten()))

    Xtrain = dat_v[:,:Ntrain]
    Xtest = dat_v[:,Ntrain:(Ntrain + Ntest)]
    return(Xtrain, Xtest)



def make_discrete_bars(N, alpha_v, Nclust, V, alpha_w, obsperdat):
    #create clusters
    topic_prop = np.zeros((V, Nclust))
    weights = np.random.dirichlet(alpha_w*np.ones(Nclust))

    X = np.zeros((V, N))
    labels = np.zeros(N)
    for i in xrange(Nclust):
        topic_prop[:,i] = np.random.dirichlet(alpha_v*np.ones(V))

    for i in xrange(N):
        ind = np.random.multinomial(1,weights)
        ind = np.where(ind)[0][0]
        labels[i] = ind
        X[:,i] = np.random.multinomial(obsperdat,topic_prop[:,ind])

    return X, topic_prop, labels, weights


def top_topic_words(topics, vocab, M):
    """ Return list of lists containing the top M words from each topic.
        
        topics : V x K ndarray, topic distributions.
        vocab : list of length V.
        M : int > 0, number of words for each topic.
    """

    top = list()

    for k in xrange(topics.shape[1]):
        p = topics[:,k]
        p /= np.sum(p)

        # Sort and then reverse to get descending
        inds = np.argsort(p)[::-1]

        tmp = [vocab[i] for i in inds[:M]]

        top.append(tmp)

    return top
