
import numpy as np
cimport numpy as np

import cython
cimport cython

from libc.math cimport log1p, exp, log
from libc.stdlib cimport malloc, free
from libc.stdio cimport printf
from libc.string cimport memset

from scipy.sparse import csc_matrix
from scipy.special import gammaln as sp_gammaln


cdef extern from "gsl/gsl_sf_gamma.h":
    double gsl_sf_lngamma(double x)


cdef inline double gammaln(double x):
    return gsl_sf_lngamma(x)


cdef double logaddexp(double x, double y):
    """ Grabbed this source from :
        https://github.com/gravitationalarry/bayestar-localization/blob/master/bayestar/logaddexp.h
    """
    cdef double tmp = x - y
    if tmp > 0:
        return x + log1p(exp(-tmp))
    elif tmp <= 0:
        return y + log1p(exp(tmp))
    else:
        # NaNs or infinities of the same sign
        return x + y


cdef double eps = np.finfo('float64').eps

def test_laexp():
    print "np: %f\n" % np.logaddexp.reduce(np.array([-0.1, -0.01]))
    print "ours: %f\n" % logaddexp(-0.1, -0.01)
    return


#TODO: Take sparse matrix structure and only loop over non-zeros
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def logmpoyla(np.ndarray[np.double_t, ndim=2] counts,
              np.ndarray[np.double_t, ndim=2, mode='fortran'] alpha):
    """
        counts : V x 1 matrix of word counts
        alpha : V x K+1 matrix of topics
    """

    cdef np.ndarray[np.double_t, ndim=1] asum
    cdef double M = 0.  # Could be an int, but counts is already double
    cdef np.ndarray[np.double_t, ndim=1] lprob
    cdef int k, v
    cdef double c_v, a_k
    cdef int K = alpha.shape[1]
    cdef int V = counts.shape[0]

    lprob = np.empty(K)
    asum = np.zeros(K)

    #M = np.sum(counts, axis=0)
    for v in xrange(V):
        M += counts[v,0]
    #asum = np.sum(alpha, axis=0)
    for k in xrange(K):
        for v in xrange(V):
            asum[k] += alpha[v,k]

    for k in xrange(K):
        lprob[k] = gammaln(M + 1.) + gammaln(asum[k] + eps) \
                   - gammaln(M + asum[k] + eps)
        for v in xrange(V):
            a_k = alpha[v,k]
            c_v = counts[v,0]  # There's always only one column of counts
            #printf("c_v: %.2f, a_k: %.2f\n", c_v, a_k)
            lprob[k] += gammaln(c_v + a_k + eps) - gammaln(a_k + eps) \
                        - gammaln(c_v + 1.)

    #lprob2 = sp_gammaln(asum + eps) + sp_gammaln(M + 1.) - sp_gammaln(M + asum + eps)
    #lprob2 += (np.sum(sp_gammaln(counts + alpha + eps) - sp_gammaln(alpha + eps)
    #                  - sp_gammaln(counts + 1.), axis=0))
    #return lprob, lprob2
    
    return lprob


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef void logmpoyla_(double* lprob, long* rows, double* counts,
                     double* alpha, double* asum, int K, int V, int W):
    """
        lprob : K vector where result is stored
        rows : Vector storing row indices of nonzeros
        counts : Vector storing nonzero counts
        alpha : V x K+1 matrix of topics.  Fortran order.
        asum : K vector with column sums of alpha
        K : Number of clusters
        V : Number of unique words in current doc (NOT the number of words in
            the vocab as it is elsewhere)
        W : Number of words in vocab (need for alpha indexing)
    """
    cdef double M = 0.  # Could be an int, but counts is already double
    cdef int k, v, ri
    cdef double c_v, a_k

    #M = np.sum(counts, axis=0)
    for v in xrange(V):
        M += counts[v]

    for k in xrange(K):
        lprob[k] = gammaln(M + 1.) + gammaln(asum[k] + eps) \
                   - gammaln(M + asum[k] + eps)
        for v in xrange(V):
            ri = rows[v]
            a_k = alpha[W*k + ri]  # Fortran order
            c_v = counts[v]
            lprob[k] += gammaln(c_v + a_k + eps) - gammaln(a_k + eps) \
                        - gammaln(c_v + 1.)


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def pred_ll(self, Xtest, nonew=False):
    """ Predictive log-likelihood of held out documents.
        
        Xtest : scipy.sparse.csc matrix of test documents
        nonew : boolean, True means don't use the "new" cluster in predictions,
                False (the default) uses the "new" cluster to evaluate
                predictions.

        Returns:
        lprob : double, predictive log-likelihood
    """
    assert type(Xtest) == csc_matrix

    cdef int W, D, K, k
    cdef double U, ll
    cdef double[:] prior_assign
    cdef double pa_sum = 0.
    cdef double[::1,:] nu_pred_view
    cdef long[:] inds
    cdef long[:] ind_ptr
    cdef double[:] data
    W, D = Xtest.shape
    K = self.nu.shape[1]
    U = self.update_U()
    prior_assign = self.prior_assign(U)
    # Construct this way to retain Fortran ordering

    if nonew:
        nu_pred = np.empty((W, K), order='F')
    else:
        nu_pred = np.empty((W, K+1), order='F')
        nu_pred[:,-1] = self.alpha_onesV
    nu_pred[:,:K] = self.nu
    nu_pred_view = nu_pred

    if nonew:
        prior_assign = prior_assign[:-1]
        for k in xrange(K):
            pa_sum += prior_assign[k]
        for k in xrange(K):
            prior_assign[k] /= pa_sum

    inds = Xtest.indices.astype(np.int)
    ind_ptr = Xtest.indptr.astype(np.int)
    data = Xtest.data.astype(np.double)

    ll = pred_ll_work(W, D, inds, ind_ptr, data, prior_assign, nu_pred_view)

    return ll


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef double pred_ll_work(int W, int D,
                         long[:] inds,
                         long[:] indptr,
                         double[:] data,
                         double[:] prior_assign,
                         double[::1,:] nu_pred):
    """ Meat of work to compute predictive log-likelihood for held out
        documents.
        
        counts : Sparse word count matrix
        prior_assign : Vector of prior cluster assignments
        nu_pred : Variational estimates of cluster centers
    """
    cdef double ll = 0.
    cdef int K = prior_assign.shape[0]
    cdef int d, k, v, i1, i2
    cdef double tmp 

    cdef long *rows
    cdef double *lp_z
    cdef double *lmp
    cdef double *counts
    cdef double *asum
    lp_z = <double*>malloc(K*sizeof(double))
    for k in xrange(K):
        lp_z[k] = log(prior_assign[k] + eps)

    lmp = <double*>malloc(K*sizeof(double))
    asum = <double*>malloc(K*sizeof(double))
    memset(asum, 0, K*sizeof(double))

    # nu_pred is in fortran order, there's probably a BLAS function to do this
    # faster.
    for k in xrange(K):
        for v in xrange(W):
            asum[k] += nu_pred[v, k]

    for d in xrange(D):
        i1 = indptr[d]
        i2 = indptr[d+1]
        V = i2 - i1

        rows = &inds[i1]
        counts = &data[i1]

        logmpoyla_(lmp, rows, counts, &nu_pred[0,0], &asum[0], K, V, W)

        tmp = lp_z[0] + lmp[0]
        for k in xrange(1,K):
            tmp = logaddexp(tmp, lp_z[k] + lmp[k])
        ll += tmp

    free(lmp)
    free(asum)
    free(lp_z)

    return ll
