from __future__ import division
import numpy as np 
import scipy as sp
#import mpmath
import sys, os
import pickle as pkl
from scipy.sparse import csc_matrix
from scipy.misc import factorial, comb, logsumexp
from scipy.special import gammaln


def logass(Xtest,nu,assignments):
    N = Xtest.shape[1]
    llik = 0
    for i in xrange(N):
        ind = assignments[i]
        llik += l_mult(Xtest[:,i],nu[ind,:])
    return(llik)




def loglik_baseline(Xtest,nu,weights):
    N = Xtest.shape[1]
    K = weights.shape[0]
    llik = 0
    for i in xrange(N):
        #print(i)
        loc = np.zeros(K)
        for j in xrange(K):
            #print(weights[j])
            #print(nu.shape)
            #print(weights.shape)
            #print(loc.shape)
            #print(K) 
            loc[j] = np.log(weights[j]) + l_mult(Xtest[:,i],nu[j,:])
        llik += logsumexp(loc)
    return(llik)


def post(Xtrain,assignments,alpha):
    un = np.array(np.unique(assignments))
    K = np.shape(un)[0]
    print(K)
    print(un)
    nu = np.zeros((V,K)) + alpha
    weights = np.zeros(K)
    N = Xtrain.shape[1]
    j = 0
    for i in un:
        idx = np.where(assignments == i)[0]
        #print(max(idx))
        #print(idx)
        q = Xtrain[:,idx]
        #print(q.shape)
        #print(np.sum(q,1).shape)
        nu[:,j] += np.sum(Xtrain[:,idx],axis = 1)
        weights[j] = np.shape(idx)[0]
        #print(weights[j])
        j = j + 1
    return(nu,weights/N)


def pred_post(Xtest,nu,weights):
    llik = 0
    N = Xtest.shape[1]
    K = np.shape(weights)[0]
    for i in xrange(N):
        print(i)
        loc = np.zeros(K)
        for j in xrange(K):
            loc[j] = np.log(weights[j]) + lmp(Xtest[:,i],nu[:,j])
        #print(loc)
        llik += logsumexp(loc)
    return(llik)


def lmp(x,nu):
    llik = 0
    N = np.sum(x)
    llik += gammaln(N + 1)
    llik += -np.sum(gammaln(x + 1))
    llik += gammaln(np.sum(nu))
    llik += -gammaln(np.sum(nu) + N)
    llik += np.sum(gammaln(x + nu))
    llik += -np.sum(gammaln(nu))
    return(llik)

def l_mult(x,p):
    N = np.sum(x) 
    llik = 0
    llik += gammaln(N+1)
    llik -= np.sum(gammaln(x+1))
    llik += np.sum(x*np.log(p))
    return(llik)


def generate_topics(k,V,alpha):
    nu = np.zeros((k,V))
    alpha = np.ones(V)*alpha
    for i in xrange(k):
        nu[i,:] = np.random.dirichlet(alpha)
    return(nu)

def generate_documents(assignments,cluster_counts,k,V,nu,n,mwords,noisewords):
    X = np.zeros((V,n))
    unif_prob = np.zeros(V)
    unif_prob += 1
    unif_prob /= np.sum(unif_prob)
    for i in xrange(n):
        clid = assignments[i]
        X[:,i] = np.random.multinomial(mwords,nu[clid,:])
        X[:,i] += np.random.multinomial(noisewords,unif_prob)
    return(X)


def generate_pitman_yor(a,sigma,n):
    cluster_counts = np.zeros(n)
    num_clusters = np.zeros(n)
    k = 0
    cluster_counts[0] = 1
    assignments = np.zeros(n)
    for i in xrange(1,n):
        print(i)
        print(k)
        #oldp = (cluster_counts[:k] - sigma)/(i + a)
        newp = (a + k*sigma)/(i + a)
        prob = np.zeros(k + 1)
        prob[:k] = (cluster_counts[:k] - sigma)/(i + a)
        prob[k] = newp
        new = np.random.multinomial(1,prob)
        whichassigned = np.where(new == 1)[0]
        #print(whichassigned)
        cluster_counts[whichassigned] += 1
        if whichassigned == k:
            k = k + 1
        num_clusters[i] = k
        assignments[i] = whichassigned
    return(cluster_counts,k,assignments,num_clusters)

if __name__ == "__main__":
    seed = 2312
    np.random.seed(seed)
    alpha = .75
    V = 200
    sigma = .75
    a = 1
    n = 10000
    noisewords = 0
    mwords = 50
    cluster_counts,k,assignments,num_clusters = generate_pitman_yor(a,sigma,n)
    nu = generate_topics(k,V,alpha)
    test_size = .2

    X = generate_documents(assignments,cluster_counts,k,V,nu,n,mwords,noisewords)
    Xsmall = X[:,:int(n/10)]
    #X = csc_matrix(X)
    Xsmall = csc_matrix(Xsmall)

    #print(X.todense())
    base = '/homes/alextank/streamingcrm/data/uci_bag_of_words/sim/'
    
    split_seed = 86975309
    np.random.seed(seed=split_seed)
    te_inds = np.random.choice(n,size = np.round(test_size*n),replace=False)
    tr_inds = np.setdiff1d(np.array(range(n)),te_inds)
    Xtrain = X[:,tr_inds]
    Xtest = X[:,te_inds]
    assignments_test = assignments[te_inds]
    assignments_train = assignments[tr_inds]
    #Xtest = np.array(Xtest.todense())
    #Xtrain = np.array(Xtrain.todense())
    #final_loglik = loglik_baseline(Xtest,nu,cluster_counts[:k]/n)
    #loglikwass = logass(Xtest,nu,assignments_test)
    #print(final_loglik)
    #print(loglikwass)
    nu,weights = post(Xtrain,assignments_train,alpha)
    llik = pred_post(Xtest,nu,weights)
    print(llik)
    print(np.sum(weights))


    
    #outfile = "pityordocs_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + ".npy"
    #np.save(outfile,[X, assignments])
    
    #store as a pickle file
    
#    outfile = "pityornclust_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + "_noisewords" + str(noisewords) + ".pkl"
#outfile = base + 'nclusts.csv'
#np.savetxt(outfile,num_clusters,delimiter=",")    
#pkl.dump(num_clusters,open(outfile,'wb'))

#    outfile = "pityordocs_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + "_noisewords" + str(noisewords) + ".pkl"
#outfile = base + 'docword.sim.pkl'   
#pkl.dump(X,open(outfile,'wb'))

#    outfile = "pityordocsSMALL_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + "_noisewords" + str(noisewords) +  ".pkl"
#outfile = base + 'docword.simsmall.pkl'    
#pkl.dump(Xsmall,open(outfile,'wb'))

    outfile = "pityornclust_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + ".pkl"
    pkl.dump(num_clusters,open(outfile,'wb'))

    outfile = "pityordocs_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + ".pkl"
    pkl.dump(X,open(outfile,'wb'))

    outfile = "pityordocsSMALL_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + ".pkl"
    pkl.dump(Xsmall,open(outfile,'wb'))


        
