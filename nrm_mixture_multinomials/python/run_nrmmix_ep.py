from __future__ import division

import os
import sys

import cPickle as pkl

import numpy as np

from scipy.sparse import csc_matrix
from scipy.io import mmread
from nrm_fast import pred_ll as pred_ll_fast

from sklearn.cross_validation import ShuffleSplit

from nrmmixture_EP import GGPMixtureMultinomialStream as GGPMixStream


def run_exper(vfile=None, dfile=None, respath=None, a=None, tau=None, sig=None,
            alpha=None, neps=None, merge_eps=None,merge_interval=None, test_size=None,
			test_interval=None, shuffle_seed=None,split_seed=None,Nep=None,Kthresh=-1):
    """ Cluster documents and save resulting object.
        
        vfile : path to vocab file
        dfile : path to pkl file containing doc word counts (pkl file
                     should be binary)
        respath : path to directory to store results
        a : param (see nrmmixture.py)
        tau : param
        sig : param
        alpha : param
        neps : param
        test_size : fraction of documents in test set
        test_interval : how often to evaluate test criteria
        seed : random seed to create test split and permutation of documents
        Kthresh : maximum number of clusters that can be used, by default it is
                  -1 which will cause no threshold to be used.
    """
    
    if Kthresh < 0:
        Kthresh = np.inf

    # Load data
    cpath = os.path.expanduser(os.path.expandvars(dfile))
    with open(cpath, 'rb') as f:
        X = pkl.load(f)
    print(X.dtype)
    X = X.astype("float")
    W, D = X.shape

    # Train/test split, returned indices are randomly permuted
    np.random.seed(seed=split_seed)
    print(D)
    print(test_size)
    print(np.round(test_size*D))
    te_inds= np.random.choice(D,size=np.round(test_size*D),replace=False)
    print(te_inds)
    tr_inds = np.setdiff1d(np.array(range(D)),te_inds)
	#resetseed to resample the ordering of the training data
    np.random.seed(seed=shuffle_seed)
    np.random.shuffle(tr_inds)
	
    #ss = ShuffleSplit(D, n_iter=1, test_size=test_size, random_state=seed)
    #for tr_inds, te_inds in ss:
        # Extract training and test set (there's only onee here)
    #    pass

    Xtest = X[:,te_inds]
    Xtrain = X[:,tr_inds]
    test_llik = list()
    num_clusts = list()
    Ntrain = np.shape(Xtrain)[1]
    # Run streaming algorithm
    x = np.squeeze(X[:,tr_inds[0]].toarray())
    ggp_stream = GGPMixStream(x, a=a, tau=tau, sig=sig, alpha=alpha,
							 newclust_eps=neps,merge_eps=merge_eps,
							merge_interval=merge_interval,n_total=Ntrain)

    #ssrint "SMALL DATA SETS FOR DEBUGGING!  REMOVE BEFORE RUNNING"
    #tr_inds = tr_inds[1:10]
    #te_inds = te_inds[1:10]
    failed = False
    for j in range(Nep):

        for i, n in enumerate(tr_inds):
   
             ind = Ntrain*j + i
             if ind % 100 == 0:
                 print "doc: %d, |C|: %d" % (i, ggp_stream.num_clusters())
                 print(j)
             
             x = np.squeeze(Xtrain[:,i].toarray())
   
             # Only consider docs with enough words
             if np.sum(x) < 20:
                 continue
             if j > 0:
                 ggp_stream.EP_update(x,i)
             else:
                 if i > 0: 
                     ggp_stream.update(x)  
             #ggp_stream.update(x)
   
             # Kill experiment if used too many clusters b/c it probably won't
             # finish.
             K = ggp_stream.num_clusters() 
             if K > Kthresh:
                 print "Error: # clusters (%d) exceeds Kthresh (%d)" % (K, Kthresh),
                 failed = True
                 break
   
             # Evaluate
             if ind >= test_interval and ind % test_interval == 0:
                 print "  evaluating test ll ",
                 tmp = ggp_stream.pred_ll_fast(Xtest,nonew=False) 
                 #tmp = ggp_stream.pred_ll_nonew(Xtest)
                 test_llik.append(tmp)
                 print "%.2f" % tmp
   
                 num_clusts.append(ggp_stream.num_clusters())
   
             # So we can see progress in the logs
        sys.stdout.flush()
        sys.stderr.flush()
    
	 
    # Write results
    respath = os.path.expanduser(os.path.expandvars(respath))
    weights = np.zeros(Kthresh)
    weights[:ggp_stream.num_clusters()]=ggp_stream.rho_sum/np.sum(ggp_stream.rho_sum)
    if not os.path.exists(respath):
        os.makedirs(respath)

    if not failed:
        res_dict = {'nrm_mm': ggp_stream, 'a': a, 'tau': tau, 'sig': sig,
                    'alpha': alpha, 'neps': neps, 'merge_eps': merge_eps, 'merge_interval':merge_interval, 'test_size': test_size,
                    'shuffle_seed': shuffle_seed, 'split_seed': split_seed,' vfile': vfile, 'test_llik': test_llik,
                    'num_clusters': num_clusts,'weights': weights}
    else:
        res_dict = {'nrm_mm': None, 'a': a, 'tau': tau, 'sig': sig,
                    'alpha': alpha, 'neps': neps, 'test_size': test_size,
                    'seed': seed, 'vfile': vfile, 'test_llik': None,
                    'num_clusters': None}

    res_name = "res-%s-shuffle_seed-%d-split_seed-%d" % (ggp_stream.name(),shuffle_seed,split_seed)

    outpath = os.path.join(respath, res_name + "D.pkl")
    with open(outpath, 'w') as f:
        pkl.dump(res_dict, f)
        f.close()
    return


if __name__ == "__main__":
    print('started')
    import argparse

    parser = argparse.ArgumentParser(description="Run NRM document clustering experiment.")
    parser.add_argument('vfile', type=str,
                        help="path to vocab file")
    parser.add_argument('dfile', type=str,
                        help="path to binary pickle file storing sparse matrix of counts")
    parser.add_argument('respath', type=str, help="path to results directory")
    parser.add_argument('a', type=float, help="")
    parser.add_argument('tau', type=float, help="")
    parser.add_argument('sig', type=float, help="")
    parser.add_argument('alpha', type=float, help="")
    parser.add_argument('neps', type=float, help="")
    parser.add_argument('merge_eps', type=float,help="")
    parser.add_argument('merge_interval',type=int,help="")
    parser.add_argument('test_size', type=float, help="")
    parser.add_argument('test_interval', type=int, help="")
    parser.add_argument('shuffle_seed', type=int, help="")
    parser.add_argument('split_seed',type=int,help="")
    parser.add_argument('Nep',type=int,help="")
    parser.add_argument('Kthresh', type=float, default=np.inf,
                        help="Maximum number of clusters allowd")

    # Example of optional argument
    #parser.add_argument('--seed', type=int,
    #                    help="Seed for random number generator", default=0)
    
    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)

    np.seterr(invalid='raise')

    # Run code
    print('running code')
    run_exper(**args)
