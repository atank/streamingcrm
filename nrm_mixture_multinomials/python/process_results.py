from __future__ import division

import os
import glob

import cPickle as pkl

import numpy as np
import pandas as pd
import sys


def process_results(respath, outfile):
    """ Go through NRM result files data frame to make plots with.  Save to
        outfile.

        respath : path to directory where result files are stored
        outpath : path to directory to store resulting data frames
    """
    
    # Lists to construct DataFrame from
    a_l = list()
    alpha_l = list()
    sig_l = list()
    tau_l = list()
    seed_l = list()
    testll_l = list()
    nclust_l = list()
    iter_l = list()
    merge_eps_l = list()

    fnames = glob.glob(os.path.join(respath, "*.pkl"))
    R = len(fnames)
    ind_end = 0
    for i, fn in enumerate(fnames):
        print "Processing file %d of %d" % (i+1, R)

        try:
            with open(fn, 'r') as f:
               d = pkl.load(f)
        except:
            print("there was an error")
            continue
        if d['nrm_mm'] is None:
            continue
        a = d['a']
        alpha = d['alpha']
        seed = d['shuffle_seed']
        sig = d['sig']
        tau = d['tau']
        nclust = d['num_clusters']
        test_ll = d['test_llik']
        #weights= d['weights']         
        #K = len(weights)
        M = len(test_ll)
        merge_eps = d['merge_eps']

        a_l.extend([a]*M)
        alpha_l.extend([alpha]*M)
        sig_l.extend([sig]*M)
        tau_l.extend([tau]*M)
        seed_l.extend([seed]*M)
        testll_l.extend(test_ll)
        nclust_l.extend(nclust)
        iter_l.extend(xrange(M))
        merge_eps_l.extend([merge_eps]*M)
        ind_start = ind_end
        ind_end = ind_start + M
        #if i == 0:
        #    weights_l = np.zeros((R*M,K))
        #    weights_l = np.zeros((143,K))

        #weights_l[ind_start:ind_end,:] = weights
    # Create DataFrame from lists
    df = pd.DataFrame({'a': a_l, 'alpha': alpha_l, 'sig': sig_l, 'tau': tau_l,
                       'id': seed_l, 'iter': iter_l, 'nclust': nclust_l,
                       'test_ll': testll_l,'merge_eps':merge_eps_l})
    print(df.shape)
    #df2 = pd.DataFrame(weights_l)
    #print(weights_l.shape)
    #print(df2.shape)
    #dff = pd.merge(df,df2,left_on='lkey',right_on='rkey',how='outer')
    #dff = pd.concat([df,df2],axis=1)
    #print(dff.shape)
    with open(outfile, 'w') as f:
        df.to_csv(f)

    return df
    

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Process results into form suitable for parsing.")
    parser.add_argument('respath', type=str,
                        help="path to results directory")
    parser.add_argument('outfile', type=str,
                        help="path to file to store dataframe")
    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)

    df = process_results(**args)
