from __future__ import division

import numpy as np
import cPickle as pkl

from util import top_topic_words


def print_words(resfile, K=10, W=5):
    """ Print the top W words from the K most prevalent topics.
    """
    with open(resfile, 'r') as f:
        d = pkl.load(f)
    nrm_mm = d['nrm_mm']
    if nrm_mm is None:
        raise RuntimeError("No results in %s" % resfile)
    vfile = d['vfile']
    with open(vfile, 'r') as f:
        vocab = [w.strip() for w in f.readlines()]

    topics = nrm_mm.nu
    inds = np.argsort(nrm_mm.rho)[::-1]  # Reverse to descending order

    inds = inds[:K]
    topics = topics[:,inds]

    t = top_topic_words(topics, vocab, W)

    return t


if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description="Print the top W words of the K mos prevalent topics.")
    parser.add_argument('resfile', type=str,
                        help="path to results file to print topics")
    parser.add_argument('K', type=int, default=10,
                        help="number of topics to print words for")
    parser.add_argument('W', type=int, default=5,
                        help="number of words to print from each topic")


    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)
    W = args['W']

    topics = print_words(**args)

    for t, top in enumerate(topics):
        print "Topic: %d" % (t+1,)
        for w in xrange(W):
            print "\t%s" % top[w]
