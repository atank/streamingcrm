from __future__ import division
import numpy as np 
import scipy as sp
import mpmath
import sys, os
import pickle as pkl
from scipy.sparse import csc_matrix
from scipy.misc import factorial, comb




def generate_topics(k,V,alpha):
    nu = np.zeros((k,V))
    alpha = np.ones(V)*alpha
    for i in xrange(k):
        nu[i,:] = np.random.dirichlet(alpha)
    return(nu)

def generate_documents(assignments,cluster_counts,k,V,nu,n,mwords,noisewords):
    X = np.zeros((V,n))
    unif_prob = np.zeros(V)
    unif_prob += 1
    unif_prob /= np.sum(unif_prob)
    for i in xrange(n):
        clid = assignments[i]
        X[:,i] = np.random.multinomial(mwords,nu[clid,:])
        X[:,i] += np.random.multinomial(noisewords,unif_prob)
    return(X)


def generate_pitman_yor(a,sigma,n):
    cluster_counts = np.zeros(n)
    num_clusters = np.zeros(n)
    k = 0
    cluster_counts[0] = 1
    assignments = np.zeros(n)
    for i in xrange(1,n):
        print(i)
        print(k)
        #oldp = (cluster_counts[:k] - sigma)/(i + a)
        newp = (a + k*sigma)/(i + a)
        prob = np.zeros(k + 1)
        prob[:k] = (cluster_counts[:k] - sigma)/(i + a)
        prob[k] = newp
        new = np.random.multinomial(1,prob)
        whichassigned = np.where(new == 1)[0]
        #print(whichassigned)
        cluster_counts[whichassigned] += 1
        if whichassigned == k:
            k = k + 1
        num_clusters[i] = k
        assignments[i] = whichassigned
    return(cluster_counts,k,assignments,num_clusters)

if __name__ == "__main__":
    seed = 2312
    np.random.seed(seed)
    alpha = .9
    V = 200
    sigma = .75
    a = 1
    n = 10000
    noisewords = 0
    mwords = 50
    cluster_counts,k,assignments,num_clusters = generate_pitman_yor(a,sigma,n)
    nu = generate_topics(k,V,alpha)
    
    X = generate_documents(assignments,cluster_counts,k,V,nu,n,mwords,noisewords)
    Xsmall = X[:,:int(n/10)]
    X = csc_matrix(X)
    Xsmall = csc_matrix(Xsmall)
    print(X.todense())


    #outfile = "pityordocs_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + ".npy"
    #np.save(outfile,[X, assignments])

    #store as a pickle file

    outfile = "pityornclust_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + ".pkl"
    pkl.dump(num_clusters,open(outfile,'wb'))

    outfile = "pityordocs_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + ".pkl"
    pkl.dump(X,open(outfile,'wb'))

    outfile = "pityordocsSMALL_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + ".pkl"
    pkl.dump(Xsmall,open(outfile,'wb'))

        