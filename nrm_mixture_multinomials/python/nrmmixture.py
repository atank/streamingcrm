from __future__ import division

import types

import numpy as np
import scipy.optimize as spopt



from scipy.sparse import csc_matrix

from util import make_discrete_bars
from nrm_fast import logmpoyla
#from util import logmpoyla
from nrm_fast import pred_ll as pred_ll_fast
import matplotlib.pyplot as plt


eps = np.finfo('float64').eps
import scipy.spatial.distance as spdist


# Distribution of U auxiliary variable for GGP
def Udist(u, n, tau, a, sig, nclust):
    if u > 0.:
        return (-(n-1)*np.log(u + eps) + (n-a*nclust)*np.log(u+tau+eps)
                + a/(sig+eps)*(u+tau)**sig)
    else:
        return np.finfo('float64').max


class GGPMixtureMultinomialStream(object):

    def __init__(self, x, a=None, tau=None, sig=None, alpha=None,
                 newclust_eps=None, prune_eps=None,merge_eps=None,merge=True,merge_interval=None,merge_metric="l1_assignments"):
        """
            x : D-dimensional ndarray, first observation, should have shape
                (D,).
            a : float > 0, NGGP "shape" parameter.
            tau : float > 0, NGGP "rate" parameter.
            sig : float > 0, NGG "discount" factor.
            alpha : float > 0, Symmetric Dirichlet prior on topics.
            newclust_eps : float > 0, Threshold to prune new clusters.
            topic_eps : float > 0, Remove topics with cumulative probability
                                     < this.
        """

        self.last_merge = 1

        #self.U_prev = 1.
        self.n = 1

        # Set up cluster data structures
        self.nclust = 1


        self.tau = tau
        self.a = a
        self.sig = sig
        self.alpha = alpha
        self.newclust_eps = newclust_eps
        self.prune_eps = prune_eps
        self.merge_eps = merge_eps
        self.merge_interval=merge_interval

        self.it = 1

        # Initialize first observation
        self.V = x.shape[0]  # Number of words in vocab
        self.rho = np.ones(1)
        self.rho_sum = np.ones(1)

        # Variational parameters for topics
        #self.nu = alpha*np.ones((1,V))
        self.nu = self.meanfield_params(x[:,np.newaxis], alpha, np.array([1.]))

        # Helper variables
        self.alpha_onesV = alpha*np.ones(self.V, order='F')
        self.U_prev = 1

        self.sim = np.zeros((self.nclust,self.nclust))
        self.merge = merge
        self.merge_metric = merge_metric
        self.sim_counts = np.zeros((self.nclust,self.nclust))


    def update_U(self):

        if (self.sig == 0):
            return 1.
        else:
            U_prev = self.U_prev
            n = self.n
            a = self.a
            tau = self.tau
            sig = self.sig
            nclust = self.nclust

            arg_tup = (n, tau, a, sig, nclust)
            res = spopt.minimize_scalar(Udist, bounds=(.01, 1e9),
                                        method='bounded', args=arg_tup)
            U = res.x
            self.U_prev = U
            self.U = U

            return U

    def update(self, x):
        """
            x : D-dimensional ndarray, should have shape (D,).
        """
        U = self.update_U()

        # Thresholding done here too
        self.rho = self.compute_soft_assigns(x[:,np.newaxis], U)

        #TODO: Maybe re-enable pruning
        # Prune "unused" topics
        #w = self.rho_sum / np.sum(self.rho_sum)
        #idx = w > self.rho_thresh
        #self.rho = self.rho[idx]
        #self.rho_sum = self.rho_sum[idx]
        #self.nu = self.nu[:,idx]
        #self.nclust = self.rho.shape[0]

        #TODO: Implement this
        # Merge redundant topics







        # Mean field step for params
        self.nu = self.meanfield_params(x[:,np.newaxis], self.nu, self.rho)
        self.n += 1

        #print(self.rho_sum.shape)
        #print(self.nclust)

        if self.merge:
            if self.n % self.merge_interval == 2:
                if self.merge_metric == "l1_assignments":
                    self.merge_clusters_l1_assignments()
                if self.merge_metric == "cosine":
                    self.merge_clusters_cosine()
        # Store new values of params in object


    def merge_clusters_cosine(self):
        nu_normed = self.nu/np.sum(self.nu,0)
        norm_sim = 1 - np.dot(nu_normed.T,nu_normed)
        norm_sim= np.tril(norm_sim,k=-1)
        norm_sim[np.where(norm_sim==0)] = 100
        to_merge = np.where(norm_sim < self.merge_eps)
        to_merge_dist = norm_sim[to_merge]
        to_merge_idx = np.argsort(to_merge_dist)
        #print(norm_sim)

        print to_merge
        c1merge = np.array(to_merge[0][to_merge_idx])
        c2merge = np.array(to_merge[1][to_merge_idx]) 
        merged = []

        for i in xrange(to_merge[0].shape[0]):
            c1 = c1merge[i]
            c2 = c2merge[i]
            if self.sim_counts[c1,c2] < self.merge_interval:
                continue
            if c1 in merged or c2 in merged:
                #print('already merged')
                continue

            self.rho_sum[c1] += self.rho_sum[c2]
            self.nu[:,c1] += self.nu[:,c2]
            idx = np.setdiff1d(range(self.nclust),c2)
            self.nu = np.asfortranarray(self.nu[:,idx])
            self.rho_sum = self.rho_sum[idx]
            self.nclust -= 1
            #print('merged')

            c1merge[np.where(c1merge > c2)[0]] = c1merge[np.where(c1merge > c2)[0]] - 1
            c2merge[np.where(c2merge > c2)[0]] =  c2merge[np.where(c2merge > c2)[0]] - 1
            merged.append(c1merge[i])
            merged.append(c2merge[i])


    def merge_clusters_l1_assignments(self):
        norm_sim = self.sim / self.sim_counts
        #print(norm_sim)
        norm_sim= np.tril(norm_sim,k=-1)
        norm_sim[np.where(norm_sim==0)] = 100
        to_merge = np.where(norm_sim < self.merge_eps)
        to_merge_dist = norm_sim[to_merge]
        to_merge_idx = np.argsort(to_merge_dist)


        print to_merge
        c1merge = np.array(to_merge[0][to_merge_idx])
        c2merge = np.array(to_merge[1][to_merge_idx])

        merged = []

        for i in xrange(to_merge[0].shape[0]):
            c1 = c1merge[i]
            c2 = c2merge[i]
            #print(sort())
            if self.sim_counts[c1,c2] < self.merge_interval:
                #print('too soon to merge')
                continue
            if c1 in merged or c2 in merged:
                #print('already merged')
                continue

            #merge components
            #print self.sim_counts.shape
            #print c1
            #print c2
            #print self.sim.shape
            total_counts = self.sim_counts[c1,:] + self.sim_counts[c2,:]
            self.sim[c1,:] = (self.sim_counts[c1,:]/total_counts)*(self.sim[c1,:]) + (self.sim_counts[c2,:]/total_counts)*(self.sim[c2,:])
            self.sim[:,c1] = self.sim[c1,:]
            self.rho_sum[c1] += self.rho_sum[c2]
            self.nu[:,c1] += self.nu[:,c2]


            ###remove component 
            idx = np.setdiff1d(range(self.nclust),c2)
            #print(idx)
            #print(self.nclust)
            self.nu = np.asfortranarray(self.nu[:,idx])
            #import ipdb; ipdb.set_trace()
            self.rho_sum = self.rho_sum[idx]

            self.sim = self.sim[:,idx]
            self.sim = self.sim[idx,:]
            #print(self.sim.shape)
            self.sim_counts = self.sim_counts[:,idx]
            self.sim_counts = self.sim_counts[idx,:]
            self.nclust -= 1
            #print('merged')
            #print c1merge
            #print c1merge[np.where(c1merge > c2)[0]]
            #print np.where(c1merge > c2)[0]

            c1merge[np.where(c1merge > c2)[0]] = c1merge[np.where(c1merge > c2)[0]] - 1
            c2merge[np.where(c2merge > c2)[0]] =  c2merge[np.where(c2merge > c2)[0]] - 1
            merged.append(c1merge[i])
            merged.append(c2merge[i])
    #def prune_clusters():


    # def prune_relative(self):

    # def prune(self):
    #     weights_norm = self.weights_sum/np.sum(self.weights_sum)
    #     np.where(weights_norm < self.prune_eps)


    def prior_assign(self, U):
        rho_sum = self.rho_sum
        sig = self.sig
        tau = self.tau
        a = self.a
        n = self.n  # This is actually correct
        nclust = self.nclust
        Eclust = self.nclust
        prior_assign = np.zeros(nclust+1)
        
        denom = (a*(U+tau)**sig + n - sig*Eclust)
        prior_assign[:nclust] = (rho_sum - sig) / denom 
        prior_assign[-1] = (a*(U+tau)**sig) / denom

        # Hack: set negative entries (due to subtracting sigma) to zero
        #np.maximum(prior_assign, 0., out=prior_assign)

        prior_assign /= np.sum(prior_assign)

        return prior_assign

    def compute_soft_assigns(self, x, U):
        # Compute prior assignments
        prior_assign = self.prior_assign(U)
        xf = np.asfortranarray(x)

        # Log-likehood of data given variational params of each existing cluster
        alpha_onesV = self.alpha_onesV[:,np.newaxis]
        V = self.V
        nu = self.nu
        alpha = self.alpha

        #print(prior_assign[:-1])

        lrho_old = np.log(prior_assign[:-1] + eps) + logmpoyla(xf, nu)
        lrho_new = np.log(prior_assign[-1] + eps) \
                   + logmpoyla(xf, np.asfortranarray(alpha_onesV))
        #lrho_old = logmpoyla(x, nu)
        #lrho_new = logmpoyla(x, alpha_onesV)
        #print(lrho_old)
        #print(lrho_new)

        #rho_old_un = np.exp(lrho_old)
        #rho_new_un = np.exp(lrho_new)
        #rho_un = np.empty(rho_old_un.shape[0]+1)
        #rho_un[:-1] = rho_old_un
        #rho_un[-1] = rho_new_un
        #rho_un = rho_un/sum(rho_un)
        #print(rho_un)

        #lrho_old = np.log(prior_assign[:-1]) + logmpoyla(x, nu)
        #lrho_new = np.log(prior_assign[-1]) + logmpoyla(x, alpha_onesV)

        maxlr = np.maximum(np.max(lrho_old), lrho_new)
        rho_old = np.exp(lrho_old - maxlr)
       
        rho_new = np.exp(lrho_new - maxlr)

        rho_denom = np.sum(rho_old) + rho_new
        rho_new /= rho_denom

        # Threshold new cluster and only add it if it's likely to be used.
        if rho_new < self.newclust_eps:
            rho = rho_old
            rho /= np.sum(rho_old)
            self.rho_sum += rho



        else:
            rho = np.empty(rho_old.shape[0]+1)
            rho[:-1] = rho_old / rho_denom
            rho[-1] = rho_new
            self.nclust += 1
            old_nu = self.nu
            K = self.nu.shape[1]
            self.nu = np.empty((V, K+1), order='F')
            self.nu[:,:K] = old_nu
            self.nu[:,-1] = self.alpha_onesV
            self.rho_sum = np.append(self.rho_sum, np.zeros(1))
            self.rho_sum += rho 

            #add row/colmn to similarity matrix
            if self.merge:
                #print self.sim.shape 
                if self.merge_metric == "l1_assignments":
                    gv = np.zeros((self.sim.shape[0]))
                    self.sim = np.hstack((self.sim,gv[:,np.newaxis]))
                    gh = np.zeros((self.sim.shape[1]))
                    self.sim = np.vstack((self.sim,gh[np.newaxis,:]))

                #print self.sim_counts.shape
                gv = np.zeros((self.sim_counts.shape[0]))
                self.sim_counts = np.hstack((self.sim_counts,gv[:,np.newaxis]))
                gh = np.zeros((self.sim_counts.shape[1]))
                self.sim_counts = np.vstack((self.sim_counts,gh[np.newaxis,:]))

        if self.merge:
            if self.merge_metric == "l1_assignments":
                self.sim += spdist.squareform(spdist.pdist(rho[:,np.newaxis]))
            self.sim_counts += 1

        return rho

    def meanfield_params(self, x, alpha, weights):
        # This does x * weights broadcasting weights down the rows.
        # Specifically, x * np.tile(weights, (x.shape[0], 1)).
        return alpha + np.einsum('ij,j->ij', x, weights, order='F')
    
    def pred_ll(self, Xtest):
        """
            Xtest : scipy.sparse matrix with test documents in the columns.
        """
        W, D = Xtest.shape
        K = self.nu.shape[1]
        U = self.update_U()
        prior_assign = self.prior_assign(U)
        # Construct this way to retain Fortran ordering
        nu_pred = np.empty((W, K+1), order='F')
        nu_pred[:,:K] = self.nu
        nu_pred[:,-1] = self.alpha_onesV

        ll = 0.

        #TODO: Optimize this loop in cython
        lp_z = np.log(prior_assign + eps)
        for d in xrange(D):

            x = np.squeeze(Xtest[:,d].toarray())

            #print(Xtrain)
            lp = lp_z + logmpoyla(x[:,np.newaxis], nu_pred)
            ll += np.logaddexp.reduce(lp)

        return ll

    def pred_ll_nonew(self, Xtest):
        """
            Xtest : scipy.sparse matrix with test documents in the columns.
        """
        W, D = Xtest.shape
        K = self.nu.shape[1]
        U = self.update_U()
        prior_assign = self.prior_assign(U)
        prior_assign = prior_assign[:-1]
        prior_assign /= np.sum(prior_assign)
        # Construct this way to retain Fortran ordering
        nu_pred = np.empty((W, K), order='F')
        nu_pred[:,:K] = self.nu

        ll = 0.

        lp_z = np.log(prior_assign + eps)
        for d in xrange(D):

            x = np.squeeze(Xtest[:,d].toarray())

            #print(Xtrain)
            lp = lp_z + logmpoyla(x[:,np.newaxis], nu_pred)
            ll += np.logaddexp.reduce(lp)

        return ll

    def num_clusters(self):
        return self.nu.shape[1]
    
    def name(self):
        oname = "a-%.2f-tau-%.2f-sig-%.2f-alpha-%.2f-neps-%.2f-meps-%.2f-minterval-%.2f" % \
            (self.a, self.tau, self.sig, self.alpha, self.newclust_eps,self.merge_eps,self.merge_interval)
        oname = oname.replace(".", "_")
        return oname

# Add pred_ll_fast to class so can call normally
GGPMixtureMultinomialStream.pred_ll_fast = types.MethodType(pred_ll_fast,
                                               None, GGPMixtureMultinomialStream)
        

if __name__ == "__main__":

    np.random.seed(867539)
    #np.random.seed(3)

    Ntrain = 500  # size of synthetic data
    Ntest = 5  #200
    alpha_v = .1  # dirichlet prior on topic proportions
    alpha_w =  1  # dirichlet prior on mixture weights
    Nclust = 20  # number of actual clusters
    V = 5  # 200  # vocabulary size 
    obsperdat = 500  # number words per documents

    q = make_discrete_bars(Ntest+Ntrain, alpha_v, Nclust, V, alpha_w, obsperdat)
    X = q[0]
    topic_prop = q[1]
    labels = q[2]
    weights = q[3]

    # Make data sparse
    X = csc_matrix(X)
    Xtrain = X[:,:Ntrain]
    Xtest = X[:,Ntrain:Ntrain + Ntest]
    g = np.squeeze(X[:,0].toarray())
    #print(g.shape)
    streaminv = GGPMixtureMultinomialStream(x=g, a=1, tau=1, sig=0, alpha=0.1,
                          newclust_eps=0.5 ,prune_eps=0,merge_eps=.001,merge_interval=100,merge=True)
    #streamd = GGPMixtureMultinomialStream(x=g, a=1, tau=1, sig=0, alpha=0.1,
    #                      newclust_eps=0.5 ,topic_eps=.1)
    #print(stream.nu)
    test_interval = 50
    lik_testd = []
    lik_testinv = []
    for i in xrange(1,Ntrain):
        print i
        #if (i % test_interval == 2):
        #    lik_testd.append(streamd.pred_ll_fast(Xtest))

        #print "pred_ll: %g\n" % streaminv.pred_ll(Xtest)
        #print "pred_ll_fast: %g\n" % streaminv.pred_ll_fast(Xtest)
        #print "pred_ll_nonew: %g\n" % streaminv.pred_ll_nonew(Xtest)
        #print "pred_ll_fast_nonew: %g\n" % streaminv.pred_ll_fast(Xtest,nonew=True)
        #import ipdb; ipdb.set_trace()

        #lik_testinv.append(streaminv.pred_ll_fast(Xtest))

        #print(stream.nclust)
        g = np.squeeze(Xtrain[:,i].toarray())
        #print(g)
        #print(labels[i])
        #streamd.update(g)
        streaminv.update(g)
        #print(stream.nu)
        #print(stream.rho)
        
    likd = np.array(lik_testd)
    likinv = np.array(lik_testd)
    #plt.plot(likd, 'r', likinv, 'b')
   # plt.plot(lik_testinv)
   # plt.show()
    #plt.show()
	#print(likinv)

    print(weights)
    print(likinv)
    print(streaminv.rho_sum/sum(streaminv.rho_sum))
    #print(streamd.rho_sum/N)
    #print(weights)
    #print(streaminv.rho)
