from __future__ import division

import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from mpltools import style

from itertools import islice


def make_iter_plots(df_file, outdir, testit=50):
    """ Make plots that depend on iteration for each parameter setting in
        results data frames.
        
        df_file : path to csv file containing data frame
        outdir : path to directory to store figures (and topics)"
        K : number of topics to write out
        W : number of words in each topic to write out
        testit : how many iterations between test evaluations
    """

    style.use('ggplot')

    with open(df_file, 'r') as f:
        df = pd.read_csv(df_file)

    def q25(x):
        return np.percentile(x, 25)
    def q75(x):
        return np.percentile(x, 75)

    funs = [np.mean, np.median, np.std, q25, q75]
    groups = df.groupby(['a', 'alpha', 'iter', 'sig', 'tau'])
    gstats = groups.agg({'nclust': funs, 'test_ll': funs})
    gstats = gstats.reset_index()

    # Group again to get statistics per iter.  Don't group over sig here
    # because we want to have all sig values for each group of the other params
    groups = gstats.groupby(['a', 'alpha', 'tau'])

    # Make plots
    ylabs = {'test_ll': 'Predictive log-likelihood',
             'nclust': 'Number of clusters'}

    #for par, gr in islice(groups, 0, 1):
    for par, gr in groups:

        sigs = gr['sig'].unique()
        nsigs = len(sigs)

        # Note, since test_ll is negative the scaling is reversed
        ylims = {'test_ll': {'ymin': 1.005*gr['test_ll'][['mean', 'median']].values.min(),
                             'ymax': 0.995*gr['test_ll'][['mean', 'median']].values.max()},
                 'nclust': {'ymin': 0.9*gr['nclust'][['mean', 'median']].values.min(),
                            'ymax': 1.1*gr['nclust'][['mean', 'median']].values.max()}}

        for ev in ['test_ll', 'nclust']:

            f = plt.figure()
            #f.set_size_inches(8, 4)  # Try to set explicitly for paper
            for i, sig in enumerate(sigs):
 
                ax = plt.subplot(1, nsigs, i+1)
                dat = gr[gr['sig'] == sig]

                it = dat['iter']*testit + testit
                mean = dat[ev]['mean']
                med = dat[ev]['median']
                q25 = dat[ev]['q25']
                q75 = dat[ev]['q75']
                std = dat[ev]['std']

                plt.errorbar(it, med, yerr=None, fmt="-o", linewidth=1.5,
                        markersize=8, markeredgecolor="None")
                plt.hold(True)
                plt.errorbar(it, mean, yerr=None, fmt="-o", linewidth=1.5,
                        markersize=8, markeredgecolor="None")
                if i == 0:
                    plt.legend(['median', 'mean'], loc='lower right')
                    plt.ylabel(ylabs[ev])
                else:
                    #plt.gca().get_yaxis().set_ticklabels([])
                    ax.get_yaxis().set_ticklabels([])

                plt.xlabel('Iteration')
                plt.ylim(ylims[ev]['ymin'], ylims[ev]['ymax'])

                plt.rc('text', usetex=True)
                plt.title(r'$\sigma: %.2f$' % sig)

            a, alpha, tau = par

            # Save figure
            fn = "%s-a-%.2f-alpha-%.2f-tau-%.2f" % (ev, a, alpha, tau)
            fn = fn.replace(".", "_")
            fn += ".pdf"
            plt.savefig(os.path.join(outdir, fn), pad_inches=0)
            plt.close('all')

    return gstats, groups


def make_topic_plots(df_file, outdir, K=10, W=5):
    """ Make plots and other summaries of learned topics.
    """
    pass



if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Create plots from nrm topic clustering.")
    parser.add_argument('df_file', type=str,
                        help="path to file containing data frame")
    parser.add_argument('outdir', type=str,
                        help="path to directory to store figures (and topics)")

    args = parser.parse_args()

    # Convert from Namespace to dictionary
    args = vars(args)

    gstats, groups = make_iter_plots(**args)
