import numpy as np
import scipy as sp
import pickle as pkl
from scipy.sparse import csc_matrix


smallsize = .1
kos_path = "/homes/alextank/streamingcrm/data/uci_bag_of_words/kos/"
kos_file = "docword.kos.pkl"
filen = open(kos_path + kos_file)
X = pkl.load(filen)
W,D = X.shape
seed = 10000

include = np.random.choice(range(D),size = int(D*.1),replace=False)

Xsmall = X[:,include]
Xsmall = csc_matrix(Xsmall)

filename = kos_path + "docword.kos_small.pkl"
pkl.dump(Xsmall,open(filename,'wb'))
