from __future__ import division

import numpy as np
import scipy.optimize as spopt

from scipy.sparse import csc_matrix

from util import logmpoyla, make_discrete_bars
import matplotlib.pyplot as plt


eps = np.finfo('float64').eps


# Distribution of U auxiliary variable for GGP
def Udist(u, n, tau, a, sig, nclust):
    if u > 0.:
        return (-(n-1)*np.log(u + eps) + (n-a*nclust)*np.log(u+tau+eps)
                + a/(sig+eps)*(u+tau)**sig)
    else:
        return np.finfo('float64').max


class GGPMixtureMultinomialStream_lag(object):

    def __init__(self, x, a=None, tau=None, sig=None, alpha=None,
                 newclust_eps=None, topic_eps=None, m_lag=None,n_smooth_it=None,
                 fixed_lag=None,lag_prune=None):
        """
            x : D-dimensional ndarray, first observation, should have shape
                (D,).
            a : float > 0, NGGP "shape" parameter.
            tau : float > 0, NGGP "rate" parameter.
            sig : float > 0, NGG "discount" factor.
            alpha : float > 0, Symmetric Dirichlet prior on topics.
            newclust_eps : float > 0, Threshold to prune new clusters.
            topic_eps : float > 0, Remove topics with cumulative probability
                                     < this.
        """

        #self.U_prev = 1.
        self.n = 1

        # Set up cluster data structures
        self.nclust = 1

        self.tau = tau
        self.a = a
        self.sig = sig
        self.alpha = alpha
        self.newclust_eps = newclust_eps
        self.rho_thresh = topic_eps

        self.it = 1







        # Initialize first observation
        self.V = x.shape[0]  # Number of words in vocab
        self.rho = np.ones(1)
        self.rho_sum = np.ones(1)

        # fixed lag variables
        self.fixed_lag = fixed_lag
        self.n_smooth_it = n_smooth_it
        self.m_lag = m_lag
        self.x_window = []
        self.rho_window = []
        self.lag_prune = lag_prune

        # Variational parameters for topics
        #self.nu = alpha*np.ones((1,V))
        self.nu = self.meanfield_params(x[:,np.newaxis], alpha, np.array([1.]))
        self.x_window.append(x)
        self.rho_window.append(np.array([1]))

        # Helper variables
        self.alpha_onesV = alpha*np.ones(self.V, order='F')
        self.U_prev = 1

    def fixed_lag_prune(self):

        idx = np.where(self.rho_sum > self.lag_prune)[0]
        #print to_prune
        print(idx.shape)
        #print(self.rho_sum.shape)
        #print(self.nclust)
        #raw_input('Press <ENTER> to continue')
        #if idx.shape < self.nclust: 
        self.rho_sum = self.rho_sum[idx]
        self.nu = self.nu[:,idx]
        self.nclust = self.rho_sum.shape[0]

        for i in xrange(self.m_lag):
            ib = np.where(idx < np.shape(self.rho_window[i]))
            self.rho_window[i] = self.rho_window[i][idx[ib]]



    def update_U(self):

        if (self.sig == 0):
            return 1
        else:
            U_prev = self.U_prev
            n = self.n
            a = self.a
            tau = self.tau
            sig = self.sig
            nclust = self.nclust

            arg_tup = (n, tau, a, sig, nclust)
            res = spopt.minimize_scalar(Udist, bounds=(.01, 1e9),
                                        method='bounded', args=arg_tup)
            U = res.x
            self.U_prev = U
            self.U = U

            return U

    def update(self, x):
        """
            x : D-dimensional ndarray, should have shape (D,).
        """

        #for new data point do:
        U = self.update_U()

        # Thresholding done here too
        self.rho = self.compute_soft_assigns(x[:,np.newaxis], U)

        # Prune "unused" topics
        #w = self.rho_sum / np.sum(self.rho_sum)
        #idx = w > self.rho_thresh
        #self.rho = self.rho[idx]
        #self.rho_sum = self.rho_sum[idx]
        #self.nu = self.nu[:,idx]
        #self.nclust = self.rho.shape[0]

        # Merge redundant topics
        

        # Mean field step for params
        self.nu = self.meanfield_params(x[:,np.newaxis], self.nu, self.rho)

        # Store new values of params in object
        self.n += 1

        print(self.nu.shape)
        #fixed lag smoothing step
        #first add most recent data point to window list
        if self.fixed_lag:
            #first delete point out of the window
            if self.n > self.m_lag:
                self.x_window.pop(0)
                self.rho_window.pop(0)

            #add new point to the window
            self.x_window.append(x)
            self.rho_window.append(self.rho)

            #print(self.rho_sum)
            #print('# of clust:')
            #print self.nclust

            Nlag = self.m_lag
            if self.n > self.m_lag:
                for j in xrange(self.n_smooth_it):
                    for i in xrange(Nlag):
                        #first delete sufficient statistics from this data point
                        #print(i)
                        #print len(self.rho_window)
                        p_shape = self.rho_window[i].shape
                        self.rho_sum[:self.rho_window[i].shape[0]] -= self.rho_window[i]
                        q = np.zeros(self.nclust)
                        q[:self.rho_window[i].shape[0]] = self.rho_window[i]
                        #print i 
                        #print(self.rho_sum)

                        #print q
                        #raw_input('Press <ENTER> to continue')
                        self.nu = self.meanfield_params(self.x_window[i][:,np.newaxis], self.nu, -q)
                        #print self.x_window[i].shape
                        
                        #perform inference again for this data point
                        U = self.update_U()
                        self.rho_window[i] = self.compute_soft_assigns(self.x_window[i][:,np.newaxis], U)
                        #print self.rho_window[i].shape
                        self.nu = self.meanfield_params(self.x_window[i][:,np.newaxis], self.nu, self.rho_window[i])
                        #print self.nu.shape
                    self.fixed_lag_prune()
        print('# of clust:')
        print self.nclust






    def prior_assign(self, U):
        rho_sum = self.rho_sum
        sig = self.sig
        tau = self.tau
        a = self.a
        n = self.n  # This is actually correct
        nclust = self.nclust
        Eclust = self.nclust
        prior_assign = np.zeros(nclust+1)
        
        denom = (a*(U+tau)**sig + n - sig*Eclust)
        prior_assign[:nclust] = (rho_sum - sig) / denom 
        prior_assign[-1] = (a*(U+tau)**sig) / denom

        # Hack: set negative entries (due to subtracting sigma) to zero
        #np.maximum(prior_assign, 0., out=prior_assign)

        prior_assign /= np.sum(prior_assign)

        return prior_assign

    def compute_soft_assigns(self, x, U):
        # Compute prior assignments
        prior_assign = self.prior_assign(U)

        # Log-likehood of data given variational params of each existing cluster
        alpha_onesV = self.alpha_onesV[:,np.newaxis]
        V = self.V
        nu = self.nu
        alpha = self.alpha

        lrho_old = np.log(prior_assign[:-1] + eps) + logmpoyla(x, nu)
        lrho_new = np.log(prior_assign[-1] +eps) \
                   + logmpoyla(x, alpha_onesV)
        #lrho_old = logmpoyla(x, nu)
        #lrho_new = logmpoyla(x, alpha_onesV)
        #print(lrho_old)
        #print(lrho_new)

        #rho_old_un = np.exp(lrho_old)
        #rho_new_un = np.exp(lrho_new)
        #rho_un = np.empty(rho_old_un.shape[0]+1)
        #rho_un[:-1] = rho_old_un
        #rho_un[-1] = rho_new_un
        #rho_un = rho_un/sum(rho_un)
        #print(rho_un)

        #lrho_old = np.log(prior_assign[:-1]) + logmpoyla(x, nu)
        #lrho_new = np.log(prior_assign[-1]) + logmpoyla(x, alpha_onesV)

        maxlr = np.maximum(np.max(lrho_old), lrho_new)
        rho_old = np.exp(lrho_old - maxlr)
       
        rho_new = np.exp(lrho_new - maxlr)

        rho_denom = np.sum(rho_old) + rho_new
        rho_new /= rho_denom

        # Threshold new cluster and only add it if it's likely to be used.
        if rho_new < self.newclust_eps:
            rho = rho_old
            rho /= np.sum(rho_old)
            self.rho_sum += rho
        else:
            rho = np.empty(rho_old.shape[0]+1)
            rho[:-1] = rho_old / rho_denom
            rho[-1] = rho_new
            self.nclust += 1
            self.nu = np.hstack((self.nu, alpha_onesV))
            self.rho_sum = np.append(self.rho_sum, np.zeros(1))
            self.rho_sum += rho 

        return rho

    def meanfield_params(self, x, alpha, weights):
        # This does x * weights broadcasting weights down the rows.
        # Specifically, x * np.tile(weights, (x.shape[0], 1)).
        return alpha + np.einsum('ij,j->ij', x, weights, order='F')
    
    def pred_ll(self, Xtest):
        """
            Xtest : scipy.sparse matrix with test documents in the columns.
        """
        U = self.update_U()
        prior_assign = self.prior_assign(U)
        nu_pred = self.nu.copy()
        #print(nu_pred.shape)
        #print(self.alpha_onesV.shape)
        nu_pred = np.hstack((nu_pred, self.alpha_onesV[:, np.newaxis]))

        ll = 0.

        W, D = Xtest.shape
        lp_z = np.log(prior_assign + eps)
        for d in xrange(D):

            x = np.squeeze(Xtest[:,d].toarray())

            #print(Xtrain)
            lp = lp_z + logmpoyla(x[:,np.newaxis], nu_pred)
            #print(lp)
            ll += np.logaddexp.reduce(lp)

        return ll

    def num_clusters(self):
        return self.nu.shape[1]
    
    def name(self):
        oname = "a-%.2f-tau-%.2f-sig-%.2f-alpha-%.2f-neps-%.2f" % \
            (self.a, self.tau, self.sig, self.alpha, self.newclust_eps)
        oname = oname.replace(".", "_")
        return oname
        

if __name__ == "__main__":

    Ntrain = 500  # size of synthetic data
    Ntest = 200
    alpha_v = .5  # dirichlet prior on topic proportions
    alpha_w =  100  # dirichlet prior on mixture weights
    Nclust = 20  # number of actual clusters
    V = 30  # vocabulary size 
    obsperdat = 20  # number words per documents
    m_lag = 5
    n_smooth_it = 1
    fixed_lag = 0
    lag_prune = .1

    q = make_discrete_bars(Ntest+Ntrain, alpha_v, Nclust, V, alpha_w, obsperdat)
    X = q[0]
    topic_prop = q[1]
    labels = q[2]
    weights = q[3]

    # Make data sparse
    X = csc_matrix(X)
    Xtrain = X[:,:Ntrain]
    Xtest = X[:,Ntrain:Ntrain + Ntest]
    g = np.squeeze(X[:,0].toarray())
    #print(g.shape)
    stream_nlag = GGPMixtureMultinomialStream_lag(x=g, a=1, tau=1, sig=0, alpha=0.1,
                          newclust_eps=0.95 ,topic_eps=.1,fixed_lag=0,n_smooth_it=1,m_lag=499)
    stream_lag = GGPMixtureMultinomialStream_lag(x=g, a=1, tau=1, sig=0, alpha=0.1,
                          newclust_eps=0.95 ,topic_eps=.1,fixed_lag=1,n_smooth_it=1,m_lag=10,
                          lag_prune=.1)
    #print(stream.nu)
    test_interval = 50
    lik_test_lag = []
    lik_test_nlag = []
    for i in xrange(1,Ntrain):
        if (i % test_interval == 2):
            lik_test_lag.append(stream_lag.pred_ll(Xtest))
            lik_test_nlag.append(stream_nlag.pred_ll(Xtest))

        #print(stream.nclust)
        g = np.squeeze(Xtrain[:,i].toarray())
        #print(g)
        #print(labels[i])
        stream_lag.update(g)
        stream_nlag.update(g)
        #print(stream.nu)
        #print(stream.rho)
        
    lik_lag = np.array(lik_test_lag)
    lik_nlag = np.array(lik_test_nlag)
    plt.plot(lik_lag,'r',lik_nlag,'b')
    plt.show()

    ##COMPUTE LIKELIHOOD UNDER TRUE PARAMETERS< SEE IF THAT IS A "CIELING"

    #print(weights)
    #print(streamd.rho_sum/sum(streamd.rho_sum))
    #print(streamd.rho_sum/N)
    #print(weights)
    #print(stream.rho)