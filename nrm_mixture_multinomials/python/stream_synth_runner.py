from __future__ import division
import numpy as np 
import scipy as sp
import mpmath
import sys, os
import pickle as pkl
from scipy.sparse import csc_matrix
from scipy.misc import factorial, comb
from nrm_fast import pred_ll as pred_ll_fast
from nrmmixture import GGPMixtureMultinomialStream
import matplotlib
import matplotlib.pyplot as plt

seed = 2312
np.random.seed(seed)
alpha = .9
V = 200
sigma = .75
a = 1
n = 10000
noisewords = 0
mwords = 50
test_interval = 200


outfile = "pityordocs_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) +".pkl"
filen = open(outfile)
X = pkl.load(filen)
n=X.shape[1]


outfile = "pityornclust_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords) + ".pkl"
filen = open(outfile)
num_clusters_true = pkl.load(filen)



ntrain = int(n*.8)  
print(ntrain)

Xtrain = X[:,:ntrain]
Xtest = X[:,ntrain:]

adp = 10


g = np.squeeze(Xtrain[:,0].toarray())
streamdp = GGPMixtureMultinomialStream(x=g, a=adp, tau=1, sig=0, alpha=0.2,
                          newclust_eps=0.5 ,prune_eps=0,merge_eps=.001,merge_interval=100,merge=False)
streaminv = GGPMixtureMultinomialStream(x=g, a=1, tau=1, sig=.5, alpha=0.2,
                          newclust_eps=0.5 ,prune_eps=0,merge_eps=.001,merge_interval=100,merge=False)
liketestdp = []
num_clusts_dp = []

liketestinv = []
num_clusts_inv = []
for i in xrange(ntrain):
    print i 
    g = np.squeeze(Xtrain[:,i].toarray())
    streamdp.update(g)
    streaminv.update(g)
    num_clusts_dp.append(streamdp.num_clusters())
    num_clusts_inv.append(streaminv.num_clusters())

    if (i % test_interval == test_interval - 1):
        liketestdp.append(streamdp.pred_ll_fast(Xtest,nonew=False))
        #num_clusts_dp.append(streamdp.num_clusters())

        liketestinv.append(streaminv.pred_ll_fast(Xtest,nonew=False))
        #num_clusts_inv.append(streaminv.num_clusters())

outfile = "pityordocs_n" + str(n) + "_a" + str(a) + "_sigma" + str(sigma) + "_V" + str(V) + "_alpha" + str(alpha) + "_mwords" + str(mwords)  + "_noisewords" + str(noisewords) + "_adp" + str(adp) + ".pdf"


plt.figure()
plt.subplot(2,1,1)
plt.plot(liketestdp, 'b', liketestinv, 'r')
plt.subplot(2,1,2)
plt.plot(num_clusts_dp, 'b', num_clusts_inv, 'r',num_clusters_true[:ntrain], 'g')
plt.show()

plt.savefig(outfile)