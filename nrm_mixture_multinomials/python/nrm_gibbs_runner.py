from __future__ import division

import types

import numpy as np
import scipy.optimize as spopt
import math
import time


from scipy.sparse import csc_matrix

from util import make_discrete_bars
from nrm_fast import logmpoyla
#from util import logmpoyla
from nrm_fast import pred_ll as pred_ll_fast
import matplotlib.pyplot as plt
from scipy.special import gammaln
from scipy.misc import logsumexp
from util import make_8x8_bars_mixture
from nrm_fast import pred_ll as pred_ll_fast
eps = np.finfo('float64').eps


class GGPMixtureMultinomialGibbs(object):
    def __init__(self,X,a=None, tau=None,sig=None,alpha=None,init=None,lik_interval=100):
        self.lik_interval = lik_interval
        self.n_samp = 0
        self.a = a
        self.tau = tau
        self.sig = sig
        self.alpha = alpha
        self.X=X
        self.Xdense = np.array(self.X.todense())
        self.n = self.X.shape[1]
        self.Voc = self.X.shape[0]
        if (init == 1):
            self.Z = np.zeros((self.n,1)) + 1
            self.n_clust = 1
            self.assignments = np.zeros(self.n)
            self.clust_counts = np.array([self.n])
            self.clust_vocab_counts = self.X.sum(1)
            self.total_clust_counts = np.array([np.sum(self.clust_vocab_counts)])

        else:
            self.Z = np.identity(self.n)
            self.n_clust = self.n
            self.assignments = range(self.n)
            self.clust_counts = np.ones(self.n)

        self.U = 15
        self.V = 5
        self.proposal_sd = 1/2

        self.theta_prior = np.zeros(self.Voc) + self.alpha
        #print(type(self.Xdense))
        #print(self.Xdense.shape)
        self.total_counts = np.sum(self.Xdense,0)
        #print(self.total_counts.shape)
        self.log_lik = []
        self.U_samp = []
        self.V_samp = []
        self.lgam_sum = self.comp_lgam_sum()
        self.pzeros = np.zeros(self.Voc)
        self.pzeros = self.pzeros[:,np.newaxis]



    def comp_lgam_sum(self):
        lgam_sum = np.zeros(self.n)
        for i in xrange(self.n):
            lgam_sum[i] += gammaln(self.total_counts[i] + 1)
            lgam_sum[i] += -np.sum(gammaln(self.X[:,i].todense() + 1))
        return(lgam_sum)


    def sample_U(self):
        ###sample from gaussian
        Vnew = np.random.normal(self.V,self.proposal_sd,1)
        lnew = self.V_log_density(Vnew)
        lold = self.V_log_density(self.V)
        diff = lnew - lold
        if (diff < 0):
            p = np.exp(diff)
            if np.random.binomial(1,p):
                #print("fart")
                self.V = Vnew
        else:
            #print("nochange")
            self.V = Vnew

        self.V_samp.append(np.squeeze(self.V))
        self.U = np.exp(self.V)
        return(self.U)


    def sample_ZFAST(self):
        #print('BEGIN')
        print(self.clust_counts)
        print(self.U)
        for i in xrange(self.n):
            print([i, self.n_clust])
            x = np.squeeze(self.X[:,i].toarray())
            #print(self.assignments[i])
            #print(self.clust_counts)
            self.clust_counts[self.assignments[i]] -= 1
            #print(self.clust_vocab_counts[:,self.assignments[i]].shape)
            self.clust_vocab_counts[:,self.assignments[i]] -= self.X[:,i]
            #print(self.total_clust_counts[self.assignments[i]])
            #print(self.total_counts[i])
            self.total_clust_counts[self.assignments[i]] -= self.total_counts[i]
            if self.clust_counts[self.assignments[i]] == 0:
                self.n_clust += -1
                idx = np.where(self.clust_counts != 0)[0]
                self.clust_counts = self.clust_counts[idx]
                here = np.where(self.assignments > self.assignments[i])
                self.assignments[here] += -1
                self.clust_vocab_counts = self.clust_vocab_counts[:,idx]
                self.total_clust_counts = self.total_clust_counts[idx]
                #print('removed')

            lpc = np.zeros(self.n_clust + 1)
            old_assignment = self.assignments[i]
            self.assignments[i] = -1
            for j in xrange(self.n_clust):
                #print(j)
                #print(self.clust_counts)
                #if self.clust_counts[j] < 1:
                #    continue
                idx = np.where(self.assignments == j)[0].tolist()
                #print(idx)
                lpriorj = np.log((self.clust_counts[j] - self.sig))

                #lpc[j] = lpriorj - self.log_mpg(self.X[:,idx],len(idx))
                #idx.append(i)
                #lpc[j] += self.log_mpg(self.X[:,idx],len(idx))

                #print([self.total_clust_counts[j], np.sum(np.sum(self.Xdense[:,idx]))])


                lpc[j] = lpriorj + self.log_mpg_completeFAST(self.clust_vocab_counts[:,j],self.total_clust_counts[j], self.X[:,i], self.total_counts[i],self.lgam_sum[i])
                #lpc[j] = lpriorj + self.log_mpg_complete(self.X[:,i].todense(), self.X[:,idx].todense())
                #print([q,lpc[j]])

            lpc[self.n_clust] = self.sig*np.log(self.U + self.tau) + np.log(self.a)
            #lpc[self.n_clust] += logmpoyla(x[:,np.newaxis], self.theta_prior) 
            lpc[self.n_clust] += self.log_mpg(self.X[:,i],1)
            #print(np.exp(lpc))
            Z = logsumexp(lpc)
            lpc -= Z
            lpc = np.exp(lpc)
            #print(lpc)
            #time.sleep(1)
            #q = input("Press Enter to continue...")
            rs = np.random.multinomial(1,lpc)
            h = np.where(rs == 1)[0]
            #print(h)
            if h == self.n_clust:
                #add component
                self.n_clust += 1
                self.clust_counts = np.resize(self.clust_counts,self.n_clust)
                self.clust_counts[self.n_clust-1] = 1

                self.clust_vocab_counts = np.hstack((self.clust_vocab_counts,self.Xdense[:,i][:,np.newaxis]))
                #self.clust_vocab_counts[:,self.n_clust-1] = self.Xdense[:,i]
                self.total_clust_counts = np.hstack((self.total_clust_counts,np.zeros(1)))
                self.total_clust_counts[-1] = self.total_counts[i]

                #old = self.clust_vocab_counts.copy()
                #self.clust_vocab_counts = np.zeros((self.Voc,self.n_clust))
                #self.clust_vocab_counts[:,:(self.n_clust - 1)] = old
                #self.clyst_vocab_counts[:,self.n_clust-1] = X[:,i]
                ##add column
                #self.Z = np.hstack((self.Z, self.nzeros)) 
                #print("new")
            else:
                self.clust_counts[h] += 1
                self.total_clust_counts[h] += self.total_counts[i]
                self.clust_vocab_counts[:,h] += self.X[:,i]

            self.assignments[i] = h





    def sample_Z(self):
        print('BEGIN')
        #print(self.clust_counts)
        for i in xrange(self.n):
            #print(self.n_clust)
            x = np.squeeze(self.X[:,i].toarray())
            #print(self.assignments[i])
            #print(self.clust_counts)
            self.clust_counts[self.assignments[i]] -= 1
            #self.clust_vocab_counts[:,self.assignments[i]] -= X[:,i]
            if self.clust_counts[self.assignments[i]] == 0:
                self.n_clust += -1
                idx = np.where(self.clust_counts != 0)[0]
                self.clust_counts = self.clust_counts[idx]
                here = np.where(self.assignments > self.assignments[i])
                self.assignments[here] += -1
                #self.clust_vocab_counts = self.clust_vocab_counts[:,idx]
                print('removed')

            lpc = np.zeros(self.n_clust + 1)
            old_assignment = self.assignments[i]
            self.assignments[i] = -1
            for j in xrange(self.n_clust):
                #print(j)
                #print(self.clust_counts)
                #if self.clust_counts[j] < 1:
                #    continue
                idx = np.where(self.assignments == j)[0].tolist()
                #print(idx)
                lpriorj = np.log((self.clust_counts[j] - self.sig))

                #lpc[j] = lpriorj - self.log_mpg(self.X[:,idx],len(idx))
                #idx.append(i)
                #lpc[j] += self.log_mpg(self.X[:,idx],len(idx))



                lpc[j] = lpriorj + self.log_mpg_complete(self.X[:,i].todense(), self.X[:,idx].todense())
                #print([q,lpc[j]])

            lpc[self.n_clust] = self.sig*np.log(self.U + self.tau) + np.log(self.a)
            #lpc[self.n_clust] += logmpoyla(x[:,np.newaxis], self.theta_prior) 
            lpc[self.n_clust] += self.log_mpg(self.X[:,i],1)
            #print(np.exp(lpc))
            Z = logsumexp(lpc)
            lpc -= Z
            lpc = np.exp(lpc)
            #print(lpc)
            #time.sleep(1)
            #q = input("Press Enter to continue...")
            rs = np.random.multinomial(1,lpc)
            h = np.where(rs == 1)[0]
            print(h)
            if h == self.n_clust:
                #add component
                self.n_clust += 1
                self.clust_counts = np.resize(self.clust_counts,self.n_clust)
                self.clust_counts[self.n_clust-1] = 1
                #old = self.clust_vocab_counts.copy()
                #self.clust_vocab_counts = np.zeros((self.Voc,self.n_clust))
                #self.clust_vocab_counts[:,:(self.n_clust - 1)] = old
                #self.clyst_vocab_counts[:,self.n_clust-1] = X[:,i]
                ##add column
                #self.Z = np.hstack((self.Z, self.nzeros)) 
            else:
                self.clust_counts[h] += 1

            #self.Z[i,:] = rs[:self.n_clust]
            self.assignments[i] = h

        #remove clusters that have nothing assigned to them
        # for i in xrange(length(self.clust_counts))

        # idx = np.where(self.clust_counts != 0)[0]
        # print(idx)
        # #self.Z = self.Z[:,idx]
        # self.clust_counts = self.clust_counts[idx]
        # self.n_clust = idx.shape[0]

    # def sample_theta(self):
    #     for i in xrange(self.n_clust):
    #         these = np.where(self.assignment == i)
    #         param = self.theta_prior + np.sum(self.X[these,:])
    #         theta[:,i] = np.random.dirichlet(param)

    #     return(theta)


    def log_mpg_completeFAST(self,n_v,nk,X1,nknew,lgam_sum):
        n_vs = n_v + X1
        nks = nk + nknew
        #print(nks)
        ll = 0
        ll += np.sum(gammaln(n_vs + self.alpha))
        ll += -np.sum(gammaln(n_v + self.alpha))
        ll += gammaln(self.Voc*self.alpha + nk)
        ll += -gammaln(self.Voc*self.alpha + nks)
        ll += lgam_sum

        #ll += gammaln(nknew + 1)
        #ll += -np.sum(gammaln(X1 + 1))
        return(ll)

    def log_mpg_complete(self,X1,X):
        n_v = np.sum(X,1)
        #print(n_v.shape)
        n_vs = n_v + X1
        nk = np.sum(n_v)
        nknew = np.sum(X1)
        nks = nk + nknew
        #print(nks)
        ll = 0
        ll += np.sum(gammaln(n_vs + self.alpha))
        ll += -np.sum(gammaln(n_v + self.alpha))
        ll += gammaln(self.Voc*self.alpha + nk)
        ll += -gammaln(self.Voc*self.alpha + nks)
        ll += gammaln(nknew + 1)
        ll += -np.sum(gammaln(X1 + 1))
        return(ll)

    def log_mpg(self,X,N):
        n_v = X.sum(1)
        n_d = np.array(X.sum(0))[0]
        nk = np.sum(n_d)
        ll = 0
        #for i in xrange(self.V):
        #    ll += lgamma(n_v[i] + self.alpha)

        ll += np.sum(gammaln(n_v + self.alpha))
        ll += - gammaln(self.Voc*self.alpha + nk)
        ll += gammaln(self.Voc*self.alpha)
        ll += -self.Voc*gammaln(self.alpha)
        #print(N)
        #print(n_d)

        #ll += np.sum(gammaln(n_d + 1))
        #ll += np.sum(np.sum(gammaln(X.todense)))
        for j in xrange(N):
            ll += gammaln(n_d[j] + 1)
            ll += -np.sum(gammaln(X[:,j].todense() + 1))

        return(ll)



    # def log_multinomial_pmf(p,counts,n):
    #     n = np.sum(counts)
    #     ll = gammaln(n + 1)
    #     ll -= np.sum(gammaln(counts))
    #     ll -= counts*np.log(p)
    #     return(ll)


    def update_U(self):
        return(self.U)

    def get_ready_for_prediction(self):
        self.nu = self.clust_vocab_counts + self.alpha
        print(self.nu.shape)

    def prior_assign(self,U):
        prior = self.clust_counts.copy()
        prior = prior/np.sum(prior)
        return(prior)

    def GGP_sample(self):
        self.n_samp += 1

        if (self.sig > 0):
            self.sample_U()
        self.sample_ZFAST()
        #self.sample_Z()
        if (self.n_samp % self.lik_interval == 2):
            self.U_samp.append(np.squeeze(self.U))
            print(self.U)
        self.log_lik.append(self.logliklihood())
        self.get_ready_for_prediction()


    def V_log_density(self,V):
        n = self.n
        ll = V*n
        ll += -(n - self.a*self.n_clust)*np.log(np.exp(V) + self.tau)
        ll += -(self.a/self.sig)*(np.power(np.exp(V) + self.tau,self.sig) - np.power(self.tau,self.sig))
        return(ll)

    def logliklihood(self):
        llik = 0
        if (sig >0):
            llik += self.UZ_logdensity()
        else:
            llik += self.Z_logdensity()
        for i in xrange(self.n_clust):
            idx = np.where(self.assignments == i)[0].tolist()
            llik += self.log_mpg(self.X[:,idx],len(idx))
        return(llik)

    def Z_logdensity(self):
        ll = 0
        if (self.n_clust > 1):
            ll += np.log(self.n_clust-1)*self.a
        print(ll)
        ll += np.sum(gammaln(self.clust_counts))
        print(ll)
        ll += -gammaln(self.n + self.a) + gammaln(self.a)
        print(ll)
        return(ll)

    def u_post(self,U):
        n = self.n
        loglik = (n-1)*np.log(U + eps)
        loglik += -(n - self.a*self.n_clust)*np.log(U + self.tau)
        loglik += -(self.a/self.sig)*(np.power(U+self.tau,self.sig) - np.power(self.tau,self.sig))
        return(np.exp(loglik))

    def UZ_logdensity(self):
        llik = 0
        llik += self.U_log_density(self.U)
        for i in xrange(self.n_clust):
            llik += math.lgamma(self.assignments[i] - self.sig)
        llik += -self.n_clust*math.lgamma(1 - self.sig)
        return(llik)

    def U_log_density(self,U):
        llik = 0
        llik += self.n_clust*np.log(self.a) + (self.n - 1)*np.log(U) - math.lgamma(self.n)
        llik += -(self.n - self.sig*self.n_clust)*np.log(U + self.tau)
        llik += -(self.a/self.sig)*(np.power(U + self.tau,self.sig) - np.power(self.tau,self.sig))
        return(llik)

        # Add pred_ll_fast to class so can call normally
GGPMixtureMultinomialGibbs.pred_ll_fast = types.MethodType(pred_ll_fast,
                                               None, GGPMixtureMultinomialGibbs)

    # def lbeta(alpha):
    #     return sum(math.lgamma(a) for a in alpha) - math.lgamma(sum(alpha))

    # def ldirichlet_pdf(alpha, theta):
    #     kernel = sum((a - 1) * math.log(t) for a, t in zip(alpha, theta))
    #     return kernel - lbeta(alpha)


if __name__ == "__main__":
    lik_interval = 1
    a = 1
    tau = 1
    sig = .5
    alpha=1

    Xtrain = np.zeros((10,10))
    Xtrain[:5,:5] = 4.
    Xtrain[5:10,5:10] = 4.
    print(Xtrain)



    Xtrain,Xtest = make_8x8_bars_mixture(Ntrain=100,Ntest=1,obsperdat=40,baselineprob=.1)
    Xtrain = csc_matrix(Xtrain)
    n = Xtrain.shape[1]
    p = Xtrain.shape[0]
    n_samp = 200
    print(n)
    print(p)
    ggp_mix = GGPMixtureMultinomialGibbs(X=Xtrain, a=a, tau=tau, sig=sig,alpha=alpha,init=1,lik_interval=lik_interval)


    #ggp_mix.assignments=np.array([0,0,0,0,0,1,1,1,1,1])
    #ggp_mix.clust_counts = np.array([5,5])
    #ggp_mix.n_clust = 2
    #ggp_mix.clust_vocab_counts = np.zeros((10,2))
    #ggp_mix.clust_vocab_counts[:,0] = np.sum(ggp_mix.Xdense[:,5:],1)
    #ggp_mix.clust_vocab_counts[:,1] = np.sum(ggp_mix.Xdense[:,5:],1)
    print('here')
    print(ggp_mix.clust_vocab_counts.shape)

    #ggp_mix.total_clust_counts = np.array([np.sum(ggp_mix.clust_vocab_counts[:,0]), np.sum(ggp_mix.clust_vocab_counts[:,1])])


    for i in xrange(n_samp):
        print([i, ggp_mix.n_clust])
        ggp_mix.GGP_sample()


    U = np.arange(0,100,1)
    pU = np.zeros(U.shape)
    j = 0;
    for i in U:
        pU[j] = ggp_mix.u_post(i)
        print(pU[j])
        j += 1


    V = np.arange(-2,10,.1)
    pV = np.zeros(V.shape)
    j = 0
    for i in V:
        pV[j] = np.exp(ggp_mix.V_log_density(i))
        j += 1


    # plt.subplot(4,1,1)
    # plt.plot(U,pU)
    # plt.subplot(4,1,2)
    # plt.hist(ggp_mix.U_samp)
    # plt.subplot(4,1,3)
    # plt.plot(V,pV)
    # plt.subplot(4,1,4)
    # plt.hist(ggp_mix.V_samp)





    #ggp_mix.clust_counts = np.array([5,5])
    #ggp_mix.assignments=np.array([0,0,0,0,0,1,1,1,1,1])
    #ggp_mix.n_clust = 2
    # print(ggp_mix.logliklihood())

    # plt.plot(ggp_mix.log_lik)
    # plt.show()













