from __future__ import division
import numpy as np 
import scipy as sp
import mpmath
import sys, os
from scipy.sparse import csc_matrix
from scipy.misc import factorial, comb
from scipy.special import gammaincc, gamma
from mpmath import mpf
os.environ['MPMATH_NOGMPY'] = 'Y'


mpmath.mp.dps = 300

def generate_assignments(a,tau,sigma,n):
    use_flex = True
    Ktop = 10000
    assignments = np.zeros((n,Ktop))
    assignments[0,0] = 1
    assignments1d = np.zeros(n)
    assignment_counts = np.zeros(Ktop)
    assignment_counts[0] = 1
    k = 1
    for i in xrange(1,n):
        print(i)
        print(k)
        if use_flex:
            if kchange:
                denominator = compute_denominator2(i,a,k,tau,sigma)
                #print(denominator)
                wo = compute_wo2(i,a,k,tau,sigma,denominator)
                w1 = compute_w12(i,a,k,tau,sigma,denominator)
                #print(wo)
                #print(w1)
            else:
                denominator += add_denom2(i,a,k,tau,sigma)
        else:
            wo = compute_wo(i,a,k)
            w1 = compute_w1(i,a,k)
        prob = np.zeros(k + 1)
        prob[k] = wo
        prob[:k] = w1*(assignment_counts[:k] - sigma)
        #print(sum(prob))
        #less = np.where(prob < 0)
        #prob[less] = 0
        print(sum(prob))

        prob /= sum(prob)
        #print(sum(prob))
        #print(min(prob))
        new = np.random.multinomial(1,prob)
        whichassigned = np.where(new == 1)[0]
        #print(whichassigned)
        assignment_counts[whichassigned] += 1
        if whichassigned == k:
            k += 1
            kchange = True
        else:
            kchange = False
        assignments[i,whichassigned] = 1
        assignments1d[i] = whichassigned

    return(assignments1d,assignments)



def compute_denominator2(n,a,k,tau,sigma):
    beta = np.power(tau,sigma)/sigma
    denominator = mpf(0)
    for i in xrange(n):
        denominator += mpmath.binomial(n-1,i)*mpf(np.power(-1,i))*mpmath.power(beta,i/sigma)*mpmath.gammainc(k - i/sigma,beta)
    denominator = denominator*n
    #print(type(denominator))
    return(denominator)





def compute_wo2(n,a,k,tau,sigma,denominator):
    beta = np.power(tau,sigma)/sigma
    numerator = mpf(0)
    #denominator = 0
    for i in xrange(n+1):
        numerator += mpmath.binomial(n,i)*np.power(-1,i)*mpmath.power(beta,i/sigma)*mpmath.gammainc(k + 1 - i/sigma,beta)
    #for i in xrange(n):
    #    denominator += choose(n-1,i)*np.power(-1,i)*np.power(beta,i/sigma)*mpmath.gammainc(k - i/sigma,beta)
    #denominator = denominator*n
    numerator = numerator*sigma
    #print(type(numerator))
    return(numerator/denominator)

def compute_w12(n,a,k,tau,sigma,denominator):
    beta = np.power(tau,sigma)/sigma
    numerator = mpf(0)
    #denominator = 0
    for i in xrange(n+1):
        #print(beta)
        #print(i/sigma)
        #print(mpmath.power(beta,i/sigma))
        numerator += mpmath.binomial(n,i)*np.power(-1,i)*mpmath.power(beta,i/sigma)*mpmath.gammainc(k - i/sigma,beta)
    #for i in xrange(n):
    #    denominator += choose(n-1,i)*np.power(-1,i)*np.power(beta,i/sigma)*mpmath.gammainc(k - i/sigma,beta)
    #denominator = denominator*n
    return(numerator/denominator)


def compute_wo(m,a,k):
    numerator = 0
    denominator = 0
    for j in xrange(m + 1):
        #print("j is " + str(j))
        #print(-(k + 1 + 2*j - 2*m))
        #print(mpmath.gammainc(-(k + 1 + 2*j - 2*m),a))

        numerator += choose(m,j)*mpmath.power(-a*a,-j + 1)*mpmath.gammainc((k + 1 + 2*j - 2*m),a)
        #print(numerator)
    for j in xrange(m):
        denominator += 2*m*choose(m - 1,j)*mpmath.power(-a*a,-j)*mpmath.gammainc((k + 2 + 2*j - 2*m),a)
    #print("numerator:" + str(numerator))
    #print("denominator:" + str(denominator))
    return(numerator/denominator)

def compute_w1(m,a,k):
    numerator = 0
    denominator = 0
    for j in xrange(m + 1):
        numerator += choose(m,j)*np.power(-a*a,-j + 1)*mpmath.gammainc((k + 2*j - 2*m),a)
    for j in xrange(m):
        denominator += m*choose(m - 1,j)*np.power(-a*a,-j)*mpmath.gammainc((k + 2 + 2*j - 2*m),a)
    return(numerator/denominator)

def choose(n,r):
    #print('new')
    #print(n)
    #print(r)
    #print(factorial(n-r))
    #return(comb(n,r,exact=False,repetition=False))
    return(mpmath.binomial(n,r))
    #return(factorial(n)/(factorial(r)*factorial(n-r)))

if __name__ == "__main__":
    sigma = .5
    tau = 1
    n = 1000
    a = 1
    assignments1d, assignments = generate_assignments(a,tau,sigma,n)
    #save data

    outfile = "n" + str(n) + "_a" + str(a) + "_tau" + str(tau) + "_sigma" + str(sigma) + ".npy"
    np.save(outfile,assignments1d)



