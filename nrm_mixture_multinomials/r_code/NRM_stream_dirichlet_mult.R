


Ntotal = 1791
test = (Ntotal - 200):Ntotal
D = 99 #dimensionality of data
N = Ntotal - length(test) #number of observations
K = N #size of cluster array
#alpha_prior = .1*rep(1,D)
alpha_prior = .6*rep(1,D)


setwd('/Users/alextank/Dropbox/NRM/NRM_Stream/')
##algorithm parameters####
a = 1
sig = .0000000000001
tau = 1


merge_dist = FALSE
merge_soft = FALSE
prune = FALSE
merge = FALSE
hard_assignments = TRUE

threshnew = .5;
threshprune = 0;
threshmerge = 5;
threshup = .1;

prune_interval = 30;
merge_interval = 250;

###NGGP parameters#####


###dirichlet prior
####data parameters#####

##algorithm data structures####
soft_assignments = array(0,c(N,K))
clust_activated = rep(0,K)
clust_activated[1] = 1
alpha_post = matrix(0,N,D)
prior_assignment_unorm = rep(0,K)

#for first cluster, update param
post = Bayesian_DirMult_Compute(alpha_prior,X[1,],1)
alpha_post[1,] = post
soft_assignments[1,1] = 1
prior_assignment_unorm[1] = 1


lik = rep(0,N)
lik_int = 100
lik_count = 1
amount_this = N/lik_int;
lik = rep(0,amount_this)
nact = rep(0,amount_this)
Up = 1

these = seq(1,N,by = lik_int)
lthese = length(these)

for (n in 2:N) {
  #print(n)
  prior_assignments = rep(0,K)
  #first compute cluster assignments
  active = which(prior_assignment_unorm > 0)
  nactive = length(active)
  Eclust = nactive
  new_clust = max(active) + 1
  print(length(active))
  #print("newclust")
  #print(new_clust)
  U = optim(Up, U_n, method = "BFGS", n = n, tau = tau, a = a, sig = sig, nclust = Eclust)$par
  #print(U)
  U_p = U
  print(U_p)
  ######compute predictive , under U_n max #######
  prior_assignments[active] = (prior_assignment_unorm[active] - sig)/(a*(U + tau)^sig + n - sig*Eclust)
  prior_assignments[new_clust] = (a*(U + tau)^sig)/(a*(U + tau)^sig + n - sig*Eclust)
  #print(prior_assignments[new_clust])
  
  prior_assignments = prior_assignments/sum(prior_assignments)
  
  if (n %% lik_int == 2) {
    lik[lik_count] = likelihood_DM_compute(X[test,], prior_assignments, active, new_clust, length(test), alpha_post, alpha_prior, D)
    nact[lik_count] = length(active)
    lik_count = lik_count + 1
  }
  
  for (i in active) {
    #pred_sig = sig_lik + sig_post[i,,]
    #soft_assignments[n,i] = prior_assignments[i]*dmvnorm(X[n,], mu_post[i,] ,pred_sig)
    
    #soft_assignments[n,i] = prior_assignments[i]*mpolya(X[n,], alpha_post[i,],D, log = FALSE)
    soft_assignments[n,i] = log(prior_assignments[i]) + mpolya(X[n,], alpha_post[i,],D, log = TRUE)
    #if (soft_assignments[n,i] == 0) {
    #}
  }
  
  #soft_assignments[n,new_clust] = prior_assignments[new_clust]*mpolya(X[n,], alpha_prior,D, log = FALSE)
  soft_assignments[n,new_clust] = log(prior_assignments[new_clust]) + mpolya(X[n,], alpha_prior,D, log = TRUE)
  
  this = c(active, new_clust)
  
  maxl = max(soft_assignments[n,c(active, new_clust)])
  soft_assignments[n,this] = exp(soft_assignments[n,this] - maxl)
  
  Z = sum(soft_assignments[n,this])
  soft_assignments[n,this] = soft_assignments[n,this]/Z
  
  print(soft_assignments[n,new_clust])
  
  ###check if new cluster assignment is above threshold###
  dummy = soft_assignments_prune_new(soft_assignments[n,],n, new_clust, threshnew)
  soft_assignments[n,] = dummy[[1]]
  if (dummy[[2]]) {
    active = c(active,new_clust)
    clust_activated[new_clust] = n
  }
  
  if (hard_assignments) {
    max = which(soft_assignments[n,] == max(soft_assignments[n,]))
    soft_assignments[n,] = 0
    soft_assignments[n,max] = 1             
  }
  #print(soft_assignments[n,1:4])
  
  
  if (abs(soft_assignments[n,1] - soft_assignments[n,2]) < .2) {
    #print(X[n,])
  }
  #print(new_clust)
  #now update parameters
  for (i in active) {
    if (i == new_clust) {
      alpha_post[i,] = alpha_prior
    }
    
    if (soft_assignments[n,i] > threshup) {
      
      post = Bayesian_DirMult_Compute(alpha_post[i,], X[n,], soft_assignments[n,i])
      #post = Bayesian_Gaussian_Compute(sig_post[i,,], sig_lik/soft_assignments[n,i], mu_post[i,], X[n,],1)
      alpha_post[i,] = post
    }
  }
  
  prior_assignment_unorm = prior_assignment_update(prior_assignment_unorm, soft_assignments[n,])
  
  #prune
  if (prune) {
    if (n %% prune_interval == 0) {
      prior_assignment_unorm = prune_clust(prior_assignment_unorm, threshprune)
    }
  }
  
  if (merge) {
    if (n %% merge_interval == 0) {
      merged = merge_cluster_DM(prior_assignment_unorm, threshmerge, active, n,clust_activated)
      prior_assignment_unorm = merged[[1]]; 
      soft_assignments = merged[[2]]; 
      mu_post = merged[[3]];
      sig_post = merged[[4]]      
    }
  }  
  #prior_assignments = prior_assignment_unorm/(sum(prior_assignment_unorm) + alpha)
  active = which(prior_assignments > 0)
}

ass = apply(soft_assignments, 1, function(x) which(x == max(x)))
assinperm = ass[inverse_perm]

